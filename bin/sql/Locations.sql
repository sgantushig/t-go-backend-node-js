CREATE TABLE Locations (
    location_id             serial primary key,
    acc_id                  integer,
    acc_tel                 integer,
    location_names          varchar,
    coordinate_names        varchar,
    location_coordinates    varchar,
    location_icon           varchar
);

INSERT INTO Locations ( acc_id, acc_tel, location_names, coordinate_names, location_coordinates )
VALUES ( 1, 99118533, 'Манай оффис', 'Central Tower, Ulaanbaatar 14200, Mongolia', '{"lat": 47.9180338,"lng": 106.92029}');