CREATE TABLE otp_limit (
    otp_id          serial primary key,
    account_id      integer,
    otp_number      integer default 0 not null
);