CREATE TABLE Rating (
    rating_id       serial primary key,
    driver_id       integer,
    order_id        integer,
    clean_car       integer,
    cool_car        integer,
    dj              integer,
    nice_driver     integer
);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (1, 1, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (2, 2, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (3, 3, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (4, 4, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (5, 5, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (6, 6, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (7, 7, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (8, 8, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (9, 9, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (10, 10, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (11, 11, 1, 1, 1, 1);

INSERT INTO Rating (driver_id, order_id, clean_car, cool_car, dj, nice_driver)
VALUES (12, 12, 1, 1, 1, 1);