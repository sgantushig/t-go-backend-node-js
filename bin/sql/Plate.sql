CREATE TABLE Plate(
    plate_id        serial primary key,
    driver_id       integer,
    driver_tel      varchar,
    plate_num       varchar,
    car_mark        varchar
);

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (1, '99118533', '7976', 'toyota prius 20');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (2, '99247524', '4649', 'toyota prius 30');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (3, '99671874', '1525', 'toyota prius 40');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (4, '88685787', '5659', 'hyundai sonata 6');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (5, '89949451', '9636', 'hyundai sonata 4');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (6, '88149151', '1245', 'hyundai sonata 7');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (7, '96005388', '6936', 'hyundai nissan civic');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (8, '88437560', '4698', 'mazda rx6');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (9, '88921840', '7172', 'nissan altima');

INSERT INTO Plate (driver_id, driver_tel, plate_num, car_mark)
VALUES (10, '99916101', '6699', 'nissan leaf');