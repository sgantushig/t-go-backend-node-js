CREATE TABLE Drive_history (
    id                  serial primary key,
    "driveId"           integer,
    "accountId"         integer,
    "lastDriver"        boolean,
    "lastCoordinates"   varchar,
    "lastTimestamp"     timestamp,
    "lastStatus"         varchar
);

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  101, 1, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  102, 2, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  103, 3, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  104, 4, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  105, 5, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  106, 6, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  107, 7, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  108, 8, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  109, 9, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );

INSERT INTO Drive_history ( "driveId" , "accountId", "lastDriver" , "lastCoordinates", "lastTimestamp", "lastStatus" )  
VALUES (  110, 10, true,'{"lat":47.915195,"lng":106.911986}' , '2019-08-26 16:54:35.908673',  'FREE' );
