CREATE TABLE Admin_account(
    id              serial primary key,
    sessionid       varchar,
    usernamehash    varchar,
    lastname        varchar,
    email           varchar,
    passwordhash    varchar,
    avatar          varchar
);

INSERT INTO Admin_account (sessionid, usernamehash, lastname, email, passwordhash, avatar)
VALUES ( '777ef87c-a036-440d-84e5-79a5e47ba997', '7690438d93cb60fd1a926d202ff296e01b2a15f971a5333abf7eeb7301e1e4c6', null, null, '9932c8a20abf81f9ff9965e809d29d1543d5dbd0d261b67c5c837f3332ac8e59', null);