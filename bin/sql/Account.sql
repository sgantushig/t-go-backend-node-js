CREATE TABLE Account(              
    id                  SERIAL PRIMARY KEY,
    tel                 varchar(8) NOT NULL,
    "pinHash"           varchar,
    name                varchar,
    avatar              varchar,
    "qrCode"            varchar,
    driver              Boolean DEFAULT false,
    timestamp           timestamp DEFAULT CURRENT_TIMESTAMP,
    "startCoordinates"  varchar,    
    "endCoordinates"    varchar,
    "randPin"           numeric(4),
    "randHashPin"       varchar,
    password            Boolean DEFAULT false,
    online              Boolean DEFAULT false,
    status              varchar
);

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '99118533', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Ebo ah', '000000001', true, now(), '{"lat" : 47.918117, "lng" : 106.921030}', '1234', true, true );

INSERT INTO Account ( tel, "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '99247524', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Gantushig Mobi', '000000002', true, now(), '{"lat" : 47.918779, "lng" : 106.917502}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '99671874', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Buya Android', '000000003', true, now(),'{"lat" : 47.920217, "lng" : 106.909254}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '88685787', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Emee2', '000000004', true, now(),'{"lat" : 47.921729, "lng" : 106.894340}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '89949451', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Sese', '000000005', true, now(), '{"lat" : 47.914849, "lng" : 106.918406}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '88149151', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Ariuka duu', '000000006', true, now(), '{"lat" : 47.905375, "lng" : 106.913566}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '96005388', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Batnyam ah', '000000007', true, now(), '{"lat" : 47.912780, "lng" : 106.920552}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '88437560', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Hosoo', '000000008', true, now(),'{"lat" : 47.921873, "lng" : 106.927140}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '88921840', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Tuwshoo', '000000009', true, now(), '{"lat" : 47.921824, "lng" : 106.922155}', '1234', true, true );

INSERT INTO Account ( tel , "pinHash", name, "qrCode", driver, timestamp, "startCoordinates", "randPin", password, online )
VALUES ( '99916101', '4c6f94c54991ba0f9a0a71f890f48794f43d12c6c8152110ceefd63e23adeec2', 'Shijiree', '000000010', true, now(), '{"lat" : 47.914383, "lng" : 106.906323}', '1234', true, true );