CREATE TABLE Token (
    token_id        serial primary key,
    token           varchar,
    account_id      integer, 
    login_id        integer
);