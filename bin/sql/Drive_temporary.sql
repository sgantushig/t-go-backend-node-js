create table Drive_temporary (
    dt_id               serial primary key,
    drive_id            INTEGER,
    acc_id              INTEGER,
    order_id            INTEGER,
    drive_coordinates   varchar,
    drive_timestamp     TIMESTAMP,
    distance_meter      INTEGER
);

