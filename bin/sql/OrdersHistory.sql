create table OrdersHistory(
    id                              serial primary key,
    "ordersId"                      varchar,

    tel                             varchar(8) ,
    "customerId"                    integer,
    "driverId"                      integer, 

    "coordinatesInit"               varchar,
    "locationInit"                  varchar,

    "coordinatesInitDriver"         varchar,
    "locationInitDriver"            varchar,

    "coordinatesDest"               varchar,
    "locationDest"                  varchar,

    "timestampInit"                 timestamp ,
    "timestampArrive"               timestamp ,

    "timestampClosed"               timestamp ,
    "timestampStarted"              timestamp ,

    "timestampDrive"                timestamp ,

    "timestampStopped"              timestamp ,

    "timestampPaid"                timestamp ,

    status                          varchar,
    extra                           varchar,

    "lastVoiceDriver"               varchar,
    description                     text ,
    comment                         text ,

    "lastVoiceDriverTimestamp"      timestamp,
    "lastVoiceCustomer"             varchar,
    "lastVoiceCustomerTimestamp"    timestamp 
);

INSERT INTO OrdersHistory("ordersId" ,tel, "customerId", "driverId", "coordinatesInit", "coordinatesInitDriver", "coordinatesDriver", "coordinatesDest", "coordinatesCurrent", "timestampInit", "timestampArrive", "timestampWaiting", "timestampClosed", "timestampStarted", 
"timestampDrive", "timestampCurrent", "timestampStopped", "timestampBilling", "timestampPayed", STATUS, extra, "lastVoiceDriver", description, comment, "lastVoiceDriverTimestamp", "lastVoiceCustomer", "lastVoiceCustomerTimestamp")

VALUES (1 ,79797979 ,18, 1, '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', '{"lat" : 47.915901, "lng" : 106.895238}', 
now(), now(), now(),now(),now(),now(),now(),now(),now(),now(), true, 'good', null, 'good', 'best', now(), null, now());