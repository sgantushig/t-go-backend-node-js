CREATE TABLE Fcm_token (
    fcm_id          serial primary key,
    account_id      integer,
    push_token      varchar
);