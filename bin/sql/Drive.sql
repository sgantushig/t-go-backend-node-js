create table Drive (
    id              serial primary key,
    "accountId"     INTEGER ,
    online          boolean,
    coordinates     varchar,
    driver          boolean, 
    status          varchar,
    timestamp       TIMESTAMP DEFAULT NOW()
);

INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (1, true, '{"lat" : 47.918117, "lng" : 106.921030}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (2, true, '{"lat" : 47.918779, "lng" : 106.917502}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (3, true, '{"lat" : 47.920217, "lng" : 106.909254}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (4, true, '{"lat" : 47.921729, "lng" : 106.894340}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (5, true, '{"lat" : 47.914849, "lng" : 106.918406}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (6, true, '{"lat" : 47.905375, "lng" : 106.913566}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (7, true, '{"lat" : 47.912780, "lng" : 106.920552}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (8, true, '{"lat" : 47.921873, "lng" : 106.927140}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (9, true, '{"lat" : 47.921824, "lng" : 106.922155}', true, 'FREE', now());


INSERT INTO Drive ("accountId", online, coordinates, driver, status, timestamp)
VALUES (10, true, '{"lat" : 47.914383, "lng" : 106.906323}', true, 'FREE', now());