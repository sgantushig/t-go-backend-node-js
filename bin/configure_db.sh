#!/bin/bash

export PGPASSWORD='User2019'

echo "Configuring ttap"

dropdb -U node_user ttapdb;
createdb -U node_user ttapdb;

psql -U node_user ttapdb < ./bin/sql/Account.sql
psql -U node_user ttapdb < ./bin/sql/Orders.sql
psql -U node_user ttapdb < ./bin/sql/OrdersHistory.sql
psql -U node_user ttapdb < ./bin/sql/Voice.sql
psql -U node_user ttapdb < ./bin/sql/Drive.sql
psql -U node_user ttapdb < ./bin/sql/OrdersTemporary.sql


echo "ttap configured"