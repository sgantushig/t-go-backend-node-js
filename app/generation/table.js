const pool = require('../../databasePool');

class GenerationTable {
  
  static storeGeneration(generation) {
    return new Promise((resolve, reject) => {
      pool.query(
        'INSERT INTO generation(expiration) VALUES($1) RETURNING id',
        [generation.expiration],
        (error, response) => {
          if (error) return reject(error);
  
          const generationId = response.rows[0].id;

          resolve({ generationId });
        }
      );
    });
  }

  static storeDrive( accountId, coordinates, driver, status, online, timestamp) {

    return new Promise((resolve, reject) => {
        pool.query(

            `INSERT INTO Drive("accountId", coordinates, driver, status, online, timestamp )
            VALUES($1, $2, $3, $4, $5, $6)`,

            [accountId, coordinates, driver, status, online, timestamp ],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }
}

module.exports = GenerationTable;