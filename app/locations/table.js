const pool = require('../../databasePool');

class LocationsTable {

    static storeLocations( acc_id, acc_tel, location_names, coordinates_names, location_coordinates, location_icon ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Locations(
                    acc_id, 
                    acc_tel, 
                    location_names, 
                    coordinate_names, 
                    location_coordinates,
                    location_icon )  
                VALUES( $1, $2, $3, $4, $5, $6 )`,

                [acc_id, acc_tel, location_names, coordinates_names, location_coordinates, location_icon],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.storeLocations");

                console.log("storeLocations")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static updateLocations( location_names, coordinate_names, location_coordinates, location_icon, acc_id, location_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Locations SET 
                    location_names = $1,
                    coordinate_names = $2,
                    location_coordinates = $3,
                    location_icon = $4
                WHERE acc_id = $5 and location_id = $6`,

                [ location_names, coordinate_names, location_coordinates, location_icon, acc_id, location_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.updateLocations");

                resolve({ msg : "success" });
                }
            );
        });
    };

    static getLocations() {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Locations`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.getLocations");

                console.log("getLocations")

                resolve(response.rows);
                }
            );
        });
    };

    static getLocationsByAcc_Id( acc_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Locations WHERE acc_id = $1 ORDER BY acc_id DESC LIMIT 1`,

                [acc_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.getLocationsByAcc_Id");

                //console.log(response.rows,"getLocationsByAcc_Id wahahahha")

                resolve(response.rows);
                }
            );
        });
    };

    static getLocationsAll( acc_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Locations WHERE acc_id = $1 ORDER BY acc_id DESC`,

                [acc_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.getLocationsAll");

                //console.log(response.rows,"getLocationsByAcc_Id wahahahha")

                resolve(response.rows);
                }
            );
        });
    };

    static getLocationsByNames( acc_id, location_names ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Locations WHERE acc_id = $1 and location_names = $2 ORDER BY acc_id DESC`,

                [acc_id, location_names],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.getLocationsByNames");

                //console.log(response.rows,"getLocationsByAcc_Id wahahahha")

                resolve(response.rows);
                }
            );
        });
    };

    static deleteLocations( acc_id, location_names, location_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `DELETE FROM Locations WHERE acc_id = $1 and location_names = $2 and location_id = $3`,

                [acc_id, location_names, location_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : LocationsTable.deleteLocations");

                //console.log(response.rows,"getLocationsByAcc_Id wahahahha")

                resolve({ msg : "success" });
                }
            );
        });
    };
    
}

module.exports = LocationsTable;