const pool = require('../../databasePool');

class AccountTable {

  static storeAccount({ tel, pinHash, name, avatar, driver, status }) {
    return new Promise((resolve, reject) => {
      pool.query(
        `INSERT INTO Account(tel, "pinHash", name, avatar, driver, status)
         VALUES($1, $2, $3, $4, $5, $6)`,
        [tel, pinHash, name, avatar, driver, status],
        (error, response) => {
          if (error) return reject(error);

          resolve({ msg: 'success!'});
        }
      );
    });
  }

  static getAccountById(id) {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account
         WHERE id = $1`,
        [id],
        (error, response) => {
          if (error) return reject(error);

          resolve({ account: response.rows[0] });
        }
      )
    });
  }

  static getTelById(id) {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT tel FROM Account
         WHERE id = $1`,
        [id],
        (error, response) => {
          if (error) return reject(error);

          resolve(response.rows[0]);
        }
      )
    });
  }

  static getAccount(tel) {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account
         WHERE tel = $1`,
        [tel],
        (error, response) => {
          if (error) return reject(error);

          resolve({ account: response.rows[0] });
        }
      )
    });
  }

  static getAccountsTable() {
    return new Promise((resolve, reject) => {
      pool.query(
        `SELECT * FROM Account ORDER BY id ASC`,
        
        (error, response) => {
          if (error) return reject(error);

          resolve(response.rows);
        }
      )
    });
  }


//*********************************** This is were the codes I wrote start. *****************************************************************************/


  static updateAccountPin( pinHash, tel ) {
    return new Promise((resolve, reject) => {
      pool.query(
        `UPDATE Account SET "pinHash" = $1 WHERE tel = $2`, 
        
        [pinHash, tel],

        (error, response) => {
          if (error) return reject(error);

          resolve({ msg: 'success!'});
        }
      );
    });
  }

  static updateAccountRand( randPin, randHashPin, tel ) {
    return new Promise((resolve, reject) => {

      //console.log("the pool query", randPin, randHashPin, tel)
      pool.query(
        `UPDATE Account SET "randPin" = $1, "randHashPin" = $2 WHERE tel = $3`,
        
        [randPin, randHashPin, tel],

        (error, response) => {
          if (error) return reject(error);

          resolve({ msg: 'success!'});
        }
      );
    });
  }

  static updateStartCoordinates( startCoordinates, id) {

    console.log("**** start coordinates from account table js ****", startCoordinates, id);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET "startCoordinates" = $1
            WHERE id = $2`,
            [startCoordinates, id],

            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateOnline( online, id) {

    console.log("**** update online from account table js ****", online, id);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET online = $1
            WHERE id = $2`,
            [online, id],

            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }
  
  static updateStatus( status, id) {

    console.log("**** update status from account table js ****", status, id);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET status = $1
            WHERE id = $2`,
            [status, id],

            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateDriverStatus( driver, id) {

    console.log("**** updateDriverStatus from account table js ****", driver, id);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET driver = $1
            WHERE id = $2`,
            [driver, id],

            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateStatusByTel( status, tel) {

    console.log("**** update status from account table js ****", status, tel);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET status = $1
            WHERE tel = $2`,
            [status, tel],

            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static nullRandHashPin(id) {

    console.log("**** nullify randhashpin ****",  id);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET "randHashPin" = null WHERE id = $1`,

            [id],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static forgotAccount(tel) {

    console.log("**** nullify randhashpin, password, randPin ****",  tel);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET "randHashPin" = null, password = false, "pinHash" = null WHERE tel = $1`,

            [tel],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateAvatar( avatar ,tel) {

    console.log("**** only the AVATAR change****", avatar, tel);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET avatar = $1 WHERE tel = $2`,

            [avatar, tel],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateAccountName( name ,tel) {

    console.log("**** only the name change****", name, tel);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET name = $1 WHERE tel = $2`,

            [name, tel],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateAccount(randHashPin, password, pinHash, name, tel) {

    console.log("**** update Account randhashpin, password, pinHash, name ****",  tel, randHashPin, password, pinHash, name);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET "randHashPin" = $1, password = $2, "pinHash" = $3, name = $4 WHERE tel = $5`,

            [randHashPin, password, pinHash, name, tel],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updateAccountPassHash(randHashPin, password, pinHash, tel) {

    console.log("**** updateAccountPassHash randhashpin, password, pinHash ****", randHashPin, password, pinHash, tel);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET "randHashPin" = $1, password = $2, "pinHash" = $3 WHERE tel = $4`,

            [randHashPin, password, pinHash,tel],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }

  static updatePinHash(pinHash, tel) {

    console.log("**** update Account pinhash ****",  tel, pinHash);

    return new Promise((resolve, reject) => {
        pool.query(

            `UPDATE Account SET "pinHash" = $1 WHERE tel = $2`,

            [pinHash, tel],
            (error, response) => {
            if (error) return reject(error);

            resolve({ msg: 'success!'});
            }
        );
    });
  }


//*********************************** This is were the codes I wrote end. *****************************************************************************/
}

module.exports = AccountTable;
