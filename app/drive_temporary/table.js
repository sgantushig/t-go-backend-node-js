const pool = require('../../databasePool');

class Drive_temporaryTable {

    static getAllDriveTemporary() {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Drive_temporary ORDER BY dt_id DESC`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Drive_temporaryTable.getAllDriveTemporary");

                console.log("Drive_temporaryTable.getAllDriveTemporary");

                resolve(response.rows);
                }
            );
        });
    };

    static getDriveTemporary( acc_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Drive_temporary WHERE acc_id = $1 ORDER BY dt_id DESC LIMIT 1`,

                [acc_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Drive_temporaryTable.getDriveHistory");

                console.log("Drive_temporaryTable.getDriveHistory");

                resolve(response.rows[0]);
                }
            );
        });
    };

    static storeDriveTemporary( drive_id, acc_id, drive_coordinates, drive_timestamp, distance_meter, order_id ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Drive_temporary ( drive_id, acc_id, drive_coordinates, drive_timestamp, distance_meter , order_id)
                VALUES ($1, $2, $3, $4, $5, $6)`,

                [ drive_id, acc_id, drive_coordinates, drive_timestamp, distance_meter, order_id ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Drive_temporaryTable.storeDriveTemporary");

                console.log("Drive_temporaryTable.storeDriveTemporary");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static deleteDriveTemporary( acc_id ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `Delete from Drive_temporary 
                Where acc_id = $1` ,

                [acc_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Drive_temporaryTable.deleteDriveTemporary");

                console.log("Drive_temporaryTable.deleteDriveTemporary");
                resolve({ msg: 'success!'});
                }
            );
        });
    }
    
}

module.exports = Drive_temporaryTable;