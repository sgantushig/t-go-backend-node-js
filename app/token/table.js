const pool = require('../../databasePool');

class TokenTable {

    static storeToken( account_id, token ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Token( account_id, token )
                VALUES( $1, $2 )`,

                [account_id, token],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : TokenTable.storeToken");

                console.log("TokenTable.storeToken")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static updateToken( token, account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE token SET token = $1 WHERE account_id = $2`,

                [ token, account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : TokenTable.updateToken");

                console.log("TokenTable.updateToken")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static getToken( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Token where account_id = $1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : TokenTable.getToken");

                console.log("TokenTable.getToken")

                resolve(response.rows);
                }
            );
        });
    };

    static deleteToken( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE token SET token = null where account_id = $1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : TokenTable.deleteToken");

                console.log("TokenTable.deleteToken")

                resolve({ msg : "success" });
                }
            );
        });
    };
    
}

module.exports = TokenTable;