const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const accountRouter = require('./api/account');
const ordersRouter = require('./api/orders');
const adminRouter = require('./api/admin');
const driveRouter = require('./api/drive');
const voiceRouter = require('./api/voice');
const ordersTemporaryRouter = require('./api/ordersTemporary');
const ordersHistoryRouter = require('./api/ordersHistory');
const locationsRouter = require('./api/locations');
const drive_temporaryRouter = require('./api/drive_temporary');
const search_locationRouter = require('./api/search_location');
const plateRouter = require('./api/plate');
const ratingRouter = require('./api/rating');
const push_notificationRouter = require('./api/push_notification');


const app = express();

var path_archive =  __dirname + "/api/assets/"
var path_assets = __dirname + "/api/"
var path_icons = __dirname + "/api/assets/location_icons"
var path_pushNotif = __dirname + "/api/assets/push_notification"

app.use(cors({ origin: 'http://103.119.92.45:3100', credentials: true }));
app.use(express.static( path_archive ));
app.use(express.static( path_assets ));
app.use(express.static( path_icons ));
app.use(express.static( path_pushNotif ));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());

app.use('/account', accountRouter);
app.use('/admin', adminRouter);

app.use('/orders', ordersRouter);
app.use('/drive', driveRouter);
app.use('/voice', voiceRouter);
app.use('/ordersTemporary', ordersTemporaryRouter);
app.use('/ordersHistory', ordersHistoryRouter);

app.use('/locations', locationsRouter);
app.use('/drive_temporary', drive_temporaryRouter);
app.use('/search_location', search_locationRouter);
app.use('/plate', plateRouter);
app.use('/rating', ratingRouter);

app.use('/fcm', push_notificationRouter);



module.exports = app;