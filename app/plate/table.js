const pool = require('../../databasePool');

class PlateTable {

    static getAllPlate() {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Plate ORDER BY driver_id DESC`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : PlateTable.getAllPlate");

                console.log("PlateTable.getAllPlate");

                resolve(response.rows[0]);
                }
            );
        });
    };

    static storePlate( driver_id, driver_tel, plate_num, car_mark ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Plate ( driver_id, driver_tel, plate_num, car_mark ) 
                VALUES ($1, $2, $3, $4)`,

                [ driver_id, driver_tel, plate_num, car_mark ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : PlateTable.storePlate");

                console.log("PlateTable.storePlate");

                resolve({ msg: 'success!'});
                }
            );
        });
    };

    static getPlateById( driver_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Plate WHERE driver_id = $1 ORDER BY plate_id DESC`,

                [driver_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : PlateTable.getPlateById");

                console.log("PlateTable.getPlateById");

                resolve(response.rows);
                }
            );
        });
    };

    static updatePlateById( driver_id, plate_num, car_mark, ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Plate SET plate_num = $2, car_mark = $3 WHERE driver_id = $1`,

                [ driver_id, plate_num, car_mark ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : PlateTable.updatePlateById");

                console.log("PlateTable.updatePlateById");

                resolve({ msg: 'success!'});
                }
            );
        });
    };

    static deletePlateById( driver_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `DELETE FROM Plate WHERE driver_id = $1`,

                [ driver_id ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : PlateTable.deletePlateById");

                console.log("PlateTable.deletePlateById");

                resolve({ msg: 'success!'});
                }
            );
        });
    };

}

module.exports = PlateTable;