const pool = require('../../databasePool');

class RatingTable {

    static getAllRate() {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Rating ORDER BY driver_id DESC`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : RatingTable.getAllRate");

                console.log("RatingTable.getAllRate");

                resolve(response.rows[0]);
                }
            );
        });
    };

    static storeRate( driver_id , order_id , clean_car , cool_car , dj , nice_driver ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Rating ( driver_id, order_id, clean_car, cool_car, dj, nice_driver ) 
                VALUES ($1, $2, $3, $4, $5, $6)`,

                [ driver_id , order_id , clean_car , cool_car , dj , nice_driver ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : RatingTable.storeRate");

                console.log("RatingTable.storeRate");

                resolve({ msg: 'success!'});
                }
            );
        });
    };

    static getRatingById( driver_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Rating WHERE driver_id = $1 ORDER BY plate_id DESC`,

                [driver_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : RatingTable.getRatingById");

                console.log("RatingTable.getRatingById");

                resolve(response.rows);
                }
            );
        });
    };

    static updateRatingById( driver_id, clean_car , cool_car , dj , nice_driver ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Plate SET clean_car = $2, cool_car = $3, dj = $4, nice_driver = $5 WHERE driver_id = $1`,

                [ driver_id, clean_car , cool_car , dj , nice_driver ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : RatingTable.updateRatingById");

                console.log("RatingTable.updateRatingById");

                resolve({ msg: 'success!'});
                }
            );
        });
    };

    static deleteRatingById( driver_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `DELETE FROM Rating WHERE driver_id = $1`,

                [ driver_id ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : RatingTable.deleteRatingById");

                console.log("RatingTable.deleteRatingById");

                resolve({ msg: 'success!'});
                }
            );
        });
    };

}

module.exports = RatingTable;