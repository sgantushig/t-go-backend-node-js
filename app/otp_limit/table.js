const pool = require('../../databasePool');

class Otp_limitTable {

    static getAllOtpLimit() {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Otp_limit ORDER BY otp_id DESC`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.getAllOtpLimit");

                //console.log(response.rows,"get drive history wahahahha")

                resolve(response.rows);
                }
            );
        });
    };

    static getNumberLimit( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT otp_number FROM Otp_limit WHERE account_id = $1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.getOtpLimit");

                //console.log(response.rows,"get drive history wahahahha")

                resolve(response.rows[0]);
                }
            );
        });
    };

    static getOtpLimit( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Otp_limit WHERE account_id = $1 ORDER BY otp_id DESC LIMIT 1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.getOtpLimit");

                //console.log(response.rows,"get drive history wahahahha")

                resolve(response.rows[0]);
                }
            );
        });
    };

    static updateOtpLimit(otp_number , account_id) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Otp_limit SET otp_number = $1 WHERE account_id = $2`,

                [otp_number, account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.updateOtpLimit");

                console.log("Otp_limitTable updateOtpLimit")

                resolve({ msg: 'success!'});
                }
            );
        });
    };

    static updateLimitToZero( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Otp_limit SET otp_number = 0 WHERE account_id = $1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.updateLimitToZero");

                console.log("Otp_limitTable updateLimitToZero")

                resolve({ msg: 'success!'});
                }
            );
        });
    };

    static storeOtpLimit( account_id, otp_number ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Otp_limit ( account_id, otp_number )
                VALUES ($1, $2)`,

                [ account_id, otp_number ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.storeOtpLimit");

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static deleteOtpLimit( acc_id ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Otp_limit SET otp_number = 0 
                Where account_id = $1` ,

                [acc_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Otp_limitTable.deleteOtpLimit");

                resolve({ msg: 'success!'});
                }
            );
        });
    }
    
}

module.exports = Otp_limitTable;