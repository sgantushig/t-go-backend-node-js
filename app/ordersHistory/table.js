//*********************************** This is were the codes I wrote start. *****************************************************************************/

const pool = require('../../databasePool');

class OrdersHistoryTable {

    static storeOrdersHistory(       

        ordersId, 
        tel, 
        customerId, 
        driverId, 
        coordinatesInit,
        locationInit, 

        coordinatesInitDriver,
        locationInitDriver, 

        coordinatesDest, 
        locationDest,

        timestampInit, 
        timestampArrive, 

        timestampClosed, 
        timestampStarted, 
        timestampDrive, 

        timestampStopped, 
        timestampPaid, 

        status, 
        extra, 
        lastVoiceDriver, 
        description, 
        comment, 
        
        lastVoiceDriverTimestamp, 
        lastVoiceCustomer, 
        lastVoiceCustomerTimestamp
        
        ){

        return new Promise((resolve, reject) => {

            // console.log("TA-DA!!!!",ordersId, tel, customerId, driverId, coordinatesInit, coordinatesInitDriver, coordinatesDriver, coordinatesDest, 
            // coordinatesCurrent, timestampInit, timestampArrive, timestampWaiting, timestampClosed, timestampStarted, 
            // timestampDrive, timestampCurrent, timestampStopped, timestampBilling, timestampPayed, status, extra, lastVoiceDriver, 
            // description, comment, lastVoiceDriverTimestamp, lastVoiceCustomer, lastVoiceCustomerTimestamp)

            pool.query(
                `INSERT INTO OrdersHistory(
                    "ordersId", 
                    tel, 
                    "customerId", 
                    "driverId", 
                    "coordinatesInit",

                    "locationInit",
                    "coordinatesInitDriver",
                    "locationInitDriver",  

                    "coordinatesDest", 
                    "locationDest" ,

                    "timestampInit", 
                    "timestampArrive" , 

                    "timestampClosed" , 
                    "timestampStarted" , 

                    "timestampDrive" , 

                    "timestampStopped" , 
                    "timestampPaid", 

                    status, 
                    extra, 

                    "lastVoiceDriver", 
                    description, 
                    comment, 

                    "lastVoiceDriverTimestamp", 
                    "lastVoiceCustomer", 
                    "lastVoiceCustomerTimestamp"
                    )

                VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25) 
                `,

                [   ordersId, 
                    tel, 
                    customerId, 
                    driverId, 
                    coordinatesInit,
                    locationInit, 
            
                    coordinatesInitDriver,
                    locationInitDriver, 
            
                    coordinatesDest, 
                    locationDest,
            
                    timestampInit, 
                    timestampArrive, 
            
                    timestampClosed, 
                    timestampStarted, 
                    timestampDrive, 
            
                    timestampStopped, 
                    timestampPaid, 
            
                    status, 
                    extra, 
                    lastVoiceDriver, 
                    description, 
                    comment, 
                    
                    lastVoiceDriverTimestamp, 
                    lastVoiceCustomer, 
                    lastVoiceCustomerTimestamp
                ],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static getOrdersHistory(){

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM ordersHistory`,

                (error, response) => {
                if (error) return reject(error);

                resolve(response.rows);
                }
            );
        });
    }

}

module.exports = OrdersHistoryTable;

// *********************************** This is were the codes I wrote end. *****************************************************************************/
