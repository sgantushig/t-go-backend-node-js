const { Router } = require('express');
const router = new Router();

const OrdersTemporaryTable = require('../ordersTemporary/table');


router.get('/', (req, res, next) => {
    OrdersTemporaryTable.getOrdersTemporary()
      .then(({ordersTemporary}) => {
        res.json(ordersTemporary)
      })
      .catch(error => next(error));
});

router.post('/', (req, res, next) => {

  const { driverId, coordinatesCurrent, estimatedTime, minkm, status} = req.body;

  OrdersTemporaryTable.storeOrdersTemporary(
    driverId,
    coordinatesCurrent, 
    estimatedTime, 
    minkm,
    status
  ) .catch(error => next(error));
});


module.exports = router;