const { Router } = require('express');
const { hash } = require('../admin/helper');
const Session = require('../admin/session');
const { setSession, authenticatedAccount } = require('../admin/helper1');
const AccountTable = require('../admin/table');
const router = new Router();

router.post('/signup', (req, res, next) => {
  const { username, password } = req.body;
  console.log("orloo:",username,password);
  const usernameHash = hash(username);
  const passwordHash = hash(password);
  AccountTable.getAccount({ usernameHash })
    .then(({ account }) => {
      if (!account) {
        return AccountTable.storeAccount({ usernameHash, passwordHash })
      } else {
        const error = new Error('This username has been taken');
         error.statusCode = 409;
        throw error;
      }
    })
    .then(() => {
      return setSession({ username, res });
    })
    .then(({ message }) => res.json({ message }))
    .catch(error => next(error));
});

router.post('/login', (req, res, next) => {
  const { username, password } = req.body;
  console.log("orloo:",username,password);
  AccountTable.getAccount({ usernameHash: hash(username) })
    .then(({ account }) => {
     
      if (account && account.passwordhash === hash(password)) {
        const { sessionid } = account;

        return setSession({ username, res, sessionid });
      } else {
        const error = new Error('Incorrect username/password');
        error.statusCode = 409;
        throw error;
      }
    })
    .then(({ message }) => res.json({ message }))
    .catch(error => next(error));
});

router.get('/logout', (req, res, next) => {
  const { username } = Session.parse(req.cookies.sessionString);

  AccountTable.updateSessionId({
    sessionid: null,
    usernamehash: hash(username)
  }).then(() => {
      // removeSession({ res });
      res.clearCookie('sessionString');

      res.json({ message: 'Successful logout'});
    }).catch(error => next(error));
});

// router.get('/authenticated', (req, res, next) => {
//   console.log(req.cookies);
//   authenticatedAccount({ sessionString: req.cookies.sessionString })
//     .then(({ authenticated }) => {
//       res.json({ authenticated });
//     })
//     .catch(error => next(error));
// });

router.get('/authenticated', (req, res) => {
  console.log(req.cookies);
  authenticatedAccount({ sessionString: req.cookies.sessionString })
    .then(({ authenticated }) => {
      res.json({ authenticated });
    })
    .catch(error => res.json(error));
});

router.get('/info', (req, res, next) => {

  // authenticatedAccount({ sessionString: req.cookies.sessionString })
  //   .then(({ account, username }) => {
  //     res.json({ info: { username, balance: account.balance } });
  //   })
  //   .catch(error => next(error));
})

module.exports = router;
