const { Router } = require('express');
const router = new Router();

const { authenticatedAccount } = require('./helper');
const RatingTable = require('../rating/table');


router.get('/', (req, res, next) => {

    RatingTable.getAllRate()
      .then(({ratings}) => {
        res.json(ratings)
      })
      .catch(error => next(error));
});

router.post('/store', (req, res, next) => {

  const { token, order_id, clean_car , cool_car , dj , nice_driver } = req.body;

  console.log( "incomings : ", clean_car , cool_car , dj , nice_driver );

  authenticatedAccount(token)
  .then(({ account }) => {

    RatingTable.storeRate( account.id , order_id , clean_car , cool_car , dj , nice_driver )
    .then(() => {

        res.json({
            "msg" : "success"
        })
        
    }) .catch(error => next(error));

  }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

});

router.post('/update', (req, res, next) => {

    const { token, plate_num, car_mark} = req.body;

    console.log( "incomings : ", plate_num, car_mark );

    authenticatedAccount(token)
    .then(({ account }) => {

        PlateTable.updatePlateById(account.id, plate_num, car_mark)
        .then(() => {

            PlateTable.getPlateById(account.id)
            .then(( plate_info ) => {

                console.log("getPlateById", plate_info)

                if( plate_info !== undefined) {

                    res.json({
                        "plate_info" : plate_info
                    })

                } else {

                    res.json({
                        "plate_info" : []
                    })

                }

            }) .catch(error => next(error));
            
        }) .catch(error => next(error));

    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));
  
});
  
router.post('/get', (req, res, next) => {

    const { token } = req.body;
  
    authenticatedAccount(token)
    .then(({ account }) => {
  
        PlateTable.getPlateById(account.id)
        .then(( plate_info ) => {

            console.log("getPlateById", plate_info)

            if( plate_info !== undefined) {

                res.json({
                    "plate_info" : plate_info
                })

            } else {

                res.json({
                    "plate_info" : []
                })

            }

        }) .catch(error => next(error));
  
    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));
  
});

router.post('/delete', (req, res, next) => {

    const { token } = req.body;
  
    authenticatedAccount(token)
    .then(({ account }) => {

        PlateTable.deletePlateById( account.id )
        .then(() => {

            PlateTable.getPlateById(account.id)
            .then(( plate_info ) => {
    
                console.log("getPlateById", plate_info)
    
                if( plate_info !== undefined) {
    
                    res.json({
                        "plate_info" : plate_info
                    })
    
                } else {
    
                    res.json({
                        "plate_info" : []
                    })
    
                }
    
            }) .catch(error => next(error));

        }) .catch(error => next(error))
  
    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));
  
});
  

module.exports = router;