const { Router } = require('express');
const { authenticatedAccount } = require('./helper');

const multer = require('multer');
const fs = require('fs');

const VoiceTable = require('../voice/table');
const OrdersTable = require('../orders/table');

const router = new Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __dirname + '/assets/tmp/')

    },
    filename: (req, file, cb) => {
        cb(null, 'voice.mp3')
    }
});
const upload = multer({ storage: storage });


router.get('/', (req, res, next) => {

    VoiceTable.getVoice()
    .then((voice) => {
        res.json( voice );
    })
    .catch(error => next(error));
});

router.post('/upload', upload.fields([{name: 'file', maxCount: 1}, {name: 'info', maxCount: 1}]), (req, res, next) => {

    const { token } = JSON.parse( req.body.info );

    var timestamp = new Date();
    var time = timestamp.getFullYear() + "-" + timestamp.getMonth() + "-" + timestamp.getDate() + "-" + timestamp.getHours() + "." + timestamp.getMinutes() + "." + timestamp.getSeconds() + "." + timestamp.getMilliseconds();

    authenticatedAccount(token)
    .then(({ account }) => {

        if(account.driver){

            OrdersTable.getClientIdByDriver(account.id)
            .then(({customerId}) => {

                if ( customerId !== null || customerId !== undefined) {

                    OrdersTable.getOrderIdByDriver(account.id)
                    .then(({ id }) => {
    
                        if(id) {
    
                            var voicePath = ('http://103.119.92.45:3000/archive/' + 'D' + account.id + '-C' + customerId + "-" + time + '.mp3');
    
                            console.log("voice - upload : driver", account.id, timestamp, id);
                            VoiceTable.storeVoice(
        
                                customerId,             //customerId 
                                account.id,             //driverId                        
                                voicePath,              //voicePath
                                timestamp,              //timestamp
                                id,                     //orderId
                                customerId              //voiceFile
        
                            ) .then(() => {
    
                                fs.copyFile(__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/archive/' + 'D' + account.id + '-C' + customerId + "-" + time + '.mp3', (err) => {
                                    if (err) throw err;
                                    console.log('source was copied to archive : driver');
    
                                    });
    
                                    setTimeout(() => {
    
                                    fs.unlinkSync(__dirname + '/assets/tmp/voice.mp3', (err) => {
                                        if (err) throw err;
                                        console.log('successfully deleted /tmp/voice');
                                    });
    
                                    }, 10)
    
                                res.json({"msg" : "success"});
        
                            }) .catch((error) => next(error))
    
                        } else {
    
                            res.json({
                                "order" : null
                            })
                            
                        }
    
                    }) .catch((error) => next(error))

                } else {

                    res.json({
                        "order" : null
                    })

                }

            }) .catch((error) => next(error))

        } else {

            OrdersTable.getDriverIdByClient(account.id)
            .then(({ driverId }) => {

                if ( driverId !== null || driverId !== undefined) {

                    OrdersTable.getOrderIdByClient(account.id)
                    .then(( { id } ) => {
    
                        if(id) {
    
                            var voicePath = ('http://103.119.92.45:3000/archive/' + 'C' + account.id  + '-D' + driverId + "-" + time + '.mp3');
        
                            console.log("voice - upload : driver", account.id, timestamp, id);
                            VoiceTable.storeVoice(
    
                                account.id,             //customerId   
                                driverId,               //driverId 
                                voicePath,              //voicePath
                                timestamp,              //timestamp
                                id,                     //orderId
                                driverId               //voiceFile
        
                            ) .then(() => {
    
                                fs.copyFile(__dirname + '/assets/tmp/voice.mp3', __dirname + '/assets/archive/' + 'C' + account.id  + '-D' + driverId  + "-" + time + '.mp3', (err) => {
                                    if (err) throw err;
                                    console.log('source was copied to archive : custumer' );
    
                                });
    
                                    setTimeout(() => {
    
                                    fs.unlinkSync(__dirname + '/assets/tmp/voice.mp3', (err) => {
                                        if (err) throw err;
                                        console.log('successfully deleted /tmp/voice : customer');
                                    });
    
                                    }, 10)
    
                                res.json({"msg" : "success"});
        
                            }) .catch((error) => next(error))
    
                            
                        } else {
    
                            res.json({
                                "order" : null
                            })
                        }
    
                    }) .catch(error => next(error))

                } else {

                    res.json({
                        "order" : null
                    })

                }

            }) .catch((error) => next(error))
        
        }
        
    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

});

module.exports = router;