const { Router } = require('express');
const multer = require('multer');
const fs = require('fs');
const axios = require('axios');
const geolib = require('geolib');
const uuidv1 = require('uuid/v1');

const NumberGenerator = require('../api/assets/test');
const AccountTable = require('../account/table');
const Drive_historyTable = require('../drive_history/table');  
const Otp_limitTable = require('../otp_limit/table');

const { hash } = require('../account/helper');
const { setSession, deleteSessionToken, authenticatedAccount } = require('./helper');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __dirname + '/assets/tmp/')

  },
  filename: (req, file, cb) => {
    cb(null, 'avatar.png')
  }
});

const upload = multer({ storage: storage });

const router = new Router();

router.get('/', (req, res, next) => {

  AccountTable.getAccountsTable()
  .then((accounts) => {

    res.json(accounts)

  }) .catch(error => next(error));

});

router.post('/login', (req, res, next) => {
  const { tel, rand_pin, driver, pin, coordinates } = req.body;

  var min = 1000;
  var max = 9999;

  var status = 'FREE'

  AccountTable.getAccount(tel)
    .then(({ account }) => {

      if (!account) {

        AccountTable.storeAccount({ tel, driver, status })
        .then(() => {

          NumberGenerator.getRndInteger1({ min, max })
          .then(({ the_code }) => {

            var the_same_text = "T-Go verification code : " + the_code;

            var phone = tel;
            var link = 'http://web2sms.skytel.mn/apiSend?token=52c95c0165e17b111400a317fe0751ea1c4472b5&sendto=' + phone + '&message=' + the_same_text + '&fbclid=IwAR2iH-AtJGiLn5lOsevsheswakjoeG4nPgRHGLygDN51A_BW6n9ImLKAaoA';

            randHashPin = hash(the_code);

            console.log("the_code_1 && randHashPin", the_code, randHashPin);

            AccountTable.updateAccountRand(the_code, randHashPin, tel)
            .then(() => {

              AccountTable.updateAccountPin(randHashPin, tel)
              .catch(error => next(error));

              AccountTable.getAccount(phone)
              .then(({ account }) => {

                randPin = account.randPin;
                randHashPin1 = account.randHashPin;

                console.log("account.randPin", randPin);
                console.log("account.randHashPin", randHashPin1);
                console.log("the_account", account);

                  axios.get(link)
                  .then(response => {
                    console.log(response.data.url);
                    console.log(response.data.explanation);

                    res.json({
                      "the_code": the_same_text,
                      "test_randPin": randPin,
                      "test_randHashPin": randHashPin1,
                      "msg" : "success",
                      "status" : "signed up & sent the OTP",
                      "otp" : true
                    });

                  }) .catch(error => next(error));

                }) .catch(error => next(error));

              }) .catch(error => next(error));

            }) .catch(error => next(error));

          }) .catch(error => next(error));

      } else if(account) { //************ if there's a registered account.***********************/

        if (account.password) { //************* if the registered account has a password set ***************/

          if (account && pin) { 

            var randPin = account.randPin;
            var randHashPin1 = account.randHashPin;
  
            var pinHash = hash(pin);
            var id = account.id;
  
            console.log("account && pin", account.pinHash, pinHash);
  
              if (account.pinHash == pinHash) {

                AccountTable.updateDriverStatus( driver, account.id)
                .then(() => {

                  AccountTable.updateStatus( status, account.id )
                  .then(() => {

                    console.log("the account with the pinHash", account, account.pinHash);
                    console.log("pin && pinHash", pin, pinHash);
        
                    return setSession({ id, tel, pinHash, res, driver })

                  }) .catch(error => next(error))

                }) .catch(error => next(error))
            
              } else if (account.pinHash != pinHash) {
    
                console.log("wrong pin", account.pinHash,"pin", pin)
                res.json({
                  "error":"wrong pin"
                })
              };
          } else {

            res.json({
              "otp" : false,
              "instructions": "now you have to send the pin"
            })

          }

        } else if (account.password == false) { //************* if the registered account hasn't a password set yet ***************/

          if (rand_pin) { //************************ if the account has send rand_pin ************************************/

            if (account.randHashPin == hash(rand_pin)) { //*********************** if the sent rand_pin is equal to the stored randHashPin *************************/

              pinHash = hash(pin);
              randHashPin = hash(rand_pin);

              var id = account.id;
      
              AccountTable.updateAccountPin(randHashPin, tel)
              .then(() => {

                AccountTable.updateDriverStatus(driver, account.id)
                .then(() => {

                  AccountTable.nullRandHashPin(account.id)
                  .then(() => {

                    AccountTable.updateStatus(status, account.id)
                    .then(() => {
    
                      console.log("the account and randpin", account, rand_pin, hash(rand_pin))
                      console.log("the pinhash && coordinates", pinHash, coordinates)
    
                      return setSession({ id, tel, pinHash, res, driver })
    
                    }) .catch(error => next(error));
      
                  }) .catch(error => next(error));
  
                }) .catch(error => next(error));

              }) .catch(error => next(error));
              
            } else if (account.randHashPin == null) { //*********************** if the account's randHashPin is null *************************/

              Otp_limitTable.getNumberLimit(account.id)
              .then(( otp_limit ) => {

                console.log("otp_limit ()()()()()()()()()()()()", otp_limit)

                if ( otp_limit !== undefined ) {  //*********************** if there's a record of otp_limit ************************/

                  var otpNumber = otp_limit.otp_number + 1;

                  console.log("this is the number of otp_limit" , otp_limit.otp_number, otpNumber)

                  Otp_limitTable.updateOtpLimit(otpNumber, account.id)
                  .then(() => {

                    Otp_limitTable.getNumberLimit(account.id)
                    .then(( otp_limit ) => {

                      console.log("this is the number" , otp_limit.otp_number)

                      if ( !(otp_limit.otp_number <= 2) ) { //*********************** if the account's otp_number is more than 3 *************************/

                        res.json({
                          "error" : "your limit of receiving otp has been reached. Wait for few minutes to get your otp nubmer."
                        })

                      } else {  //*********************** if the account's otp_number is not more than 3 *************************/

                        NumberGenerator.getRndInteger1({ min, max })
                        .then(({ the_code }) => {
            
                          var the_same_text = "T-Go verification code : " + the_code;
                          
                          var phone = tel;
                          var link = 'http://web2sms.skytel.mn/apiSend?token=52c95c0165e17b111400a317fe0751ea1c4472b5&sendto=' + phone + '&message=' + the_same_text + '&fbclid=IwAR2iH-AtJGiLn5lOsevsheswakjoeG4nPgRHGLygDN51A_BW6n9ImLKAaoA';
            
                          randHashPin = hash(the_code);
                          pinHash = hash(pin);
            
                          AccountTable.updateAccountRand(the_code, randHashPin, tel)
                          .then(() => {
            
                            AccountTable.updateAccountPin(randHashPin, tel)
                            .catch(error => next(error));
            
                            AccountTable.getAccount(phone)
                            .then(({ account }) => {
            
                              var randPin = account.randPin;
                              var randHashPin1 = account.randHashPin;
            
                              console.log("account.randPin", randPin);
                              console.log("account.randHashPin", randHashPin1);
                              console.log("the_account", account);
            
                              axios.get(link)
                              .then(response => {
            
                                console.log(response.data.url);
                                console.log(response.data.explanation);
            
                                res.json({
                                  "the_code": the_same_text,
                                  "test_randPin": randPin,
                                  "test_randHashPin": randHashPin1,
                                  "msg" : "success",
                                  "status" : "sent the OTP",
                                  "otp" : true
                                });
            
                              }).catch(error => next(error));
            
                            }).catch(error => next(error));
            
                          }).catch(error => next(error));
                            
                        }) .catch(error => next(error))

                      }

                      setTimeout(() => {

                        Otp_limitTable.updateLimitToZero(account.id)
                        .catch(error => next(error))

                        console.log("zero-fied the number_limit");

                      }, 100000);

                      console.log("otp_limit inside of if statement /*-/*-/*-/*-/*-/*-/*-", otp_limit)

                    }) .catch(error => next(error))

                  }) .catch(error => next(error));

                } else { //*********************** if there isn't a record of otp_limit ************************/

                  Otp_limitTable.storeOtpLimit(account.id, 1)
                  .catch(error => next(error));

                }

              }) .catch(error => next(error))

            } else if (account.randHashPin != hash(rand_pin)) { //*********************** if the sent rand_pin is not equal to the stored randHashPin *************************/
  
              res.json({
  
                "error":"wrong one time password"
            
              })
      
            }

          } else { //*********************** if the account did not send the rand_pin *************************/

            Otp_limitTable.getNumberLimit(account.id)
            .then(( otp_limit ) => {

              console.log("otp_limit ()()()()()()()()()()()()", otp_limit)

              if ( otp_limit !== undefined ) {

                var otpNumber = otp_limit.otp_number + 1;

                console.log("this is the number of otp_limit" , otp_limit.otp_number, otpNumber)

                Otp_limitTable.updateOtpLimit(otpNumber, account.id)
                .then(() => {

                  Otp_limitTable.getNumberLimit(account.id)
                  .then(( otp_limit ) => {

                    console.log("this is the number" , otp_limit.otp_number)

                    if ( !(otp_limit.otp_number <= 2) ) {

                      res.json({
                        "error" : "your limit of receiving otp has been reached. Wait for few minutes to get your otp nubmer."
                      })

                    } else {

                      NumberGenerator.getRndInteger1({ min, max })
                      .then(({ the_code }) => {
          
                        var the_same_text = "T-Go verification code : " + the_code;
                        
                        var phone = tel;
                        var link = 'http://web2sms.skytel.mn/apiSend?token=52c95c0165e17b111400a317fe0751ea1c4472b5&sendto=' + phone + '&message=' + the_same_text + '&fbclid=IwAR2iH-AtJGiLn5lOsevsheswakjoeG4nPgRHGLygDN51A_BW6n9ImLKAaoA';
          
                        randHashPin = hash(the_code);
                        pinHash = hash(pin);
          
                        AccountTable.updateAccountRand(the_code, randHashPin, tel)
                        .then(() => {
          
                          AccountTable.updateAccountPin(randHashPin, tel)
                          .catch(error => next(error));
          
                          AccountTable.getAccount(phone)
                          .then(({ account }) => {
          
                            var randPin = account.randPin;
                            var randHashPin1 = account.randHashPin;
          
                            console.log("account.randPin", randPin);
                            console.log("account.randHashPin", randHashPin1);
                            console.log("the_account", account);
          
                            axios.get(link)
                            .then(response => {
          
                              console.log(response.data.url);
                              console.log(response.data.explanation);
          
                              res.json({
                                "the_code": the_same_text,
                                "test_randPin": randPin,
                                "test_randHashPin": randHashPin1,
                                "msg" : "success",
                                "status" : "sent the OTP",
                                "otp" : true
                              });
          
                            }).catch(error => next(error));
          
                          }).catch(error => next(error));
          
                        }).catch(error => next(error));
                          
                      }) .catch(error => next(error))

                    }

                    setTimeout(() => {

                      Otp_limitTable.updateLimitToZero(account.id)
                      .catch(error => next(error))

                      console.log("zero-fied the number_limit");

                    }, 100000);

                    console.log("otp_limit inside of if statement /*-/*-/*-/*-/*-/*-/*-", otp_limit)

                  }) .catch(error => next(error))

                }) .catch(error => next(error));

              } else {

                Otp_limitTable.storeOtpLimit(account.id, 1)
               .catch(error => next(error));

              }

            }) .catch(error => next(error))

          }

        }

      }

    }).catch(error => next(error));

});

router.post('/logout', (req, res, next) => {

  const token = req.body

  console.log("the log out route : ", token, req.body);
  
  deleteSessionToken({ res, token })

});

router.post('/otp', (req, res, next) => {

  const tel = req.body

  NumberGenerator.getRndInteger1({ min, max })
  .then(({ the_code }) => {

    var the_same_text = "T-Go verification code : " + the_code;

    var phone = tel;
    var link = 'http://web2sms.skytel.mn/apiSend?token=52c95c0165e17b111400a317fe0751ea1c4472b5&sendto=' + phone + '&message=' + the_same_text + '&fbclid=IwAR2iH-AtJGiLn5lOsevsheswakjoeG4nPgRHGLygDN51A_BW6n9ImLKAaoA';

    randHashPin = hash(the_code);

    console.log("the_code_1 && randHashPin", the_code, randHashPin);

    AccountTable.updateAccountRand(the_code, randHashPin, tel)
    .then(() => {

      AccountTable.updateAccountPin(randHashPin, tel)
      .catch(error => next(error));

      AccountTable.getAccount(phone)
      .then(({ account }) => {

        randPin = account.randPin;
        randHashPin1 = account.randHashPin;

        console.log("account.randPin", randPin);
        console.log("account.randHashPin", randHashPin1);
        console.log("the_account", account);

        axios.get(link)
        .then(response => {
          console.log(response.data.url);
          console.log(response.data.explanation);

          res.json({
            "the_code": the_same_text,
            "test_randPin": randPin,
            "test_randHashPin": randHashPin1,
            "msg" : "success",
            "status" : "signed up & sent the OTP",
            "otp" : true
          });

        }) .catch(error => next(error));

      }) .catch(error => next(error));
        
    }) .catch(error => next(error));

  }) .catch(error => next(error));

});

router.post('/profile', upload.fields([{ name: 'file', maxCount: 1 }, { name: 'info', maxCount: 1 }]), (req, res, next) => {

  const { token, pin, name, action } = JSON.parse(req.body.info);

  authenticatedAccount(token)
  .then(({ account }) => {
    switch (action) {
      case 'get':

        res.json({
          "account" : {
              "id" : account.id,
              "tel" : account.tel,
              "name" : account.name,
              "avatar" : account.avatar,
              "qrCode" : account.qrCode,
              "driver" : account.driver,
              "timestamp" : account.timestamp,
              "startCoordinates" : JSON.parse(account.startCoordinates),
              "endCoordinates" : JSON.parse(account.endCoordinates),
              "password" : account.password,
              "online" : account.online,
              "status" : account.status
              
          }
        });

      break;

      case 'setAvatar':

        // ********************************************************************************************************//
        var avatar_name = account.tel + "_" + uuidv1();

        var avatar_url = "http://103.119.92.45:3000/assets/" + avatar_name + ".png";

        console.log("account - driver :", account.avatar);

        if (account.avatar !== null) {

          var avatar_str = account.avatar.replace('http://103.119.92.45:3000/assets/', '');

          fs.unlink(__dirname + '/assets/' + avatar_str, (error) => {
            if (error) {
              throw error
            } else {

              console.log("unlink success");

            };
          })

        };

        fs.copyFile(__dirname + '/assets/tmp/avatar.png', __dirname + '/assets/' + avatar_name + '.png', (error) => {
          if (error) {
            throw error
          } else {
            console.log("copyFile success");

            fs.unlink(__dirname + '/assets/tmp/avatar.png', (error) => {
              if (error) {
                throw error
              } else {

                console.log("unlink success");

                AccountTable.updateAvatar(avatar_url, account.tel)
                .then(()=>{

                  AccountTable.getAccount(account.tel)
                  .then(({account}) => {

                    var pinHash = hash(pin);
                    var password = true;
            
                    if(pin && name) {
            
                      console.log("---------------------------------------------name  and pin is coming")
            
                      AccountTable.updateAccount(pinHash, password, pinHash, name, account.tel)
                      .then(()=>{
              
                        AccountTable.getAccount(account.tel)
                        .then(({account}) => {
                                  
                          res.json({
                            "account" : {
                                "id" : account.id,
                                "tel" : account.tel,
                                "name" : account.name,
                                "avatar" : account.avatar,
                                "qrCode" : account.qrCode,
                                "driver" : account.driver,
                                "timestamp" : account.timestamp,
                                "startCoordinates" : JSON.parse(account.startCoordinates),
                                "endCoordinates" : JSON.parse(account.endCoordinates),
                                "password" : account.password,
                                "online" : account.online,
                                "status" : account.status
              
                            }
                          })
              
                        }) .catch(error => next(error));
              
                      }) .catch((error) => console.log(error));
                      
                    } else if(pin && !name) {
            
                      console.log("--------------------------------------------only pin is coming")
            
                      AccountTable.updateAccountPassHash( pinHash, password, pinHash, account.tel)
                      .then(() => {
            
                        AccountTable.getAccount(account.tel)
                        .then(({account}) => {
                                  
                          res.json({
                            "account" : {
                                "id" : account.id,
                                "tel" : account.tel,
                                "name" : account.name,
                                "avatar" : account.avatar,
                                "qrCode" : account.qrCode,
                                "driver" : account.driver,
                                "timestamp" : account.timestamp,
                                "startCoordinates" : JSON.parse(account.startCoordinates),
                                "endCoordinates" : JSON.parse(account.endCoordinates),
                                "password" : account.password,
                                "online" : account.online,
                                "status" : account.status
              
                            }
                          })
              
                        }) .catch(error => next(error));
            
                      }) .catch(error => next(error));
            
                    } else if (name && !pin) {
            
                      console.log("------------------------------------------------------only name is coming")
            
                      AccountTable.updateAccountName(name, account.tel)
                      .then(() => {
            
                        AccountTable.getAccount(account.tel)
                        .then(({account}) => {
                                  
                          res.json({
                            "account" : {
                                "id" : account.id,
                                "tel" : account.tel,
                                "name" : account.name,
                                "avatar" : account.avatar,
                                "qrCode" : account.qrCode,
                                "driver" : account.driver,
                                "timestamp" : account.timestamp,
                                "startCoordinates" : JSON.parse(account.startCoordinates),
                                "endCoordinates" : JSON.parse(account.endCoordinates),
                                "password" : account.password,
                                "online" : account.online,
                                "status" : account.status
              
                            }
                          })
              
                        }) .catch(error => next(error));
            
                      }) .catch(error => next(error));
            
                    } else {

                      res.json({
                        "account" : {
                            "id" : account.id,
                            "tel" : account.tel,
                            "name" : account.name,
                            "avatar" : avatar_url,
                            "qrCode" : account.qrCode,
                            "driver" : account.driver,
                            "timestamp" : account.timestamp,
                            "startCoordinates" : JSON.parse(account.startCoordinates),
                            "endCoordinates" : JSON.parse(account.endCoordinates),
                            "password" : account.password,
                            "online" : account.online,
                            "status" : account.status
          
                        }

                      });

                    }
                            
                  }) .catch(error => next(error));

                  console.log("account.profile - update IF AVATAR!!! ");

                }) .catch(error => next(error));

              };

            });

          };

        });

        //******************************************************************************************************************************/



      break;

      case 'setOther':

        var pinHash = hash(pin);
        var password = true;

        if(pin && name) {

          console.log("---------------------------------------------name  and pin is coming")

          AccountTable.updateAccount(pinHash, password, pinHash, name, account.tel)
          .then(()=>{

            AccountTable.getAccount(account.tel)
            .then(({account}) => {
                      
              res.json({
                "account" : {
                    "id" : account.id,
                    "tel" : account.tel,
                    "name" : account.name,
                    "avatar" : account.avatar,
                    "qrCode" : account.qrCode,
                    "driver" : account.driver,
                    "timestamp" : account.timestamp,
                    "startCoordinates" : JSON.parse(account.startCoordinates),
                    "endCoordinates" : JSON.parse(account.endCoordinates),
                    "password" : account.password,
                    "online" : account.online,
                    "status" : account.status

                }
              })

            }) .catch(error => next(error));

          }) .catch((error) => console.log(error));
          
        } else if(pin && !name) {

          console.log("--------------------------------------------only pin is coming")

          AccountTable.updateAccountPassHash( pinHash, password, pinHash, account.tel)
          .then(() => {

            AccountTable.getAccount(account.tel)
            .then(({account}) => {
                      
              res.json({
                "account" : {
                    "id" : account.id,
                    "tel" : account.tel,
                    "name" : account.name,
                    "avatar" : account.avatar,
                    "qrCode" : account.qrCode,
                    "driver" : account.driver,
                    "timestamp" : account.timestamp,
                    "startCoordinates" : JSON.parse(account.startCoordinates),
                    "endCoordinates" : JSON.parse(account.endCoordinates),
                    "password" : account.password,
                    "online" : account.online,
                    "status" : account.status

                }
              })

            }) .catch(error => next(error));

          }) .catch(error => next(error));

        } else if (name && !pin) {

          console.log("------------------------------------------------------only name is coming")

          AccountTable.updateAccountName(name, account.tel)
          .then(() => {

            AccountTable.getAccount(account.tel)
            .then(({account}) => {
                      
              res.json({
                "account" : {
                    "id" : account.id,
                    "tel" : account.tel,
                    "name" : account.name,
                    "avatar" : account.avatar,
                    "qrCode" : account.qrCode,
                    "driver" : account.driver,
                    "timestamp" : account.timestamp,
                    "startCoordinates" : JSON.parse(account.startCoordinates),
                    "endCoordinates" : JSON.parse(account.endCoordinates),
                    "password" : account.password,
                    "online" : account.online,
                    "status" : account.status

                }
              })

            }) .catch(error => next(error));

          }) .catch(error => next(error));

        }

      break;

    }

  }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

});

router.post('/forgot', (req, res, next) => {

  const { tel } = req.body;

  var min = 1000;
  var max = 9999;

  AccountTable.forgotAccount(tel)
  .then(() => {

    NumberGenerator.getRndInteger1({ min, max })
    .then(({ the_code }) => {

      var the_same_text = "T-Go verification code : " + the_code;
      
      var phone = tel;
      var link = 'http://web2sms.skytel.mn/apiSend?token=52c95c0165e17b111400a317fe0751ea1c4472b5&sendto=' + phone + '&message=' + the_same_text + '&fbclid=IwAR2iH-AtJGiLn5lOsevsheswakjoeG4nPgRHGLygDN51A_BW6n9ImLKAaoA';

      randHashPin = hash(the_code);

      AccountTable.updateAccountRand(the_code, randHashPin, tel)
      .then(() => {

        AccountTable.updateAccountPin(randHashPin, tel)
        .catch(error => next(error));

        AccountTable.getAccount(phone)
        .then(({ account }) => {

          var randPin = account.randPin;
          var randHashPin1 = account.randHashPin;

          console.log("account.randPin", randPin);
          console.log("account.randHashPin", randHashPin1);
          console.log("the_account", account);

          axios.get(link)
          .then(response => {
            console.log(response.data.url);
            console.log(response.data.explanation);

            res.json({
              "the_code": the_same_text,
              "test_randPin": randPin,
              "test_randHashPin": randHashPin1,
              "msg" : "success",
              "status" : "sent the OTP"
            });

          }) .catch(error => next(error));

        }) .catch(error => next(error));

      }) .catch(error => next(error));
        
    }) .catch(error => next(error));

  }) .catch(error => next(error));
  
});

router.post('/taxi', (req, res, next) => {

  const {token} = req.body;

  authenticatedAccount(token)
  .then(({ account }) => {

    var the_coordinates = JSON.parse(account.startCoordinates);

    // var something = account.startCoordinates.replace('lat', '"lat"');
    // var some_thing = something.replace('lng', '"lng"');
    //console.log("at least auth is working fine.", something)
    //var the_coordinates = JSON.parse(some_thing)

    var the_thing = typeof(the_coordinates)

    if (the_thing == 'object') {

      console.log("the thing is JSON")

      Drive_historyTable.selectFromBoth()
      .then(({ las_record }) => {

        //console.log(" account.taxi route : ", las_record)

        const driver = las_record.map((taxi) => {
          var cctv = JSON.parse(taxi.coordinates)
          return {
            id : taxi.accountId,         
            coordinates2 : JSON.parse(taxi.coordinates),
            coordinates1 : JSON.parse(taxi.lastCoordinates),
            online : taxi.online,
            status : taxi.status,
            driver : taxi.driver,
            timestamp : taxi.timestamp,
            plate_num : taxi.plate_num,
            car_mark : taxi.car_mark,
            meter : geolib.getDistance(
              { latitude: the_coordinates.lat, longitude: the_coordinates.lng },
              { latitude: cctv.lat, longitude: cctv.lng }
            )
          }
        })
        
        //console.log("taxi route drivers : ", driver)

        const result = driver.filter(driver => driver.meter <= 2000);
  
        res.json({ drivers : result });

      }) .catch(error => next(error));

    } else if (the_thing == 'string') {

      res.json({ error : "not a valid type."})
      
    } 

  }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));
  
});

module.exports = router;