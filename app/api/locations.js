const { Router } = require('express');
const axios = require('axios');

const { authenticatedAccount } = require('./helper');
const LocationsTable = require('../locations/table');

const router = new Router();


router.get('/', (req, res, next) => {

    LocationsTable.getLocations()
    .then((locations) => {
  
      res.json(locations)
  
    }) .catch(error => next(error));
  
});

router.post('/store', (req, res, next) => {

    const { coordinates, token, location_names, location_icon } = req.body;
    const key = "AIzaSyBJ7WDbOTG6J1ah6gALgmoNzK9Mp9n2uX4"

    const nates = JSON.stringify(coordinates);
    const coord = JSON.parse(nates);
    var latlng = coord.lat + "," + coord.lng;

    if(location_icon == "office") {
         loc_icon = "http://103.119.92.45:3000/location_icons/office_icon.png";
    } else if (location_icon == "home") {
         loc_icon = "http://103.119.92.45:3000/location_icons/home_icon.png";
    } else if (location_icon == "other") {
         loc_icon = "http://103.119.92.45:3000/location_icons/map_marker_icon.png";
    } else if (location_icon == "restaurant") {
         loc_icon = "http://103.119.92.45:3000/location_icons/restaurant_icon.png";
    } else if (location_icon == "gym") {
         loc_icon = "http://103.119.92.45:3000/location_icons/dumbbell_icon.png";
    } else {
        res.json({
            "error" : "undefined map marking."
        })
    };

    authenticatedAccount(token)
    .then(({ account }) => {

        axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${key}`)
        .then(function (response) {
            
            console.log("the data we WANT", response.data.results[0].formatted_address);

            var coordinates_names = response.data.results[0].formatted_address;

            LocationsTable.storeLocations( account.id, account.tel, location_names, coordinates_names, coordinates, loc_icon )
            .then(() => {

                LocationsTable.getLocationsAll(account.id)
                .then(( locations ) => {

                    const all_locations = locations.map((taxi) => {

                        return {
                            location_id : taxi.location_id,
                            acc_id : taxi.acc_id,
                            location_coordinates : JSON.parse(taxi.location_coordinates),
                            acc_tel : taxi.acc_tel,
                            location_names : taxi.location_names,
                            coordinate_names : taxi.coordinate_names,
                            location_icon : taxi.location_icon
                        }
                    })

                    res.json({ 
                        "msg" : "success",
                        "locations" : all_locations
                    });

                }) .catch(error => next(error));

            }) .catch(error => next(error));

        }) .catch(error => next(error));

    }) .catch(error => next(error));
  
});

router.post('/get', (req, res, next) => {

    const { token, location_names } = req.body;

    authenticatedAccount(token)
    .then(({ account }) => {

        if ( location_names == "all" ) {

            LocationsTable.getLocationsAll( account.id )
            .then(( locations ) => {

                console.log(locations);
    
                const all_locations = locations.map((taxi) => {
    
                    return {
                        location_id : taxi.location_id,
                        acc_id : taxi.acc_id,
                        location_coordinates : JSON.parse(taxi.location_coordinates),
                        acc_tel : taxi.acc_tel,
                        location_names : taxi.location_names,
                        coordinate_names : taxi.coordinate_names,
                        location_icon : taxi.location_icon
                    }
                })
    
                res.json({ 
                    "msg" : "success",
                    "locations" : all_locations
                });

            }) .catch(error => next(error));

        } else {

            LocationsTable.getLocationsByNames(account.id, location_names)
            .then(( locations ) => {
    
                console.log(locations);
    
                const all_locations = locations.map((taxi) => {
    
                    return {
                        location_id : taxi.location_id,
                        acc_id : taxi.acc_id,
                        location_coordinates : JSON.parse(taxi.location_coordinates),
                        acc_tel : taxi.acc_tel,
                        location_names : taxi.location_names,
                        coordinate_names : taxi.coordinate_names,
                        location_icon : taxi.location_icon
                    }
                })
    
                res.json({ 
                    "msg" : "success",
                    "locations" : all_locations
                });
    
            }) .catch(error => next(error));

        }

    }) .catch(error => next(error));
  
});

router.post('/update', (req, res, next) => {

    const { coordinates, token, location_names, location_icon, location_id } = req.body;
    const key = "AIzaSyBJ7WDbOTG6J1ah6gALgmoNzK9Mp9n2uX4"

    const nates = JSON.stringify(coordinates);
    const coord = JSON.parse(nates);
    var latlng = coord.lat + "," + coord.lng;

    if(location_icon == "office") {
        loc_icon = "http://103.119.92.45:3000/location_icons/office_icon.png";
   } else if (location_icon == "home") {
        loc_icon = "http://103.119.92.45:3000/location_icons/home_icon.png";
   } else if (location_icon == "other") {
        loc_icon = "http://103.119.92.45:3000/location_icons/map_marker_icon.png";
   } else if (location_icon == "restaurant") {
        loc_icon = "http://103.119.92.45:3000/location_icons/restaurant_icon.png";
   } else if (location_icon == "gym") {
        loc_icon = "http://103.119.92.45:3000/location_icons/dumbbell_icon.png";
   } else {
       res.json({
           "error" : "undefined map marking."
       })
   };

    authenticatedAccount(token)
    .then(({ account }) => {

        axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${key}`)
        .then(function (response) {
            
            console.log("the data we WANT", response.data.results[0].formatted_address);

            var coordinates_names = response.data.results[0].formatted_address;

            LocationsTable.updateLocations( location_names, coordinates_names, coordinates, loc_icon, account.id, location_id )
            .then(() => {

                LocationsTable.getLocationsAll(account.id)
                .then(( locations ) => {

                    const all_locations = locations.map((taxi) => {

                        return {
                            location_id : taxi.location_id,
                            acc_id : taxi.acc_id,
                            location_coordinates : JSON.parse(taxi.location_coordinates),
                            acc_tel : taxi.acc_tel,
                            location_names : taxi.location_names,
                            coordinate_names : taxi.coordinate_names,
                            location_icon : taxi.location_icon
                        }
                    })

                    res.json({ 
                        "msg" : "success",
                        "locations" : all_locations
                    });

                }) .catch(error => next(error));

            }) .catch(error => next(error));

        }) .catch(error => next(error));

    }) .catch(error => next(error));
  
});

router.post('/delete', (req, res, next) => {

    const { token, location_names, location_id } = req.body;

    authenticatedAccount(token)
    .then(({ account }) => {

        LocationsTable.deleteLocations(account.id,  location_names, location_id )
        .then(() => {

            LocationsTable.getLocationsAll(account.id)
            .then(( locations ) => {

                const all_locations = locations.map((taxi) => {

                    return {
                        location_id : taxi.location_id,
                        acc_id : taxi.acc_id,
                        location_coordinates : JSON.parse(taxi.location_coordinates),
                        acc_tel : taxi.acc_tel,
                        location_names : taxi.location_names,
                        coordinate_names : taxi.coordinate_names,
                        location_icon : taxi.location_icon
                    }
                })

                res.json({ 
                    "msg" : "success",
                    "locations" : all_locations
                });

            }) .catch(error => next(error));

        }) .catch(error => next(error));

    }) .catch(error => next(error));
  
});


module.exports = router;