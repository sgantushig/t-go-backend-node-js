const { Router } = require('express');
var firebase = require("firebase-admin");
const Fcm_tokenTable = require("../fcm_token/table");
const { authenticatedAccount } = require('./helper');

const router = new Router();
var serviceAccount = require("../push_notification/serviceAccount_Customer.json");


firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  //databaseURL: "https://t-go-e4d0d.firebaseio.com"
});


var payload = {
    notification: {
      title: "This is a Notification",
      body: "This is the body of the notification message."
    }
};
  
var options = {
    priority: "high",
    timeToLive: 60 * 60 *24
};

var registrationToken = "";

const sendPushNotification = (topic, data) => {

  var message = {
    data : data,
    topic : topic
  };

  return firebase.messaging().send(message) 
};

const sendPushNotificationByTokens = (tokens, data) => {

  var message = {
    data : data,
    tokens : tokens
  };

  return firebase.messaging().sendMulticast(message)
};

  // admin.messaging().sendToDevice(registrationToken, payload, options)
  // .then(function(response) {
  //   console.log("Successfully sent message:", response);
  //   console.log(response.results[0].error);
  // })
  // .catch(function(error) {
  //   console.log("Error sending message:", error);
  //   console.log(response.results[0].error);
  // });


router.post('/', (req, res, next) => {

  const { token, fcmToken } = req.body

  console.log("fcm route : ", req.body);

  authenticatedAccount( token )
  .then(({ account }) => {

    Fcm_tokenTable.getFcm_Token( account.id )
    .then(( the_token ) => {

      if (the_token.length !== 0) {

        Fcm_tokenTable.updateFcm_Token( fcmToken, account.id )
        .then(() => {

          Fcm_tokenTable.getFcm_Token( account.id )
          .then(( the_token ) => {
    
            res.json({
              "msg" : "success",
              "yourSentToken" : the_token
            })
      
          }) .catch(error => next(error));

        }) .catch(error => next(error));

      } else {

        Fcm_tokenTable.storeFcm_Token( account.id, fcmToken )
        .then(() => {
      
          Fcm_tokenTable.getFcm_Token( account.id )
          .then(( the_token ) => {

            res.json({
              "msg" : "success",
              "yourSentToken" : the_token
            })
      
          }) .catch(error => next(error));
      
        }) .catch(error => next(error));

      }

    }) .catch(error => next(error))

  })  .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

});

router.post('/send', (req, res, next) => {

  const { token } = req.body

  console.log("fcm send : ", req.body);

  authenticatedAccount( token )
  .then(({ account }) => {

    Fcm_tokenTable.getAllFcm_Token()
    .then((token) => {

      const data = {
        
        title: "This is a Notification",
        body: "This is the body of the notification message."
        
      };

      const tokens = token.map(tokens => {
        return tokens.push_token
      })

      console.log("uujujujujujujujujuju", tokens)

      sendPushNotificationByTokens(tokens, data)

      res.json(token)

    }) .catch(error => next(error));

  })  .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

});

module.exports = router;