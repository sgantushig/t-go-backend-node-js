const { Router } = require('express');
const axios = require('axios');
const utf8 = require('utf8');

const { authenticatedAccount } = require('./helper');

const router = new Router();


router.post('/', (req, res, next) => {

    const { coordinates, token, location_input } = req.body;
    const key = "AIzaSyBJ7WDbOTG6J1ah6gALgmoNzK9Mp9n2uX4"

    const nates = JSON.stringify(coordinates);
    const coord = JSON.parse(nates);
    var latlng = coord.lat + "," + coord.lng;

    const convert = utf8.encode(location_input);

    authenticatedAccount(token)
    .then(() => {

        axios.get(`https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=${convert}&inputtype=textquery&fields=photos,formatted_address,name,opening_hours,rating,geometry&locationbias=circle:200000@${latlng}&key=${key}`)
        .then(function (response) {

            const resp = response.data;

            var repo = response.data.candidates

            if(repo[0] === undefined) {

                res.json({ 
                    "msg" : "success",
                    "result" : [],
                    "status" : "ZERO_RESULTS"
                });

            } else {

                const array = resp.candidates.map(res => res.photos.map((photo) => {

                    return {

                        "coordinates" : res.geometry.location,
                        "name" : res.name
                        
                    }

                }));

                res.json({ 
                    "msg" : "success",
                    "result" : array[0]
                });

            }

        }) .catch(error => next(error));

    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));
  
});


module.exports = router;