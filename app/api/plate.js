const { Router } = require('express');
const router = new Router();

const { authenticatedAccount } = require('./helper');
const PlateTable = require('../plate/table');


router.get('/', (req, res, next) => {

    PlateTable.getAllPlate()
      .then(({plates}) => {
        res.json(plates)
      })
      .catch(error => next(error));
});

router.post('/store', (req, res, next) => {

  const { token, plate_num, car_mark} = req.body;

  console.log( "incomings : ", plate_num, car_mark );

  authenticatedAccount(token)
  .then(({ account }) => {

    PlateTable.storePlate(account.id, account.tel, plate_num, car_mark)
    .then(() => {

        PlateTable.getPlateById(account.id)
        .then(( plate_info ) => {

            console.log("getPlateById", plate_info)

            if( plate_info !== undefined) {

                res.json({
                    "plate_info" : plate_info
                })

            } else {

                res.json({
                    "plate_info" : []
                })

            }

        }) .catch(error => next(error));
        
    }) .catch(error => next(error));

  }) .catch(error => next(error));

});

router.post('/update', (req, res, next) => {

    const { token, plate_num, car_mark} = req.body;

    console.log( "incomings : ", plate_num, car_mark );

    authenticatedAccount(token)
    .then(({ account }) => {

        PlateTable.updatePlateById(account.id, plate_num, car_mark)
        .then(() => {

            PlateTable.getPlateById(account.id)
            .then(( plate_info ) => {

                console.log("getPlateById", plate_info)

                if( plate_info !== undefined) {

                    res.json({
                        "plate_info" : plate_info
                    })

                } else {

                    res.json({
                        "plate_info" : []
                    })

                }

            }) .catch(error => next(error));
            
        }) .catch(error => next(error));

    }) .catch(error => next(error));
  
});
  
router.post('/get', (req, res, next) => {

    const { token } = req.body;
  
    authenticatedAccount(token)
    .then(({ account }) => {
  
        PlateTable.getPlateById(account.id)
        .then(( plate_info ) => {

            console.log("getPlateById", plate_info)

            if( plate_info !== undefined) {

                res.json({
                    "plate_info" : plate_info
                })

            } else {

                res.json({
                    "plate_info" : []
                })

            }

        }) .catch(error => next(error));
  
    }) .catch(error => next(error));
  
});

router.post('/delete', (req, res, next) => {

    const { token } = req.body;
  
    authenticatedAccount(token)
    .then(({ account }) => {

        PlateTable.deletePlateById( account.id )
        .then(() => {

            PlateTable.getPlateById(account.id)
            .then(( plate_info ) => {
    
                console.log("getPlateById", plate_info)
    
                if( plate_info !== undefined) {
    
                    res.json({
                        "plate_info" : plate_info
                    })
    
                } else {
    
                    res.json({
                        "plate_info" : []
                    })
    
                }
    
            }) .catch(error => next(error));

        }) .catch(error => next(error))
  
    }) .catch(error => next(error));
  
});
  

module.exports = router;