const { Router } = require('express');
const axios = require('axios');
const geolib = require('geolib');

const { authenticatedAccount } = require('./helper');

const AccountTable = require('../account/table');
const OrdersTable = require('../orders/table');
const OrdersTemporary = require('../ordersTemporary/table');
const DriveTable = require('../drive/table');
const OrdersHistoryTable = require('../ordersHistory/table');
const VoiceTable = require('../voice/table');
const Drive_temporaryTable = require('../drive_temporary/table');
const RatingTable =require('../rating/table');

const router = new Router();


router.get('/', (req, res, next) => {
    OrdersTable.getOrders()
    .then((orders) => {
        res.json(orders);
    })
    .catch(error => next(error));
});

router.post('/get', (req, res, next) => {

    const { token, coordinates, coordinatesDest, suggestedMinutes, paymentMethod, chosen_client } = req.body;

    console.log("req body from order js !!!!!!!!!!!!!!!!!! : ", req.body);

    authenticatedAccount(token)
    .then(({ account }) => {

        switch (req.body.status) {

            case "INIT": //customer

                OrdersTable.deleteOrderByCustomerId(account.id)
                .then(() => {

                    OrdersTemporary.deleteDrive(account.id)
                    .then(() => {

                        const timestampInit = new Date();

                        if(typeof coordinates == "object") {
        
                            var latlng = coordinates.lat + "," + coordinates.lng;
                            var dinates = JSON.stringify(coordinates);
                            var the_coordinates = JSON.parse(dinates);
        
                        } else {
        
                            var the_coordinates = JSON.parse(coordinates);
                            var coordinates1 = JSON.parse(coordinates);
                            var latlng = coordinates1.lat + "," + coordinates1.lng;
        
                        };
                        
                        var key = "AIzaSyBJ7WDbOTG6J1ah6gALgmoNzK9Mp9n2uX4"
        
                        axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${key}`)
                        .then(function (response) {
                            
                            console.log("the data we WANT", response.data.results[0].formatted_address);
        
                            var locationInit = response.data.results[0].formatted_address;

                            var status = "INIT"
        
                            OrdersTable.storeOrderInit(account.id, coordinates, locationInit, timestampInit, account.tel, status)
                            .then(() => {

                                DriveTable.getOneDrive( account.id )
                                .then(({ drivers }) => {

                                    OrdersTable.getOrderIdByClient(account.id)
                                    .then(( orderId ) => {

                                        AccountTable.updateStatus(status, account.id)
                                        .then(() => {
            
                                            OrdersTable.getOrderCustomer(account.id)
                                            .then(({order}) => {

                                                const driver = drivers.map((taxi) => {
                                                    var cctv = JSON.parse(taxi.coordinates)
                                                    return {
                                                        id : taxi.accountId, //      
                                                        coordinates : JSON.parse(taxi.coordinates), //
                                                        online : taxi.online, //
                                                        status : taxi.status, // 
                                                        driver : taxi.driver, //
                                                        timestamp : taxi.timestamp, //
                                                        minkm : geolib.getDistance(
                                                            { latitude: the_coordinates.lat, longitude: the_coordinates.lng },
                                                            { latitude: cctv.lat, longitude: cctv.lng }
                                                        )
                                                    }
                                                });
        
                                                const sorted = driver.sort((a, b) => (a.minkm > b.minkm ? 1 : -1))
        
                                                //console.log("the final result : ", driver);
                                                console.log("the final result sorted : ", sorted);
        
                                                if (sorted.length >= 2) {

                                                    console.log( "if : ", sorted[0], sorted[1])
        
                                                    OrdersTemporary.storeOrdersTemporary( orderId.id, sorted[0].id, sorted[0].coordinates, sorted[0].minkm, account.id, coordinates )
                                                    .then(() => {
        
                                                        OrdersTemporary.storeOrdersTemporary( orderId.id, sorted[1].id, sorted[1].coordinates, sorted[1].minkm, account.id, coordinates )
                                                        .then(() => {

                                                            AccountTable.getTelById(sorted[0].id)
                                                            .then(( tel1 ) => {

                                                                console.log(" tel if undefined - error /*-/*-/*-/*-/*-/*-/*-", sorted[0].id, tel1)
                                                                
                                                                AccountTable.getTelById(sorted[1].id)
                                                                .then(( tel2 ) => {

                                                                    console.log(" tel if undefined - error2 /*-/*-/*-/*-/*-/*-/*-", sorted[1].id, tel2)

                                                                    res.json({
                                                                        "msg":"success",
                                                                        "order" : {
                                                                            "id" : order.id,
                                                                            "tel" : order.tel,
                                                                            "customerId" : order.customerId,
                                                                            "driverId": order.driverId,
                                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                            "locationInit": order.locationInit,
                                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                            "locationDest" : order.locationDest,
                                                                            "timestampInit" : order.timestampInit,
                                                                            "timestampArrive" : order.timestampArrive,
                                                                            "timestampClosed" : order.timestampClosed,
                                                                            "timestampStarted" : order.timestampStarted,
                                                                            "timestampDrive" : order.timestampDrive,
                                                                            "timestampStopped" : order.timestampStopped,
                                                                            "timestampPaid" : order.timestampPaid,
                                                                            "status" : order.status,
                                                                            "extra" : order.extra,
                                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                                            "description" : order.description,
                                                                            "comment" : order.comment,
                                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                                        }, 
                                                                        "driver_1": {
                                                                            "id" : sorted[0].accountId,
                                                                            "tel" : tel1.tel,
                                                                            "driver" : sorted[0].driver,
                                                                            "coordinates" : sorted[0].coordinates,
                                                                            "status" : sorted[0].status,
                                                                            "timestamp" : sorted[0].timestamp,
                                                                            "minkm" : sorted[0].minkm
                                                                        },
                                                                        "driver_2": {
                                                                            "id" : sorted[1].accountId,
                                                                            "tel" : tel2.tel,
                                                                            "driver" : sorted[1].driver,
                                                                            "coordinates" : sorted[1].coordinates,
                                                                            "status" : sorted[1].status,
                                                                            "timestamp" : sorted[1].timestamp,
                                                                            "minkm" : sorted[1].minkm
                                                                        }
                            
                                                                    });

                                                                }) .catch(error => next(error));
                                                
                                                            }) .catch(error => next(error));

                                                        }) .catch(error => next(error));
        
                                                    }) .catch(error => next(error));
        
                                                } else if (sorted.length == 0) {
                                                    console.log("else if : ", sorted)
        
                                                    res.json({
                                                        "msg":"success",
                                                        "order" : {
                                                            "id" : order.id,
                                                            "tel" : order.tel,
                                                            "customerId" : order.customerId,
                                                            "driverId": order.driverId,
                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                            "locationInit": order.locationInit,
                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                            "locationDest" : order.locationDest,
                                                            "timestampInit" : order.timestampInit,
                                                            "timestampArrive" : order.timestampArrive,
                                                            "timestampClosed" : order.timestampClosed,
                                                            "timestampStarted" : order.timestampStarted,
                                                            "timestampDrive" : order.timestampDrive,
                                                            "timestampStopped" : order.timestampStopped,
                                                            "timestampPaid" : order.timestampPaid,
                                                            "status" : order.status,
                                                            "extra" : order.extra,
                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                            "description" : order.description,
                                                            "comment" : order.comment,
                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                        }, 
                                                        "driver_1" : [],
                                                        "driver_2" : []
                                                    })
        
                                                } else {
                                                    console.log("else : ", sorted)
        
                                                    OrdersTemporary.storeOrdersTemporary( orderId.id, sorted[0].accountId, sorted[0].coordinates,sorted[0].minkm, account.id, coordinates )
                                                    .then(() => {

                                                        AccountTable.getTelById(sorted[0].accountId)
                                                        .then(( tel1 ) => {

                                                            res.json({
                                                                "msg":"success",
                                                                "order" : {
                                                                    "id" : order.id,
                                                                    "tel" : order.tel,
                                                                    "customerId" : order.customerId,
                                                                    "driverId": order.driverId,
                                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                    "locationInit": order.locationInit,
                                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                    "locationDest" : order.locationDest,
                                                                    "timestampInit" : order.timestampInit,
                                                                    "timestampArrive" : order.timestampArrive,
                                                                    "timestampClosed" : order.timestampClosed,
                                                                    "timestampStarted" : order.timestampStarted,
                                                                    "timestampDrive" : order.timestampDrive,
                                                                    "timestampStopped" : order.timestampStopped,
                                                                    "timestampPaid" : order.timestampPaid,
                                                                    "status" : order.status,
                                                                    "extra" : order.extra,
                                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                                    "description" : order.description,
                                                                    "comment" : order.comment,
                                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                                }, 
                                                                "driver_1": {
                                                                    "id" : sorted[0].accountId,
                                                                    "tel" : tel1.tel,
                                                                    "driver" : sorted[0].driver,
                                                                    "coordinates" : sorted[0].coordinates,
                                                                    "status" : sorted[0].status,
                                                                    "timestamp" : sorted[0].timestamp,
                                                                    "minkm" : sorted[0].minkm
                                                                },
                                                                "driver_2" : []

                                                            });

                                                        }) .catch(error => next(error));

                                                    }) .catch(error => next(error));
        
                                                };

                                            }) .catch(error => next(error));

                                        }) .catch(error => next(error));
                                    
                                    }) .catch(error => next(error));

                                }) .catch(error => next(error));
        
                            }) .catch(error => next(error));
        
                        }) .catch(error => next(error));

                    }) .catch(error => next(error));

                }) .catch(error => next(error));

            break;
            
            case "STARTED": //driver

                console.log("A0 starting", account.id, suggestedMinutes);

                OrdersTemporary.getOrderTemporary_check( account.id, chosen_client)
                .then(( orderstemporary ) => {

                    console.log("A1 getOrderTemporary_check", orderstemporary);

                    if( orderstemporary !== null) {

                        OrdersTemporary.updateOrderTemporary( suggestedMinutes, account.id)
                        .then(() => { 

                            console.log("A2 updateOrderTemporary");

                            setTimeout(() => {
        
                                OrdersTemporary.matchOrderTemporary(chosen_client)
                                .then((drive) => {

                                    console.log("A3 matchOrderTemporary", drive, chosen_client, drive.length);

                                    if (drive !== null || drive.length !== 0 || drive === undefined ) {

                                        if ( drive.length >= 2 ) {

                                            if ( drive[0].estimatedTime === null || drive[0].estimatedTime === undefined ) {
                                                console.log("it worked, the first one is null");
        
                                                drive[0] = drive[1];
        
                                            } else if ( drive[1].estimatedTime === null || drive[1].estimatedTime === undefined ) {
                                                console.log("working fine, the second one is null");
        
                                                drive[0] = drive[0];
        
                                            } else {
                                                console.log("it works both have it though")
        
                                                if(drive[0].estimatedTime>drive[1].estimatedTime){
                                                    drive[0]=drive[1];
                                                }
                                                if(drive[0].estimatedTime == drive[1].estimatedTime){
                                                    if(drive[0].minkm > drive[1].minkm){
                                                        drive[0]=drive[1];
                                                    }
                                                };
                                            }

                                        } 

                                        console.log("A4 matchOrderTemporary after the match : ", drive[0]);
                
                                        var status = "STARTED"
    
                                        OrdersTable.getOrderIdByClient(chosen_client)
                                        .then((orderId) => {

                                            console.log("A5 getOrderIdByClient", orderId);

                                            if (orderId !== null) {

                                                var order_id = orderId.id;

                                                OrdersTable.updateOrderStatus(status, chosen_client)
                                                .then(() => {
                        
                                                    console.log("A6 updateOrderStatus");
                        
                                                    AccountTable.updateStatus(status, account.id)
                                                    .then(() => {
                        
                                                        console.log("A7 updateStatus");
                        
                                                        AccountTable.updateStatus(status, chosen_client)
                                                        .then(() => {
                        
                                                            console.log("A8 updateStatus");
                        
                                                            var id = order_id;
                                                            var driveId = drive[0].driver_id;
                                                            var coordinatesInitDriver = JSON.parse(drive[0].coordinatesCurrent);
                                                            var estimatedTime = drive[0].estimatedTime;
                            
                                                            var latlng = coordinatesInitDriver.lat + "," + coordinatesInitDriver.lng;
                                                            var key = "AIzaSyBJ7WDbOTG6J1ah6gALgmoNzK9Mp9n2uX4"
                            
                                                            const timestampStarted = new Date();
        
                                                            console.log("A9 axios", latlng, drive[0].coordinatesCurrent, coordinatesInitDriver );
                            
                                                            axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latlng}&key=${key}`)
                                                            .then(function (response) {
                                                                
                                                                console.log("A9 axios get the data we WANT", response.data.results[0].formatted_address);
                                        
                                                                var location = response.data.results[0].formatted_address;
                            
                                                                OrdersTable.updateOrderStarted( driveId, coordinatesInitDriver, timestampStarted, estimatedTime, location, id )
                                                                .then(() => {

                                                                    console.log("A10 updateOrderStarted");
                    
                                                                    OrdersTable.getOrderByBothId( account.id, chosen_client)
                                                                    .then(( order ) => {

                                                                        console.log("A11 getOrderByBothId", order)
        
                                                                        OrdersTemporary.deleteDrive(chosen_client)
                                                                        .then(()=>{

                                                                            console.log("A12 deleteDrive");

                                                                            AccountTable.getAccountById(chosen_client)
                                                                            .then(({ account }) => {

                                                                                console.log("A13 getAccountById");

                                                                                var msg = "you have been chosen with : " + chosen_client

                                                                                if (order != null || order != undefined ) {
                            
                                                                                    res.json({
                                                                                        msg,
                                                                                        "voice" : null,
                                                                                        "customer_account" : {
                                                                                            "id": account.id,
                                                                                            "tel" : account.tel,
                                                                                            "name" : account.name,
                                                                                            "avatar" : account.avatar,
                                                                                            "driver" : account.driver,
                                                                                            "timestamp" : account.timestamp,
                                                                                            "coordinates": JSON.parse(account.startCoordinates),
                                                                                            "online" : account.online,
                                                                                            "status": account.status
                                                                                        },
                                                                                        "order" : {
                                                                                            "id" : order.id,
                                                                                            "tel" : order.tel,
                                                                                            "customerId" : order.customerId,
                                                                                            "driverId": order.driverId,
                                                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                            "locationInit": order.locationInit,
                                                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                            "locationDest" : order.locationDest,
                                                                                            "timestampInit" : order.timestampInit,
                                                                                            "timestampArrive" : order.timestampArrive,
                                                                                            "timestampClosed" : order.timestampClosed,
                                                                                            "timestampStarted" : order.timestampStarted,
                                                                                            "timestampDrive" : order.timestampDrive,
                                                                                            "timestampStopped" : order.timestampStopped,
                                                                                            "timestampPaid" : order.timestampPaid,
                                                                                            "status" : order.status,
                                                                                            "extra" : order.extra,
                                                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                            "description" : order.description,
                                                                                            "comment" : order.comment,
                                                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                            
                                                                                        }
                                                                                        
                                                                                    })
                                    
                                                                                } else {
                                    
                                                                                    res.json({
                                                                                        "order" : null
                                                                                    })
                                    
                                                                                }

                                                                            }) .catch(error => next(error));

                                                                        }) .catch(error => next(error));
                            
                                                                    }) .catch(error => next(error));
                    
                                                                }) .catch(error => next(error));
                            
                                                            }) .catch(error => next(error));
                        
                                                        }) .catch(error => {

                                                            console.log(error);
                        
                                                            OrdersTable.getOrderByBothId( account.id, chosen_client)
                                                            .then(( order ) => {
                        
                                                                console.log("B1 getOrderByBothId", order);
                        
                                                                AccountTable.getAccountById(chosen_client)
                                                                .then(({ account }) => {
                        
                                                                    console.log("B2 getAccountById", account);
                        
                                                                    var msg = "you have been chosen with : " + chosen_client
                        
                                                                    if (order != null || order != undefined ) {

                                                                        res.json({
                                                                            msg,
                                                                            "voice" : null,
                                                                            "customer_account" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinates),
                                                                                "online" : account.online,
                                                                                "status": account.status
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                "distance_meter" : distance_meter
                                                                            }
                                                                            
                                                                        })
                        
                                                                    } else {
                        
                                                                        res.json({
                                                                            "order" : null
                                                                        })
                        
                                                                    }
                        
                                                                }) .catch(error => next(error));
                        
                                                            }) .catch(error => next(error))
                        
                                                        }) // this thing is cannot be used but there's no other way to do it. sorry. but i will get it right soon. 
                        
                                                    }) .catch(error => next(error));
                        
                                                }) .catch(error => next(error));

                                            } else {

                                                res.json({
                                                    "order" : null
                                                })

                                            }
    
                                        }) .catch(error => next(error));

                                } else {

                                    res.json({
                                        "order" : null
                                    })
            
                                }

                                }) .catch(error => {

                                    console.log(error);

                                    OrdersTable.getOrderByBothId( account.id, chosen_client)
                                    .then(( order ) => {

                                        console.log("B1 getOrderByBothId", order);

                                        AccountTable.getAccountById(chosen_client)
                                        .then(({ account }) => {

                                            console.log("B2 getAccountById", account);

                                            var msg = "you have been chosen with : " + chosen_client

                                            if (order != null || order != undefined ) {

                                                res.json({
                                                    msg,
                                                    "voice" : null,
                                                    "customer_account" : {
                                                        "id": account.id,
                                                        "tel" : account.tel,
                                                        "name" : account.name,
                                                        "avatar" : account.avatar,
                                                        "driver" : account.driver,
                                                        "timestamp" : account.timestamp,
                                                        "coordinates": JSON.parse(account.startCoordinates),
                                                        "online" : account.online,
                                                        "status": account.status
                                                    },
                                                    "order" : {
                                                        "id" : order.id,
                                                        "tel" : order.tel,
                                                        "customerId" : order.customerId,
                                                        "driverId": order.driverId,
                                                        "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                        "locationInit": order.locationInit,
                                                        "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                        "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                        "locationDest" : order.locationDest,
                                                        "timestampInit" : order.timestampInit,
                                                        "timestampArrive" : order.timestampArrive,
                                                        "timestampClosed" : order.timestampClosed,
                                                        "timestampStarted" : order.timestampStarted,
                                                        "timestampDrive" : order.timestampDrive,
                                                        "timestampStopped" : order.timestampStopped,
                                                        "timestampPaid" : order.timestampPaid,
                                                        "status" : order.status,
                                                        "extra" : order.extra,
                                                        "lastVoiceDriver" : order.lastVoiceDriver,
                                                        "description" : order.description,
                                                        "comment" : order.comment,
                                                        "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                        "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                        "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                        "distance_meter" : distance_meter
                                                    }
                                                    
                                                })

                                            } else {

                                                res.json({
                                                    "order" : null
                                                })

                                            }

                                        }) .catch(error => next(error));

                                    }) .catch(error => next(error))

                                }) // this thing is cannot be used but there's no other way to do it. sorry. but i will get it right soon. 
            
                            }, 5000)
        
                        }) .catch(error => next(error));
        
                    } else {

                        res.json({
                            "order" : null
                        })

                    }

                }) .catch(error => next(error));
            
            break;

            case "ARRIVED": //driver 

                var status = "ARRIVED"
                const timestampArrive = new Date();

                console.log("ARRIVED", timestampArrive)

                OrdersTable.storeOrderArrive(timestampArrive, account.id)
                .then(() => {

                    OrdersTable.updateOrderStatusDriver(status, account.id)
                    .then(() => { 

                        AccountTable.updateStatus(status, account.id)
                        .then(() => { 

                            OrdersTable.getClientIdByDriver(account.id)
                            .then(({customerId}) => {
                                
                                AccountTable.updateStatus(status, customerId)
                                .then(() => {
    
                                    OrdersTable.getOrder(account.id)
                                    .then(({order}) => {

                                        var clean_car = 0
                                        var cool_car = 0 
                                        var dj = 0 
                                        var nice_driver = 0

                                        RatingTable.storeRate( account.id , order.id , clean_car , cool_car , dj , nice_driver )
                                        .then(() => {

                                            res.json({
                                                "order" : {
                                                    "id" : order.id,
                                                    "tel" : order.tel,
                                                    "customerId" : order.customerId,
                                                    "driverId": order.driverId,
                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                    "locationInit": order.locationInit,
                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                    "locationDest" : order.locationDest,
                                                    "timestampInit" : order.timestampInit,
                                                    "timestampArrive" : order.timestampArrive,
                                                    "timestampClosed" : order.timestampClosed,
                                                    "timestampStarted" : order.timestampStarted,
                                                    "timestampDrive" : order.timestampDrive,
                                                    "timestampStopped" : order.timestampStopped,
                                                    "timestampPaid" : order.timestampPaid,
                                                    "status" : order.status,
                                                    "extra" : order.extra,
                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                    "description" : order.description,
                                                    "comment" : order.comment,
                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                }, 
                                                "msg" : "success"
                                            });

                                        }) .catch(error => next(error));   
    
                                    }) .catch(error => next(error));
    
                                }) .catch(error => next(error));
    
                            }) .catch(error => next(error));

                        }) .catch(error => next(error));

                    }) .catch(error => next(error));

                }) .catch(error => next(error));

            break;    

            case "DRIVING": //driver

                var status = "DRIVING"
                const timestampDrive = new Date();

                OrdersTable.storeOrderDriving(timestampDrive,  account.id)
                .then(() => {

                    OrdersTable.updateOrderStatusDriver(status, account.id)
                    .then(() => { 

                        AccountTable.updateStatus(status, account.id)
                        .then(() => { 

                            OrdersTable.getClientIdByDriver(account.id)
                            .then(({customerId}) => {
                                
                                AccountTable.updateStatus(status, customerId)
                                .then(() => {

                                    OrdersTable.getOrder(account.id)
                                    .then(({ order }) => {
    
                                        res.json({
                                            "order" : {
                                                "id" : order.id,
                                                "tel" : order.tel,
                                                "customerId" : order.customerId,
                                                "driverId": order.driverId,
                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                "locationInit": order.locationInit,
                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                "locationDest" : order.locationDest,
                                                "timestampInit" : order.timestampInit,
                                                "timestampArrive" : order.timestampArrive,
                                                "timestampClosed" : order.timestampClosed,
                                                "timestampStarted" : order.timestampStarted,
                                                "timestampDrive" : order.timestampDrive,
                                                "timestampStopped" : order.timestampStopped,
                                                "timestampPaid" : order.timestampPaid,
                                                "status" : order.status,
                                                "extra" : order.extra,
                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                "description" : order.description,
                                                "comment" : order.comment,
                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                            }, 
                                            "msg" : "success"
                                            })

                                    }) .catch(error => next(error));

                                }) .catch(error => next(error));

                            }) .catch(error => next(error));

                        }) .catch(error => next(error));

                    }) .catch(error => next(error));

                }) .catch(error => next(error));

            break;

            case "STOPPED": //driver

                OrdersTable.getOrderByDriverId(account.id)
                .then(({ order }) => {

                    const timestampStopped = new Date();
                
                    var coordinates_init = JSON.parse(order.coordinatesInit);

                    var origins = coordinates_init.lat + "," + coordinates_init.lng;

                    if(typeof coordinatesDest == "object") {

                        var destinations = coordinatesDest.lat + "," + coordinatesDest.lng;

                    } else {

                        var coordinates1 = JSON.parse(coordinatesDest)
                        var destinations = coordinates1.lat + "," + coordinates1.lng;

                    }

                    console.log(order.coordinatesInit, coordinatesDest);

                    var key = "AIzaSyBJ7WDbOTG6J1ah6gALgmoNzK9Mp9n2uX4"

                    axios.get(`https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${origins}&destinations=${destinations}&key=${key}&language=eng&region=mn`)
                    .then(function (response) {
                        
                        console.log(response.data);
                        
                        var location = response.data.destination_addresses[0];

                        var amount = response.data.rows[0].elements[0].distance.value;
                        var inTugrugs = amount * 1;
                        var description = inTugrugs + " ₮"

                        var status = "STOPPED"

                        console.log("CONSOLE<>" ,"coordinatesDest :", coordinatesDest, "timestampStopped :", timestampStopped, "location :", location, "id :", account.id, "amount :", amount);

                        OrdersTable.storeOrderStopped(coordinatesDest, timestampStopped, location, account.id)
                        .then(() => {

                            OrdersTable.updateOrderStatusDriver(status, account.id)
                            .then(() => {

                                AccountTable.updateStatus(status, account.id)
                                .then(() => {

                                    OrdersTable.getClientIdByDriver(account.id)
                                    .then(({customerId}) => {
                                        
                                        AccountTable.updateStatus(status, customerId)
                                        .then(() => {
    
                                            OrdersTable.updateOrderDescription(description, account.id)
                                            .then(() => {
        
                                                AccountTable.updateStatus(status, customerId)
                                                .then(() => {
                    
                                                    OrdersTable.getOrder(account.id)
                                                    .then(({order}) => {
                                                        
                                                        res.json({
                                                            "msg" : "success",
                                                            "order" : {
                                                                "id" : order.id,
                                                                "tel" : order.tel,
                                                                "customerId" : order.customerId,
                                                                "driverId": order.driverId,
                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                "locationInit": order.locationInit,
                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                "locationDest" : order.locationDest,
                                                                "timestampInit" : order.timestampInit,
                                                                "timestampArrive" : order.timestampArrive,
                                                                "timestampClosed" : order.timestampClosed,
                                                                "timestampStarted" : order.timestampStarted,
                                                                "timestampDrive" : order.timestampDrive,
                                                                "timestampStopped" : order.timestampStopped,
                                                                "timestampPaid" : order.timestampPaid,
                                                                "status" : order.status,
                                                                "extra" : order.extra,
                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                "description" : order.description,
                                                                "comment" : order.comment,
                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                            }, 
                                                            "response" : response.data,
                                                            "amount": {
                                                                amount,
                                                                "tugrugs" : inTugrugs
                                                            } 
    
                                                        });
                    
                                                    }) .catch(error => next(error));
            
                                                }) .catch(error => next(error));
        
                                            }) .catch(error => next(error));
                                            
                                        }) .catch(error => next(error));
    
                                    }) .catch(error => next(error));

                                }).catch(error => next(error));
                                
                            }).catch(error => next(error));

                        }) .catch(error => next(error));

                    }) .catch(error => next(error));

                }) .catch(error => next(error));

            break;
            
            case "PAID": //driver -- changed to customer

                var status = "PAID"
                const timestampPaid = new Date();

                OrdersTable.storeOrderBilling( timestampPaid, account.id)
                .then(() => {

                    OrdersTable.updateOrderStatus(status, account.id)
                    .then(() => {

                        AccountTable.updateStatus(status, account.id)
                        .then(() => {

                            OrdersTable.getDriverIdByClient(account.id)
                            .then(({driverId}) => {
                                
                                AccountTable.updateStatus(status, driverId)
                                .then(() => {

                                    OrdersTable.updateOrderComment(paymentMethod, account.id)
                                    .then(() => {
    
                                        OrdersTable.getOrderCustomer(account.id)
                                        .then(({order}) => {

                                            Drive_temporaryTable.deleteDriveTemporary(driverId)
                                            .then(() => {

                                                res.json({
                                                    "order" : {
                                                        "id" : order.id,
                                                        "tel" : order.tel,
                                                        "customerId" : order.customerId,
                                                        "driverId": order.driverId,
                                                        "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                        "locationInit": order.locationInit,
                                                        "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                        "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                        "locationDest" : order.locationDest,
                                                        "timestampInit" : order.timestampInit,
                                                        "timestampArrive" : order.timestampArrive,
                                                        "timestampClosed" : order.timestampClosed,
                                                        "timestampStarted" : order.timestampStarted,
                                                        "timestampDrive" : order.timestampDrive,
                                                        "timestampStopped" : order.timestampStopped,
                                                        "timestampPaid" : order.timestampPaid,
                                                        "status" : order.status,
                                                        "extra" : order.extra,
                                                        "lastVoiceDriver" : order.lastVoiceDriver,
                                                        "description" : order.description,
                                                        "comment" : order.comment,
                                                        "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                        "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                        "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                    },  
                                                    "msg" : "success"
                                                });

                                            }) .catch(error => next(error));
            
                                        }) .catch(error => next(error));
    
                                    }) .catch(error => next(error));

                                }) .catch(error => next(error));

                            }) .catch(error => next(error));
                                
                        }) .catch(error => next(error));
                            
                    }) .catch(error => next(error));

                }) .catch(error => next(error));

            break;

            case "CLOSED": //driver

                const timestampClosed = new Date();

                var status = "CLOSED"
                var status_1 = "FREE"

                OrdersTable.storeOrderClosed( timestampClosed, account.id)
                .then(() => {

                    OrdersTable.getOrder(account.id)
                    .then(({ order }) => {

                        if (order !== undefined || order) {

                            var ordersId = order.id;
                            var tel = order.tel;
    
                            var customerId = order.customerId;
                            var driverId = order.driverId;
    
                            var coordinatesInit = "'" + JSON.stringify(order.coordinatesInit) + "'"; 
                            var locationInit = order.locationInit;
    
                            var coordinatesInitDriver = "'" + JSON.stringify(order.coordinatesInitDriver) + "'";
                            var locationInitDriver = order.locationInitDriver;
    
                            var coordinatesDest = "'" + JSON.stringify(order.coordinatesDest) + "'";
                            var locationDest = order.locationDest;
    
                            var timestampInit = order.timestampInit;
                            var timestampArrive = order.timestampArrive;
    
                            var timestampClosed = order.timestampClosed;
                            var timestampStarted = order.timestampStarted;
    
                            var timestampDrive = order.timestampDrive;
    
                            var timestampStopped = order.timestampStopped;
    
                            var timestampPaid = order.timestampPaid;
    
                            var status = order.status;
                            var extra = order.extra;
                            var lastVoiceDriver = order.lastVoiceDriver;
    
                            var description = order.description;
                            var comment = order.comment;
    
                            var lastVoiceDriverTimestamp = order.lastVoiceDriverTimestamp;
                            var lastVoiceCustomer = order.lastVoiceCustomer;
                            var lastVoiceCustomerTimestamp = order.lastVoiceCustomerTimestamp;

                            OrdersHistoryTable.storeOrdersHistory(

                                ordersId, 
                                tel, 
                                customerId, 
                                driverId, 
                                coordinatesInit,
                                locationInit, 
    
                                coordinatesInitDriver,
                                locationInitDriver, 
    
                                coordinatesDest, 
                                locationDest,
    
                                timestampInit, 
                                timestampArrive, 
    
                                timestampClosed, 
                                timestampStarted, 
                                timestampDrive, 
    
                                timestampStopped, 
                                timestampPaid, 
    
                                status, 
                                extra, 
                                lastVoiceDriver, 
                                description, 
                                comment, 
    
                                lastVoiceDriverTimestamp, 
                                lastVoiceCustomer, 
                                lastVoiceCustomerTimestamp
    
                            ) .then(() => {
    
                                OrdersTable.updateOrderStatusDriver(status, account.id)
                                .then(() => {
    
                                    AccountTable.updateStatus(status_1, account.id)
                                    .then(() => { 
    
                                        OrdersTable.getClientIdByDriver(account.id)
                                        .then(({customerId}) => {
    
                                            VoiceTable.getLastVoice(account.id, customerId)
                                            .then(({ voice }) => {
    
                                                if (!voice[0] === undefined || !voice.length == 0){
    
                                                    OrdersTable.updateOrderVoice(voice[0].timestamp,voice[0].timestamp, voice[0].voicePath, voice[0].voicePath, account.id)
                                                    .catch(error => next(error));
    
                                                } else {
    
                                                    return;
    
                                                }
    
                                            }) .then(() => {
    
                                                AccountTable.updateStatus(status_1, customerId)
                                                .then(() => {
    
                                                    OrdersTable.deleteOrderByDriverId(account.id)
                                                    .then(() => {
    
                                                        Drive_temporaryTable.deleteDriveTemporary(account.id)
                                                        .then(() => { 
    
                                                            console.log(' The Creator says : "Order data is moved to the history table. Chek your admin panel and database." ')
                                                            res.json({ 
        
                                                                "msg" : "success",
                                                                "order" : null
        
                                                                });
    
                                                        }) .catch(error => next(error));
    
                                                    }) .catch(error => next(error));
    
                                                }) .catch(error => next(error));
    
                                            }) .catch(error => next(error));
                                
                                        }).catch(error => next(error));
    
                                    }) .catch(error => next(error));
    
                                }) .catch(error => next(error));
    
                            }).catch(error => next(error));

                        } else {
                            res.json({
                                "order" : null
                            })
                        }

                    }).catch(error => next(error));

                }).catch(error => next(error));

            break;

            case 'CANCELLED': //customer

                var status = "FREE"

                OrdersTable.getDriverIdByClient(account.id)
                .then(({ driverId }) => {

                    console.log(driverId, "sda shu nere")

                    if(driverId == null || undefined) {

                        AccountTable.updateStatus(status, account.id)
                        .then(() => {

                            OrdersTable.deleteOrderByCustomerId(account.id)
                            .then(()=>{

                                console.log("Cancel switch is working, I guess.", account.id);

                                OrdersTemporary.deleteDrive(account.id)
                                .then(() => {

                                    Drive_temporaryTable.deleteDriveTemporary(account.id)
                                    .then(() => { 

                                        res.json({
                                        
                                            "msg":"success",
                                            "order" : null
    
                                        })
    
                                        console.log("deleted from orders temporary too.")

                                    }) .catch(error => next(error));
                                    
                                }) .catch(error => next(error));

                            }) .catch(error => next(error));

                        }) .catch(error => next(error));

                    } else {

                        AccountTable.updateStatus(status, driverId)
                        .then(() => {
    
                            AccountTable.updateStatus(status, account.id)
                            .then(() => {
    
                                OrdersTable.deleteOrderByCustomerId(account.id)
                                .then(()=>{
    
                                    console.log("Cancel switch is working, I guess.", account.id);
    
                                    OrdersTemporary.deleteDrive(account.id)
                                    .then(() => {

                                        Drive_temporaryTable.deleteDriveTemporary(account.id)
                                        .then(() => { 

                                            res.json({
                                            
                                                "msg":"success",
                                                "order" : null
        
                                            })
        
                                            console.log("deleted from orders temporary too.")

                                        }) .catch(error => next(error));
    
                                    }) .catch(error => next(error));
    
                                }) .catch(error => next(error));
    
                            }) .catch(error => next(error));
    
                        }) .catch(error => next(error));

                    };

                }) .catch(error => next(error));

            break;
        
        };

        if (!req.body.status && token) {

            if(account.driver) {

                OrdersTable.getOrderByDriverId(account.id)
                .then(({order}) => {

                    if(order) {

                        req.json({
                            "order" : {
                                "id" : order.id,
                                "tel" : order.tel,
                                "customerId" : order.customerId,
                                "driverId": order.driverId,
                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                "locationInit": order.locationInit,
                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                "locationDest" : order.locationDest,
                                "timestampInit" : order.timestampInit,
                                "timestampArrive" : order.timestampArrive,
                                "timestampClosed" : order.timestampClosed,
                                "timestampStarted" : order.timestampStarted,
                                "timestampDrive" : order.timestampDrive,
                                "timestampStopped" : order.timestampStopped,
                                "timestampPaid" : order.timestampPaid,
                                "status" : order.status,
                                "extra" : order.extra,
                                "lastVoiceDriver" : order.lastVoiceDriver,
                                "description" : order.description,
                                "comment" : order.comment,
                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                            }, 
                            "msg":"success"
                        })

                    } else {

                        console.log("anhaan! catch you!")

                        res.json({

                            "order" : null
                        })
                    }

                }) .catch((error) => next(error))

            } else {

                OrdersTable.getOrderCustomer(account.id)
                .then(({order}) => {

                    if(order) {

                        res.json({
                            "order" : {
                                "id" : order.id,
                                "tel" : order.tel,
                                "customerId" : order.customerId,
                                "driverId": order.driverId,
                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                "locationInit": order.locationInit,
                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                "locationDest" : order.locationDest,
                                "timestampInit" : order.timestampInit,
                                "timestampArrive" : order.timestampArrive,
                                "timestampClosed" : order.timestampClosed,
                                "timestampStarted" : order.timestampStarted,
                                "timestampDrive" : order.timestampDrive,
                                "timestampStopped" : order.timestampStopped,
                                "timestampPaid" : order.timestampPaid,
                                "status" : order.status,
                                "extra" : order.extra,
                                "lastVoiceDriver" : order.lastVoiceDriver,
                                "description" : order.description,
                                "comment" : order.comment,
                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                            }, 
                            "msg":"success"
                        }) 

                    } else {

                        console.log("anhaan! catch you1!")

                        res.json({
                            "order" : null
                        })
                        
                    }

                }) .catch(error => next(error))
            }

        };
        

    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

});

module.exports = router;