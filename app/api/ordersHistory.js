const { Router } = require('express');

const OrdersHistoryTable = require('../ordersHistory/table');

const router = new Router();

router.get('/', (req, res) => {

    OrdersHistoryTable.getOrdersHistory()
    .then((ordersHistory) => {

        res.json(ordersHistory);
    })
    .catch(error => console.log(error));
});

module.exports = router;