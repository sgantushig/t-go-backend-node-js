const { Router } = require('express');
const geolib = require('geolib');

const { authenticatedAccount } = require('./helper');

const VoiceTable = require('../voice/table');
const DriveTable = require('../drive/table');
const OrdersTemporaryTable = require('../ordersTemporary/table');
const OrdersTable = require('../orders/table');
const AccountTable = require('../account/table');
const Drive_historyTable = require('../drive_history/table');
const Drive_temporaryTable = require('../drive_temporary/table');
const PlateTable = require('../plate/table');

const router = new Router();

router.get('/', (req, res, next) => {

    DriveTable.getDrive()
    .then(({ drive }) => {
        drive.sort((a, b) => parseFloat(b.id) - parseFloat(a.id));
        res.json(drive);
    }) .catch(error => next(error));
});

//******************************************************* This route has been changed completely ********************************************************************/

router.post('/', (req, res, next) => {

    var { token, coordinates } = req.body;

    authenticatedAccount(token)
    .then(({ account }) => {

        var online = true;
                
        if (account.driver) {   // **************************************** if the account is driver *********************************************/

            DriveTable.getDriveByAccount(account.id)
            .then(({ drive }) => {

                console.log("A1 getDriveByAccount")

                if (drive === undefined || drive.length == 0) {  // ********************************* if the account hasn't any record of drive ***************************/

                    DriveTable.storeDrive(account.id, coordinates, account.online, account.status, account.driver)
                    .then(() => {

                        console.log("A2 storeDrive")

                        DriveTable.getDriveByAccount(account.id)
                        .then(({ drive }) => { 

                            console.log("A3 getDriveByAccount")

                            Drive_historyTable.storeDriveHistory(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].status, drive[0].driver, drive[0].timestamp)
                            .then(() => {

                                console.log("A4 storeDriveHistory")
                                
                                AccountTable.updateOnline(online, account.id)
                                .then(() => {

                                    console.log("A5 updateOnline")
                
                                    AccountTable.updateStartCoordinates( coordinates, account.id )
                                    .then(() => {

                                        console.log("A6 updateStartCoordinates")
        
                                        OrdersTable.getOrderByDriverId(account.id)
                                        .then(({ order }) => {

                                            console.log("A7 getOrderByDriverId")
                        
                                            if(!order){  // ********************************** if there isn't any order ************************************************/

                                                OrdersTemporaryTable.getOrderTemporaryByDriverId(account.id)
                                                .then(( temporary_orders ) => {

                                                    console.log("A8 getOrderTemporaryByDriverId")

                                                    // This if statement has to be checked only by length property//
                                                    if (temporary_orders.length !== 0) {      // ********************************** if there's a record of temporary orders **********************************/

                                                        var the_coordinates = coordinates;

                                                        const temp_orders = temporary_orders.map((taxi) => {

                                                            var cctv = JSON.parse(taxi.coordinatesClient)

                                                            return {

                                                                orderId : taxi.id,
                                                                clientId : taxi.clientId,
                                                                driverId : taxi.driver_id,
                                                                coordinates : JSON.parse(taxi.coordinatesClient),
                                                                locationInit : taxi.locationInit,
                                                                timestampInit : taxi.timestampInit,
                                                                estimatedTime : taxi.estimatedTime,
                                                                timestamp : taxi.timestamp,
                                                                meter : geolib.getDistance(
                                                                    { latitude: the_coordinates.lat, longitude: the_coordinates.lng },
                                                                    { latitude: cctv.lat, longitude: cctv.lng }
                                                                )
                                                            }
                                                        })

                                                        //const result = driver.filter(driver => driver.meter <= 2000);
                                                    
                                                        res.json({ temp_orders : temp_orders });

                                                    } else {        // ********************************** if there's no record of any temporary orders **********************************/

                                                        res.json({
                    
                                                            "msg": "success",
                                                            "order": null,
                                                            "voice" : null
                        
                                                        });

                                                    }

                                                }) .catch(error => next(error));
                                                
                                            } else { //************************** if there's an order *****************************************************/

                                                VoiceTable.getVoiceByDriverId(account.id, order.id, account.id)
                                                .then(({ voice }) => { 

                                                    console.log("A9 getVoiceByDriverId")

                                                    const driver_account = account;

                                                    AccountTable.getAccountById(clientId)
                                                    .then(({ account }) => {

                                                        console.log("A10 getAccountById")

                                                        var clientId = order.customerId;
                            
                                                        var msg = "you have been chosen with : " + clientId

                                                        var coord_customer = JSON.parse(order.coordinatesInit);
                                                        var coord_driver = JSON.parse(driver_account.startCoordinates)
                                                        
                                                        const distance_meter = geolib.getDistance(
                                                            { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                            { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                        )

                                                        var distance_pp = distance_meter * 1
                                                        var distance_price = distance_pp + " ₮"
                            
                                                        if(voice) { // *********************** if there's a voice ****************************************/

                                                            res.json({
                                                                "drive_temporary" : {
                                                                    "dt_id" : null,
                                                                    "drive_id" : null,
                                                                    "acc_id" : account.id,
                                                                    "order_id" : order.id,
                                                                    "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                    "drive_timestamp" : null,
                                                                    "distance_meter" : distance_meter,
                                                                    "distance_price" : distance_price
                                                                },
                                                                "customer_account" : {
                                                                    "id": account.id,
                                                                    "tel" : account.tel,
                                                                    "name" : account.name,
                                                                    "avatar" : account.avatar,
                                                                    "driver" : account.driver,
                                                                    "timestamp" : account.timestamp,
                                                                    "coordinates": JSON.parse(account.startCoordinates),
                                                                    "online" : account.online,
                                                                    "status": account.status
                                                                },
                                                                "order" : {
                                                                    "id" : order.id,
                                                                    "tel" : order.tel,
                                                                    "customerId" : order.customerId,
                                                                    "driverId": order.driverId,
                                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                    "locationInit": order.locationInit,
                                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                    "locationDest" : order.locationDest,
                                                                    "timestampInit" : order.timestampInit,
                                                                    "timestampArrive" : order.timestampArrive,
                                                                    "timestampClosed" : order.timestampClosed,
                                                                    "timestampStarted" : order.timestampStarted,
                                                                    "timestampDrive" : order.timestampDrive,
                                                                    "timestampStopped" : order.timestampStopped,
                                                                    "timestampPaid" : order.timestampPaid,
                                                                    "status" : order.status,
                                                                    "extra" : order.extra,
                                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                                    "description" : order.description,
                                                                    "comment" : order.comment,
                                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                    
                                                                },
                                                                "voice" :{
                                                                    "id" : voice.id,
                                                                    "orderId" : voice.orderId,
                                                                    "customerId" : voice.customerId,
                                                                    "driverId" : voice.driverId,
                                                                    "voicePath" : voice.voicePath,
                                                                    "timestamp" : voice.timestamp
                                                                },
                                                                msg
                                                                
                                                            });
                            
                                                        } else { //********************************* if there's no record of voice ***************************/
                            
                                                            res.json({
                                                                msg,
                                                                "voice" : null,
                                                                "drive_temporary" : {
                                                                    "dt_id" : null,
                                                                    "drive_id" : null,
                                                                    "acc_id" : account.id,
                                                                    "order_id" : order.id,
                                                                    "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                    "drive_timestamp" : null,
                                                                    "distance_meter" : distance_meter,
                                                                    "distance_price" : distance_price
                                                                },
                                                                "customer_account" : {
                                                                    "id": account.id,
                                                                    "tel" : account.tel,
                                                                    "name" : account.name,
                                                                    "avatar" : account.avatar,
                                                                    "driver" : account.driver,
                                                                    "timestamp" : account.timestamp,
                                                                    "coordinates": JSON.parse(account.startCoordinates),
                                                                    "online" : account.online,
                                                                    "status": account.status
                                                                } ,
                                                                "order" : {
                                                                    "id" : order.id,
                                                                    "tel" : order.tel,
                                                                    "customerId" : order.customerId,
                                                                    "driverId": order.driverId,
                                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                    "locationInit": order.locationInit,
                                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                    "locationDest" : order.locationDest,
                                                                    "timestampInit" : order.timestampInit,
                                                                    "timestampArrive" : order.timestampArrive,
                                                                    "timestampClosed" : order.timestampClosed,
                                                                    "timestampStarted" : order.timestampStarted,
                                                                    "timestampDrive" : order.timestampDrive,
                                                                    "timestampStopped" : order.timestampStopped,
                                                                    "timestampPaid" : order.timestampPaid,
                                                                    "status" : order.status,
                                                                    "extra" : order.extra,
                                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                                    "description" : order.description,
                                                                    "comment" : order.comment,
                                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                    
                                                                }, 

                                                            });
                            
                                                        }
                                                        
                                                    }) .catch(error => next(error));

                                                }) .catch(error => next(error));
                    
                                            }; //******************************* if there's an order (end of the else statement) ******************************/
                            
                                        }) .catch(error => next(error));
                            
                                    }) .catch(error => next(error));
                    
                                }) .catch(error => next(error));
    
                            }) .catch(error => next(error));

                        }) .catch(error => next(error));
        
                    }) .catch(error => next(error));

                } else {  // ----------------------------------------- if the account has a record of drive ---------------------------------------/

                    Drive_historyTable.getDriveHistory( account.id )
                    .then(({ drive_history }) => {

                        console.log("A11 getDriveHistory")
    
                        if (drive_history !== undefined ) { //****************************** if the account has a record of drive_history ***********************/
    
                            Drive_historyTable.deleteDriveHistory( drive_history.accountId )
                            .catch(error => next(error));

                            console.log("A12 deleteDriveHistory")
                            
                        } 

                        Drive_historyTable.storeDriveHistory(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].status, drive[0].driver, drive[0].timestamp)
                        .then(() => {

                            console.log("A13 storeDriveHistory")
        
                            DriveTable.storeDrive(account.id, coordinates, account.online, account.status, account.driver)
                            .then(() => {
                
                                console.log("A14 storeDrive")

                                AccountTable.updateOnline(online, account.id)
                                .then(()=>{

                                    console.log("A15 updateOnline")
                
                                    AccountTable.updateStartCoordinates( coordinates, account.id )
                                    .then(() => {

                                        console.log("A16 updateStartCoordinates")
    
                                        OrdersTable.getOrderByDriverId(account.id)
                                        .then(({ order }) => {

                                            console.log("A17 getOrderByDriverId")
                            
                                            if(order === undefined){ //**************************************************  if there's no order ****************************************/
                                                
                                                OrdersTemporaryTable.getOrderTemporaryByDriverId(account.id)
                                                .then(( temporary_orders ) => {

                                                    console.log("A18 getOrderByDriverId")
                                               
                                                    // This if statement has to be checked only by length property//
                                                    if (temporary_orders.length !== 0) { //**************************************************  if there's a temporary orders ****************************************/

                                                        var the_coordinates = coordinates

                                                        const temp_orders = temporary_orders.map((taxi) => {

                                                            var cctv = JSON.parse(taxi.coordinatesClient)

                                                            return {

                                                            orderId : taxi.id,
                                                            clientId : taxi.clientId,
                                                            driverId : taxi.driver_id,
                                                            coordinates : JSON.parse(taxi.coordinatesClient),
                                                            locationInit : taxi.locationInit,
                                                            timestampInit : taxi.timestampInit,
                                                            estimatedTime : taxi.estimatedTime,
                                                            timestamp : taxi.timestamp,
                                                            meter : geolib.getDistance(
                                                                { latitude: the_coordinates.lat, longitude: the_coordinates.lng },
                                                                { latitude: cctv.lat, longitude: cctv.lng }
                                                            )
                                                            }
                                                        })

                                                        //const result = driver.filter(driver => driver.meter <= 2000);
                                                    
                                                        res.json({ temp_orders : temp_orders });

                                                    } else { //**************************************************  if there's no temporary orders ****************************************/

                                                        res.json({
                    
                                                            "msg": "success",
                                                            "order": null,
                                                            "voice" : null
                        
                                                        });

                                                    }

                                                }) .catch(error => next(error));
                                        
                                            } else { //**************************************************  if there's an order ****************************************/

                                                VoiceTable.getVoiceByDriverId(account.id, order.id, account.id)
                                                .then(({ voice }) => {

                                                    console.log("A19 getVoiceByDriverId")
                            
                                                    var clientId = order.customerId;

                                                    const driver_account = account;
                            
                                                    AccountTable.getAccountById(clientId)
                                                    .then(({ account }) => {

                                                        console.log("A20 getAccountById")
                            
                                                        var msg = "you have been chosen with : " + clientId
                            
                                                        if(voice) { //**************************************************  if there's a voice ****************************************/

                                                            if (order.status == "DRIVING") { //**************************************************  if the order status is DRIVING ****************************************/

                                                                Drive_temporaryTable.getDriveTemporary(driver_account.id)
                                                                .then(( drive_temporary ) => {

                                                                    //this is were the error comes out. This if has to checked with undefined ////////

                                                                    console.log("A21 getAccountById /-/-/-/-/-/-/-/-/-/--/-/-/-/-/-/-/", drive_temporary)

                                                                    if(drive_temporary === undefined) { //**************************************************  if there's no drive_temporary ****************************************/

                                                                        var distance_meter = 0;

                                                                        DriveTable.getDriveByAccount(driver_account.id)
                                                                        .then(({ drive }) => {

                                                                            console.log("A22 getAccountById")

                                                                            Drive_temporaryTable.storeDriveTemporary(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].timestamp, distance_meter, order.id)
                                                                            .then(() => {

                                                                                console.log("A23 storeDriveTemporary")

                                                                                Drive_temporaryTable.getDriveTemporary(driver_account.id)
                                                                                .then(( drive_temporary ) => {

                                                                                    console.log("A24 storeDriveTemporary")

                                                                                    var distance_pp = drive_temporary.distance_meter * 1
                                                                                    var distance_price = distance_pp + " ₮"

                                                                                    res.json({
                                                                                        msg,
                                                                                        "voice" :{
                                                                                            "id" : voice.id,
                                                                                            "orderId" : voice.orderId,
                                                                                            "customerId" : voice.customerId,
                                                                                            "driverId" : voice.driverId,
                                                                                            "voicePath" : voice.voicePath,
                                                                                            "timestamp" : voice.timestamp
                                                                                        },
                                                                                        "drive_temporary" : {
                                                                                            "dt_id" : drive_temporary.dt_id,
                                                                                            "drive_id" : drive_temporary.drive_id,
                                                                                            "acc_id" : drive_temporary.acc_id,
                                                                                            "order_id" : drive_temporary.order_id,
                                                                                            "drive_coordinates" : JSON.parse(drive_temporary.drive_coordinates),
                                                                                            "drive_timestamp" : drive_temporary.drive_timestamp,
                                                                                            "distance_meter" : drive_temporary.distance_meter,
                                                                                            "distance_price" : distance_price
                                                                                        },
                                                                                        "customer_account" : {
                                                                                            "id": account.id,
                                                                                            "tel" : account.tel,
                                                                                            "name" : account.name,
                                                                                            "avatar" : account.avatar,
                                                                                            "driver" : account.driver,
                                                                                            "timestamp" : account.timestamp,
                                                                                            "coordinates": JSON.parse(account.startCoordinates),
                                                                                            "online" : account.online,
                                                                                            "status": account.status
                                                                                        } ,
                                                                                        "order" : {
                                                                                            "id" : order.id,
                                                                                            "tel" : order.tel,
                                                                                            "customerId" : order.customerId,
                                                                                            "driverId": order.driverId,
                                                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                            "locationInit": order.locationInit,
                                                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                            "locationDest" : order.locationDest,
                                                                                            "timestampInit" : order.timestampInit,
                                                                                            "timestampArrive" : order.timestampArrive,
                                                                                            "timestampClosed" : order.timestampClosed,
                                                                                            "timestampStarted" : order.timestampStarted,
                                                                                            "timestampDrive" : order.timestampDrive,
                                                                                            "timestampStopped" : order.timestampStopped,
                                                                                            "timestampPaid" : order.timestampPaid,
                                                                                            "status" : order.status,
                                                                                            "extra" : order.extra,
                                                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                            "description" : order.description,
                                                                                            "comment" : order.comment,
                                                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                            
                                                                                        }

                                                                                    });

                                                                                }) .catch(error => next(error));
    
                                                                            }) .catch(error => next(error));

                                                                        }) .catch(error => next(error));

                                                                    } else { //**************************************************  if there's a record of drive_temporary ****************************************/

                                                                        var meter = drive_temporary.distance_meter
                                                                        var dt_coordinates = JSON.parse(drive_temporary.drive_coordinates)

                                                                        var distance = geolib.getDistance(
                                                                            { latitude: dt_coordinates.lat, longitude: dt_coordinates.lng },
                                                                            { latitude: coordinates.lat, longitude: coordinates.lng }
                                                                        )

                                                                        var distance_meter = meter + distance;

                                                                        var distance_pp = distance_meter.distance_meter * 1
                                                                        var distance_price = distance_pp + " ₮"

                                                                        DriveTable.getDriveByAccount(driver_account.id)
                                                                        .then(({ drive }) => {

                                                                            console.log("A25 storeDriveTemporary")

                                                                            Drive_temporaryTable.storeDriveTemporary(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].timestamp, distance_meter, order.id)
                                                                            .then(() => {

                                                                                console.log("A26 storeDriveTemporary")

                                                                                Drive_temporaryTable.getDriveTemporary(driver_account.id)
                                                                                .then(( drive_temporary ) => {

                                                                                    console.log("A27 storeDriveTemporary")

                                                                                    res.json({

                                                                                        msg,
                                                                                        "drive_temporary" : {
                                                                                            "dt_id" : drive_temporary.dt_id,
                                                                                            "drive_id" : drive_temporary.drive_id,
                                                                                            "acc_id" : drive_temporary.acc_id,
                                                                                            "order_id" : drive_temporary.order_id,
                                                                                            "drive_coordinates" : JSON.parse(drive_temporary.drive_coordinates),
                                                                                            "drive_timestamp" : drive_temporary.drive_timestamp,
                                                                                            "distance_meter" : drive_temporary.distance_meter,
                                                                                            "distance_price" : distance_price
                                                                                        },
                                                                                        "voice" :{
                                                                                            "id" : voice.id,
                                                                                            "orderId" : voice.orderId,
                                                                                            "customerId" : voice.customerId,
                                                                                            "driverId" : voice.driverId,
                                                                                            "voicePath" : voice.voicePath,
                                                                                            "timestamp" : voice.timestamp
                                                                                        },
                                                                                        "customer_account" : {
                                                                                            "id": account.id,
                                                                                            "tel" : account.tel,
                                                                                            "name" : account.name,
                                                                                            "avatar" : account.avatar,
                                                                                            "driver" : account.driver,
                                                                                            "timestamp" : account.timestamp,
                                                                                            "coordinates": JSON.parse(account.startCoordinates),
                                                                                            "online" : account.online,
                                                                                            "status": account.status
                                                                                        } ,
                                                                                        "order" : {
                                                                                            "id" : order.id,
                                                                                            "tel" : order.tel,
                                                                                            "customerId" : order.customerId,
                                                                                            "driverId": order.driverId,
                                                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                            "locationInit": order.locationInit,
                                                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                            "locationDest" : order.locationDest,
                                                                                            "timestampInit" : order.timestampInit,
                                                                                            "timestampArrive" : order.timestampArrive,
                                                                                            "timestampClosed" : order.timestampClosed,
                                                                                            "timestampStarted" : order.timestampStarted,
                                                                                            "timestampDrive" : order.timestampDrive,
                                                                                            "timestampStopped" : order.timestampStopped,
                                                                                            "timestampPaid" : order.timestampPaid,
                                                                                            "status" : order.status,
                                                                                            "extra" : order.extra,
                                                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                            "description" : order.description,
                                                                                            "comment" : order.comment,
                                                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                            
                                                                                        }

                                                                                    });

                                                                                }) .catch(error => next(error));
    
                                                                            }) .catch(error => next(error));

                                                                        }) .catch(error => next(error));

                                                                    };

                                                                }) .catch(error => next(error));
    
                                                            } else { //**************************************************  if the order status is not DRIVING ****************************************/

                                                                var coord_customer = JSON.parse(order.coordinatesInit);
                                                                var coord_driver = JSON.parse(driver_account.startCoordinates)

                                                                const distance_meter = geolib.getDistance(
                                                                    { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                    { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                )

                                                                var distance_pp = distance_meter * 1
                                                                var distance_price = distance_pp + " ₮"

                                                                res.json({

                                                                    msg,
                                                                    "drive_temporary" : {
                                                                        "dt_id" : null,
                                                                        "drive_id" : null,
                                                                        "acc_id" : account.id,
                                                                        "order_id" : order.id,
                                                                        "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                        "drive_timestamp" : null,
                                                                        "distance_meter" : distance_meter,
                                                                        "distance_price" : distance_price
                                                                    },
                                                                    "voice" :{
                                                                        "id" : voice.id,
                                                                        "orderId" : voice.orderId,
                                                                        "customerId" : voice.customerId,
                                                                        "driverId" : voice.driverId,
                                                                        "voicePath" : voice.voicePath,
                                                                        "timestamp" : voice.timestamp
                                                                    },
                                                                    "customer_account" : {
                                                                        "id": account.id,
                                                                        "tel" : account.tel,
                                                                        "name" : account.name,
                                                                        "avatar" : account.avatar,
                                                                        "driver" : account.driver,
                                                                        "timestamp" : account.timestamp,
                                                                        "coordinates": JSON.parse(account.startCoordinates),
                                                                        "online" : account.online,
                                                                        "status": account.status
                                                                    } ,
                                                                    "order" : {
                                                                        "id" : order.id,
                                                                        "tel" : order.tel,
                                                                        "customerId" : order.customerId,
                                                                        "driverId": order.driverId,
                                                                        "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                        "locationInit": order.locationInit,
                                                                        "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                        "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                        "locationDest" : order.locationDest,
                                                                        "timestampInit" : order.timestampInit,
                                                                        "timestampArrive" : order.timestampArrive,
                                                                        "timestampClosed" : order.timestampClosed,
                                                                        "timestampStarted" : order.timestampStarted,
                                                                        "timestampDrive" : order.timestampDrive,
                                                                        "timestampStopped" : order.timestampStopped,
                                                                        "timestampPaid" : order.timestampPaid,
                                                                        "status" : order.status,
                                                                        "extra" : order.extra,
                                                                        "lastVoiceDriver" : order.lastVoiceDriver,
                                                                        "description" : order.description,
                                                                        "comment" : order.comment,
                                                                        "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                        "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                        "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                        
                                                                    }

                                                                });

                                                            }
                            
                                                        } else { //**************************************************  if there's no voice ****************************************/

                                                            if (order.status == "DRIVING") {

                                                                Drive_temporaryTable.getDriveTemporary(driver_account.id)
                                                                .then(( drive_temporary ) => {

                                                                    console.log("A28 storeDriveTemporary")

                                                                    if(drive_temporary === undefined) {   // ************************** if there's no record of drive_temporary *****************************/

                                                                        var distance_meter = 0;

                                                                        DriveTable.getDriveByAccount(driver_account.id)
                                                                        .then(({ drive }) => {

                                                                            console.log("A29 getDriveByAccount")

                                                                            Drive_temporaryTable.storeDriveTemporary(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].timestamp, distance_meter, order.id)
                                                                            .then(() => {

                                                                                console.log("A30 storeDriveTemporary")

                                                                                Drive_temporaryTable.getDriveTemporary(driver_account.id)
                                                                                .then(( drive_temporary ) => {

                                                                                    console.log("A31 getDriveTemporary")

                                                                                    var distance_pp = drive_temporary.distance_meter * 1
                                                                                    var distance_price = distance_pp + " ₮"

                                                                                    res.json({
                                                                                        msg,
                                                                                        "voice" : null,
                                                                                        "drive_temporary" : {
                                                                                            "dt_id" : drive_temporary.dt_id,
                                                                                            "drive_id" : drive_temporary.drive_id,
                                                                                            "acc_id" : drive_temporary.acc_id,
                                                                                            "order_id" : drive_temporary.order_id,
                                                                                            "drive_coordinates" : JSON.parse(drive_temporary.drive_coordinates),
                                                                                            "drive_timestamp" : drive_temporary.drive_timestamp,
                                                                                            "distance_meter" : drive_temporary.distance_meter,
                                                                                            "distance_price" : distance_price
                                                                                        },
                                                                                        "customer_account" : {
                                                                                            "id": account.id,
                                                                                            "tel" : account.tel,
                                                                                            "name" : account.name,
                                                                                            "avatar" : account.avatar,
                                                                                            "driver" : account.driver,
                                                                                            "timestamp" : account.timestamp,
                                                                                            "coordinates": JSON.parse(account.startCoordinates),
                                                                                            "online" : account.online,
                                                                                            "status": account.status
                                                                                        } ,
                                                                                        "order" : {
                                                                                            "id" : order.id,
                                                                                            "tel" : order.tel,
                                                                                            "customerId" : order.customerId,
                                                                                            "driverId": order.driverId,
                                                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                            "locationInit": order.locationInit,
                                                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                            "locationDest" : order.locationDest,
                                                                                            "timestampInit" : order.timestampInit,
                                                                                            "timestampArrive" : order.timestampArrive,
                                                                                            "timestampClosed" : order.timestampClosed,
                                                                                            "timestampStarted" : order.timestampStarted,
                                                                                            "timestampDrive" : order.timestampDrive,
                                                                                            "timestampStopped" : order.timestampStopped,
                                                                                            "timestampPaid" : order.timestampPaid,
                                                                                            "status" : order.status,
                                                                                            "extra" : order.extra,
                                                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                            "description" : order.description,
                                                                                            "comment" : order.comment,
                                                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                            
                                                                                        }, 
                                                                                        

                                                                                    });

                                                                                }) .catch(error => next(error));
    
                                                                            }) .catch(error => next(error));

                                                                        }) .catch(error => next(error));

                                                                    } else {    // ********************* if there's a record of drive_temporary ********************

                                                                        var meter = drive_temporary.distance_meter
                                                                        var dt_coordinates = JSON.parse(drive_temporary.drive_coordinates)

                                                                        var distance = geolib.getDistance(
                                                                            { latitude: dt_coordinates.lat, longitude: dt_coordinates.lng },
                                                                            { latitude: coordinates.lat, longitude: coordinates.lng }
                                                                        )

                                                                        var distance_meter = meter + distance;

                                                                        var distance_pp = distance_meter * 1
                                                                        var distance_price = distance_pp + " ₮"

                                                                        DriveTable.getDriveByAccount(driver_account.id)
                                                                        .then(({ drive }) => {

                                                                            console.log("A32 getDriveByAccount")

                                                                            Drive_temporaryTable.storeDriveTemporary(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].timestamp, distance_meter, order.id)
                                                                            .then(() => {

                                                                                console.log("A33 storeDriveTemporary")

                                                                                Drive_temporaryTable.getDriveTemporary(driver_account.id)
                                                                                .then(( drive_temporary ) => {

                                                                                    console.log("A34 getDriveTemporary")

                                                                                    res.json({

                                                                                        msg,
                                                                                        "voice" : null,
                                                                                        "drive_temporary" : {
                                                                                            "dt_id" : drive_temporary.dt_id,
                                                                                            "drive_id" : drive_temporary.drive_id,
                                                                                            "acc_id" : drive_temporary.acc_id,
                                                                                            "order_id" : drive_temporary.order_id,
                                                                                            "drive_coordinates" : JSON.parse(drive_temporary.drive_coordinates),
                                                                                            "drive_timestamp" : drive_temporary.drive_timestamp,
                                                                                            "distance_meter" : drive_temporary.distance_meter,
                                                                                            "distance_price" : distance_price
                                                                                        },
                                                                                        "customer_account" : {
                                                                                            "id": account.id,
                                                                                            "tel" : account.tel,
                                                                                            "name" : account.name,
                                                                                            "avatar" : account.avatar,
                                                                                            "driver" : account.driver,
                                                                                            "timestamp" : account.timestamp,
                                                                                            "coordinates": JSON.parse(account.startCoordinates),
                                                                                            "online" : account.online,
                                                                                            "status": account.status
                                                                                        },
                                                                                        "order" : {
                                                                                            "id" : order.id,
                                                                                            "tel" : order.tel,
                                                                                            "customerId" : order.customerId,
                                                                                            "driverId": order.driverId,
                                                                                            "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                            "locationInit": order.locationInit,
                                                                                            "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                            "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                            "locationDest" : order.locationDest,
                                                                                            "timestampInit" : order.timestampInit,
                                                                                            "timestampArrive" : order.timestampArrive,
                                                                                            "timestampClosed" : order.timestampClosed,
                                                                                            "timestampStarted" : order.timestampStarted,
                                                                                            "timestampDrive" : order.timestampDrive,
                                                                                            "timestampStopped" : order.timestampStopped,
                                                                                            "timestampPaid" : order.timestampPaid,
                                                                                            "status" : order.status,
                                                                                            "extra" : order.extra,
                                                                                            "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                            "description" : order.description,
                                                                                            "comment" : order.comment,
                                                                                            "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                            "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                            "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                            
                                                                                        }

                                                                                    });

                                                                                }) .catch(error => next(error));
    
                                                                            }) .catch(error => next(error));

                                                                        }) .catch(error => next(error));

                                                                    };

                                                                }) .catch(error => next(error));
    
                                                            } else {   // ************** "if there's no voice" - else statement ********************.


                                                                var coord_customer = JSON.parse(order.coordinatesInit);
                                                                var coord_driver = JSON.parse(driver_account.startCoordinates)

                                                                const distance_meter = geolib.getDistance(
                                                                    { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                    { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                )

                                                                var distance_pp = distance_meter * 1
                                                                var distance_price = distance_pp + " ₮"


                                                                res.json({

                                                                    msg,
                                                                    "drive_temporary" : {
                                                                        "dt_id" : null,
                                                                        "drive_id" : null,
                                                                        "acc_id" : account.id,
                                                                        "order_id" : order.id,
                                                                        "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                        "drive_timestamp" : null,
                                                                        "distance_meter" : distance_meter,
                                                                        "distance_price" : distance_price
                                                                    },
                                                                    "voice" : null,
                                                                    "customer_account" : {
                                                                        "id": account.id,
                                                                        "tel" : account.tel,
                                                                        "name" : account.name,
                                                                        "avatar" : account.avatar,
                                                                        "driver" : account.driver,
                                                                        "timestamp" : account.timestamp,
                                                                        "coordinates": JSON.parse(account.startCoordinates),
                                                                        "online" : account.online,
                                                                        "status": account.status
                                                                    } ,
                                                                    "order" : {
                                                                        "id" : order.id,
                                                                        "tel" : order.tel,
                                                                        "customerId" : order.customerId,
                                                                        "driverId": order.driverId,
                                                                        "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                        "locationInit": order.locationInit,
                                                                        "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                        "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                        "locationDest" : order.locationDest,
                                                                        "timestampInit" : order.timestampInit,
                                                                        "timestampArrive" : order.timestampArrive,
                                                                        "timestampClosed" : order.timestampClosed,
                                                                        "timestampStarted" : order.timestampStarted,
                                                                        "timestampDrive" : order.timestampDrive,
                                                                        "timestampStopped" : order.timestampStopped,
                                                                        "timestampPaid" : order.timestampPaid,
                                                                        "status" : order.status,
                                                                        "extra" : order.extra,
                                                                        "lastVoiceDriver" : order.lastVoiceDriver,
                                                                        "description" : order.description,
                                                                        "comment" : order.comment,
                                                                        "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                        "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                        "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                       
                                                                    }

                                                                });

                                                            }; // *************************** end of the else statement inside "if there's no voice" - else statement. : if order status is not DRIVING.****************************/
                            
                                                        }; // *************************** the end of the else statement : if there's no voice. ***********************************/
                                                        
                                                    }) .catch(error => next(error));
                            
                                                }) .catch(error => next(error));

                                            } // ************************* end of the else statement. If there's not order. Which is there's an order. ******************************/

                                        }) .catch(error => next(error));
                            
                                    }) .catch(error => next(error));
                    
                                }) .catch(error => next(error));
                
                            }) .catch(error => next(error));
        
                        }) .catch(error => next(error));
        
                    }) .catch(error => next(error))
    
                } // ************************************** end of the else statement : if the account has a record of drive *******************************************/

            }) .catch(error => next(error));

        } else { // ************************* if the account is not a driver *********************************/

            DriveTable.getDriveByAccount(account.id)
            .then(({ drive }) => {

                console.log("B1 getDriveByAccount");

                if (!drive === undefined || drive.length === 0) { //**************** if account hasn't any record of drive ************/

                    DriveTable.storeDrive(account.id, coordinates, account.online, account.status, account.driver)
                    .then(() => {

                        console.log("B2 storeDrive");

                        DriveTable.getDriveByAccount(account.id)
                        .then(({ drive }) => { 

                            console.log("B3 getDriveByAccount");

                            Drive_historyTable.storeDriveHistory(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].status, drive[0].driver, drive[0].timestamp)
                            .then(() => { 

                                console.log("B4 storeDriveHistory");
    
                                AccountTable.updateOnline(online, account.id)
                                .then(()=>{

                                    console.log("B5 updateOnline");
        
                                    AccountTable.updateStartCoordinates( coordinates, account.id )
                                    .then(() => {

                                        console.log("B6 updateStartCoordinates");
        
                                        OrdersTable.getOrderCustomer(account.id)
                                        .then(({ order }) => {

                                            console.log("B7 getOrderCustomer");                                          
                            
                                            if (order !== undefined || order !== null) { //************ if the account has any record of order ****************/

                                                VoiceTable.getVoiceByCustomerId(account.id, order.id, account.id)
                                                .then(({ voice }) => {

                                                    var extra = order.extra;
                                                    var minutes = Math.floor(extra);
                                                    var driverId = order.driverId;

                                                    console.log("B8 getVoiceByCustomerId");
                            
                                                    AccountTable.getAccountById(driverId)
                                                    .then(({ account }) => {

                                                        console.log("B9 getAccountById");
                            
                                                        if (!account) { //*************** if there's an order but no driver has been found yet ***********************/
                        
                                                            res.json({
                                
                                                                "msg": "success",
                                                                "drive_temporary" : {
                                                                    "dt_id" : null,
                                                                    "drive_id" : null,
                                                                    "acc_id" : null,
                                                                    "order_id" : order.id,
                                                                    "drive_coordinates" : {
                                                                        "lat" : null,
                                                                        "lng" : null
                                                                    },
                                                                    "drive_timestamp" : null,
                                                                    "distance_meter" : null,
                                                                    "distance_price" : null
                                                                },
                                                                "order" : {
                                                                    "id" : order.id,
                                                                    "tel" : order.tel,
                                                                    "customerId" : order.customerId,
                                                                    "driverId": order.driverId,
                                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                    "locationInit": order.locationInit,
                                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                    "locationDest" : order.locationDest,
                                                                    "timestampInit" : order.timestampInit,
                                                                    "timestampArrive" : order.timestampArrive,
                                                                    "timestampClosed" : order.timestampClosed,
                                                                    "timestampStarted" : order.timestampStarted,
                                                                    "timestampDrive" : order.timestampDrive,
                                                                    "timestampStopped" : order.timestampStopped,
                                                                    "timestampPaid" : order.timestampPaid,
                                                                    "status" : order.status,
                                                                    "extra" : order.extra,
                                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                                    "description" : order.description,
                                                                    "comment" : order.comment,
                                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                    
                                                                },
                                                                "driver" : null,
                                                                "voice" : null
                                                            });
                            
                                                        } else { //********************* there's an order and a driver has been found *******************/
                            
                                                            PlateTable.getPlateById(driverId)
                                                            .then(( plate_info ) => {

                                                                console.log("B9.5 getPlateById");

                                                                var coord_customer = JSON.parse(order.coordinatesInit);
                                                                var coord_driver = JSON.parse(account.startCoordinates)
                                                                
                                                                const distance_meter = geolib.getDistance(
                                                                    { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                    { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                )
        
                                                                var distance_pp = distance_meter * 1
                                                                var distance_price = distance_pp + " ₮"

                                                                if (plate_info.length !== 0) { //*************************** if the driver has an info of the car *********************/

                                                                    if (voice) { //********************if there's a voice for the client ***************************/
                                    
                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinat),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : plate_info[0].plate_num,
                                                                                "car_mark" : plate_info[0].car_mark
                                                                            }, 
                                                                            "voice" :{
                                                                                "id" : voice.id,
                                                                                "orderId" : voice.orderId,
                                                                                "customerId" : voice.customerId,
                                                                                "driverId" : voice.driverId,
                                                                                "voicePath" : voice.voicePath,
                                                                                "timestamp" : voice.timestamp
                                                                            }
                                        
                                                                        });
                                    
                                                                    } else { //********************* if there isn't any record of voice *****************************/
                                    
                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                            
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinates),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : plate_info[0].plate_num,
                                                                                "car_mark" : plate_info[0].car_mark
                                                                            }, 
                                                                            "voice" : null
                                        
                                                                        });
                                    
                                                                    }

                                                                } else { //*************************** if the driver hasn't any info of the car *********************/

                                                                    if (voice) { //********************if there's a voice for the client ***************************/
                                        
                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinat),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : null,
                                                                                "car_mark" : null
                                                                            }, 
                                                                            "voice" :{
                                                                                "id" : voice.id,
                                                                                "orderId" : voice.orderId,
                                                                                "customerId" : voice.customerId,
                                                                                "driverId" : voice.driverId,
                                                                                "voicePath" : voice.voicePath,
                                                                                "timestamp" : voice.timestamp
                                                                            }
                                        
                                                                        });
                                    
                                                                    } else { //********************* if there isn't any record of voice *****************************/
                                    
                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinates),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : null,
                                                                                "car_mark" : null
                                                                            }, 
                                                                            "voice" : null
                                        
                                                                        });
                                    
                                                                    }

                                                                }

                                                            }) .catch(error => next(error));
                            
                                                        }

                                                    }) .catch(error => next(error));
                                                    
                                                }) .catch(error => next(error));
                            
                                            } else { //******************************* if the client has not any record of an order ***************************/
                        
                                                res.json({
                        
                                                    "order" : null,
                                                    "drive_temporary" : null,
                                                    "voice" : null,
                                                    "driver" : null,
                                                    "msg":"success"
                        
                                                })
                                            }
        
                                        }) .catch(error => next(error));
                        
                                    }) .catch(error => next(error));
                    
                                }) .catch(error => next(error));
    
                            }) .catch(error => next(error))

                        }) .catch(error => next(error));

                    }) .catch(error => next(error));

                } else { //**************** if account has a record of drive ************/

                    Drive_historyTable.getDriveHistory( account.id )
                    .then(({ drive_history }) => {

                        console.log("B10 drive_history", drive_history);
    
                        if (drive_history !== undefined ) { //**************** if account hasn a record of drive_history ************/
    
                            Drive_historyTable.deleteDriveHistory( drive_history.accountId )
                            .catch(error => next(error));

                            console.log("B11 deleteDriveHistory");
                            
                        } 

                        Drive_historyTable.storeDriveHistory(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].status, drive[0].driver, drive[0].timestamp)
                        .then(() => {

                            console.log("B12 storeDriveHistory");
        
                            DriveTable.storeDrive(account.id, coordinates, account.online, account.status, account.driver)
                            .then(() => {

                                console.log("B13 storeDrive");
        
                                AccountTable.updateOnline(online, account.id)
                                .then(()=>{

                                    console.log("B14 updateOnline");
        
                                    AccountTable.updateStartCoordinates( coordinates, account.id )
                                    .then(() => {

                                        console.log("B15 updateStartCoordinates");
        
                                        OrdersTable.getOrderCustomer(account.id)
                                        .then(({ order }) => {

                                            console.log("B16 getOrderCustomer");

                                            if (order !== undefined ) { //**************** if account has a record of order ************/

                                                VoiceTable.getVoiceByCustomerId(account.id, order.id, account.id)
                                                .then(({ voice }) => {

                                                    console.log("B17 getVoiceByCustomerId");
    
                                                    var extra = order.extra;
                            
                                                    var minutes = Math.floor(extra);
                                                    var driverId = order.driverId;

                                                    var customer_account = account;
                            
                                                    AccountTable.getAccountById(driverId)
                                                    .then(({ account }) => {

                                                        console.log("B18 getVoiceByCustomerId");
                            
                                                        if (!account) { //**************** if the order of the client hasn't any driver ************/
                            
                                                            res.json({
                                
                                                                "msg": "success",
                                                                "drive_temporary" : {
                                                                    "dt_id" : null,
                                                                    "drive_id" : null,
                                                                    "acc_id" : null,
                                                                    "order_id" : order.id,
                                                                    "drive_coordinates" : {
                                                                        "lat" : null,
                                                                        "lng" : null
                                                                    },
                                                                    "drive_timestamp" : null,
                                                                    "distance_meter" : null,
                                                                    "distance_price" : null
                                                                },
                                                                "order" : {
                                                                    "id" : order.id,
                                                                    "tel" : order.tel,
                                                                    "customerId" : order.customerId,
                                                                    "driverId": order.driverId,
                                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                    "locationInit": order.locationInit,
                                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                    "locationDest" : order.locationDest,
                                                                    "timestampInit" : order.timestampInit,
                                                                    "timestampArrive" : order.timestampArrive,
                                                                    "timestampClosed" : order.timestampClosed,
                                                                    "timestampStarted" : order.timestampStarted,
                                                                    "timestampDrive" : order.timestampDrive,
                                                                    "timestampStopped" : order.timestampStopped,
                                                                    "timestampPaid" : order.timestampPaid,
                                                                    "status" : order.status,
                                                                    "extra" : order.extra,
                                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                                    "description" : order.description,
                                                                    "comment" : order.comment,
                                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                    
                                                                },
                                                                "driver" : null,
                                                                "voice" : null
                                                            });
                            
                                                        } else { //**************** if the order of the client has a driver ************/
                            
                                                            PlateTable.getPlateById(driverId)
                                                            .then(( plate_info ) => {

                                                                var coord_customer = JSON.parse(order.coordinatesInit);
                                                                var coord_driver = JSON.parse(account.startCoordinates)
                                                                
                                                                const distance_meter = geolib.getDistance(
                                                                    { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                    { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                )
        
                                                                var distance_pp = distance_meter * 1
                                                                var distance_price = distance_pp + " ₮"

                                                                console.log("B9.6 getPlateById",plate_info);

                                                                if (plate_info.length !== 0) { //*************************** if the driver has an info of the car *********************/

                                                                    if (voice) { //******************** if there's a voice for the client ***************************/

                                                                        if (order.status == "DRIVING") { //**************************************************  if the order status is DRIVING ****************************************/

                                                                            Drive_temporaryTable.getDriveTemporary(account.id)
                                                                            .then(( drive_temporary ) => {
            
                                                                                console.log("21 getAccountById")
            
                                                                                if(drive_temporary == "no record") { //**************************************************  if there's no drive_temporary ****************************************/
            
                                                                                    var distance_meter = 0;
            
                                                                                    DriveTable.getDriveByAccount(account.id)
                                                                                    .then(({ drive }) => {
            
                                                                                        console.log("22 getAccountById")
            
                                                                                        Drive_temporaryTable.storeDriveTemporary(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].timestamp, distance_meter, order.id)
                                                                                        .then(() => {
            
                                                                                            console.log("23 storeDriveTemporary")
            
                                                                                            Drive_temporaryTable.getDriveTemporary(account.id)
                                                                                            .then(( drive_temporary ) => {
            
                                                                                                console.log("24 storeDriveTemporary")
            
                                                                                                var distance_pp = drive_temporary.distance_meter * 1
                                                                                                var distance_price = distance_pp + " ₮"

                                                                                                res.json({
                                                                
                                                                                                    "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                                                    "drive_temporary" : {
                                                                                                        "dt_id" : drive_temporary.dt_id,
                                                                                                        "drive_id" : drive_temporary.drive_id,
                                                                                                        "acc_id" : drive_temporary.acc_id,
                                                                                                        "order_id" : drive_temporary.order_id,
                                                                                                        "drive_coordinates" : JSON.parse(drive_temporary.drive_coordinates),
                                                                                                        "drive_timestamp" : drive_temporary.drive_timestamp,
                                                                                                        "distance_meter" : drive_temporary.distance_meter,
                                                                                                        "distance_price" : distance_price
                                                                                                    },
                                                                                                    "order" : {
                                                                                                        "id" : order.id,
                                                                                                        "tel" : order.tel,
                                                                                                        "customerId" : order.customerId,
                                                                                                        "driverId": order.driverId,
                                                                                                        "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                                        "locationInit": order.locationInit,
                                                                                                        "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                                        "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                                        "locationDest" : order.locationDest,
                                                                                                        "timestampInit" : order.timestampInit,
                                                                                                        "timestampArrive" : order.timestampArrive,
                                                                                                        "timestampClosed" : order.timestampClosed,
                                                                                                        "timestampStarted" : order.timestampStarted,
                                                                                                        "timestampDrive" : order.timestampDrive,
                                                                                                        "timestampStopped" : order.timestampStopped,
                                                                                                        "timestampPaid" : order.timestampPaid,
                                                                                                        "status" : order.status,
                                                                                                        "extra" : order.extra,
                                                                                                        "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                                        "description" : order.description,
                                                                                                        "comment" : order.comment,
                                                                                                        "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                                        "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                                        "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                                    },
                                                                                                    "driver" : {
                                                                                                        "id": account.id,
                                                                                                        "tel" : account.tel,
                                                                                                        "name" : account.name,
                                                                                                        "avatar" : account.avatar,
                                                                                                        "driver" : account.driver,
                                                                                                        "timestamp" : account.timestamp,
                                                                                                        "coordinates": JSON.parse(account.startCoordinates),
                                                                                                        "online" : account.online,
                                                                                                        "status": account.status,
                                                                                                        "plate_num" : plate_info[0].plate_num,
                                                                                                        "car_mark" : plate_info[0].car_mark
                                                                                                    }, 
                                                                                                    "voice" :{
                                                                                                        "id" : voice.id,
                                                                                                        "orderId" : voice.orderId,
                                                                                                        "customerId" : voice.customerId,
                                                                                                        "driverId" : voice.driverId,
                                                                                                        "voicePath" : voice.voicePath,
                                                                                                        "timestamp" : voice.timestamp
                                                                                                    }
                                                                
                                                                                                });
            
                                                                                            }) .catch(error => next(error));
                
                                                                                        }) .catch(error => next(error));
            
                                                                                    }) .catch(error => next(error));
            
                                                                                } else { //**************************************************  if there's a record of drive_temporary ****************************************/
            
                                                                                    var meter = drive_temporary.distance_meter
                                                                                    var dt_coordinates = JSON.parse(drive_temporary.drive_coordinates)
            
                                                                                    var distance = geolib.getDistance(
                                                                                        { latitude: dt_coordinates.lat, longitude: dt_coordinates.lng },
                                                                                        { latitude: coordinates.lat, longitude: coordinates.lng }
                                                                                    )
            
                                                                                    var distance_meter = meter + distance;
            
                                                                                    var distance_pp = distance_meter.distance_meter * 1
                                                                                    var distance_price = distance_pp + " ₮"
            
                                                                                    DriveTable.getDriveByAccount(account.id)
                                                                                    .then(({ drive }) => {
            
                                                                                        console.log("A25 storeDriveTemporary")
            
                                                                                        Drive_temporaryTable.storeDriveTemporary(drive[0].id, drive[0].accountId, drive[0].coordinates, drive[0].timestamp, distance_meter, order.id)
                                                                                        .then(() => {
            
                                                                                            console.log("A26 storeDriveTemporary")
            
                                                                                            Drive_temporaryTable.getDriveTemporary(account.id)
                                                                                            .then(( drive_temporary ) => {
            
                                                                                                console.log("A27 storeDriveTemporary")
            
                                                                                                res.json({
                                                                
                                                                                                    "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                                                    "drive_temporary" : {
                                                                                                        "dt_id" : drive_temporary.dt_id,
                                                                                                        "drive_id" : drive_temporary.drive_id,
                                                                                                        "acc_id" : drive_temporary.acc_id,
                                                                                                        "order_id" : drive_temporary.order_id,
                                                                                                        "drive_coordinates" : JSON.parse(drive_temporary.drive_coordinates),
                                                                                                        "drive_timestamp" : drive_temporary.drive_timestamp,
                                                                                                        "distance_meter" : drive_temporary.distance_meter,
                                                                                                        "distance_price" : distance_price
                                                                                                    },
                                                                                                    "order" : {
                                                                                                        "id" : order.id,
                                                                                                        "tel" : order.tel,
                                                                                                        "customerId" : order.customerId,
                                                                                                        "driverId": order.driverId,
                                                                                                        "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                                        "locationInit": order.locationInit,
                                                                                                        "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                                        "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                                        "locationDest" : order.locationDest,
                                                                                                        "timestampInit" : order.timestampInit,
                                                                                                        "timestampArrive" : order.timestampArrive,
                                                                                                        "timestampClosed" : order.timestampClosed,
                                                                                                        "timestampStarted" : order.timestampStarted,
                                                                                                        "timestampDrive" : order.timestampDrive,
                                                                                                        "timestampStopped" : order.timestampStopped,
                                                                                                        "timestampPaid" : order.timestampPaid,
                                                                                                        "status" : order.status,
                                                                                                        "extra" : order.extra,
                                                                                                        "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                                        "description" : order.description,
                                                                                                        "comment" : order.comment,
                                                                                                        "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                                        "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                                        "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                                    },
                                                                                                    "driver" : {
                                                                                                        "id": account.id,
                                                                                                        "tel" : account.tel,
                                                                                                        "name" : account.name,
                                                                                                        "avatar" : account.avatar,
                                                                                                        "driver" : account.driver,
                                                                                                        "timestamp" : account.timestamp,
                                                                                                        "coordinates": JSON.parse(account.startCoordinates),
                                                                                                        "online" : account.online,
                                                                                                        "status": account.status,
                                                                                                        "plate_num" : plate_info[0].plate_num,
                                                                                                        "car_mark" : plate_info[0].car_mark
                                                                                                    }, 
                                                                                                    "voice" :{
                                                                                                        "id" : voice.id,
                                                                                                        "orderId" : voice.orderId,
                                                                                                        "customerId" : voice.customerId,
                                                                                                        "driverId" : voice.driverId,
                                                                                                        "voicePath" : voice.voicePath,
                                                                                                        "timestamp" : voice.timestamp
                                                                                                    }

                                                                                                });
            
                                                                                            }) .catch(error => next(error));
                
                                                                                        }) .catch(error => next(error));
            
                                                                                    }) .catch(error => next(error));
            
                                                                                };
            
                                                                            }) .catch(error => next(error));
                
                                                                        } else { //**************************************************  if the order status is not DRIVING ****************************************/
            
                                                                            var coord_customer = JSON.parse(order.coordinatesInit);
                                                                            var coord_driver = JSON.parse(account.startCoordinates)
            
                                                                            const distance_meter = geolib.getDistance(
                                                                                { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                                { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                            )
            
                                                                            var distance_pp = distance_meter * 1
                                                                            var distance_price = distance_pp + " ₮"
            
                                                                            res.json({
            
                                                                                "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                                "drive_temporary" : {
                                                                                    "dt_id" : null,
                                                                                    "drive_id" : null,
                                                                                    "acc_id" : account.id,
                                                                                    "order_id" : order.id,
                                                                                    "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                    "drive_timestamp" : null,
                                                                                    "distance_meter" : distance_meter,
                                                                                    "distance_price" : distance_price
                                                                                },
                                                                                "voice" :{
                                                                                    "id" : voice.id,
                                                                                    "orderId" : voice.orderId,
                                                                                    "customerId" : voice.customerId,
                                                                                    "driverId" : voice.driverId,
                                                                                    "voicePath" : voice.voicePath,
                                                                                    "timestamp" : voice.timestamp
                                                                                },
                                                                                "driver" : {
                                                                                    "id": account.id,
                                                                                    "tel" : account.tel,
                                                                                    "name" : account.name,
                                                                                    "avatar" : account.avatar,
                                                                                    "driver" : account.driver,
                                                                                    "timestamp" : account.timestamp,
                                                                                    "coordinates": JSON.parse(account.startCoordinates),
                                                                                    "online" : account.online,
                                                                                    "status": account.status,
                                                                                    "plate_num" : plate_info[0].plate_num,
                                                                                    "car_mark" : plate_info[0].car_mark
                                                                                },
                                                                                "order" : {
                                                                                    "id" : order.id,
                                                                                    "tel" : order.tel,
                                                                                    "customerId" : order.customerId,
                                                                                    "driverId": order.driverId,
                                                                                    "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                    "locationInit": order.locationInit,
                                                                                    "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                    "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                    "locationDest" : order.locationDest,
                                                                                    "timestampInit" : order.timestampInit,
                                                                                    "timestampArrive" : order.timestampArrive,
                                                                                    "timestampClosed" : order.timestampClosed,
                                                                                    "timestampStarted" : order.timestampStarted,
                                                                                    "timestampDrive" : order.timestampDrive,
                                                                                    "timestampStopped" : order.timestampStopped,
                                                                                    "timestampPaid" : order.timestampPaid,
                                                                                    "status" : order.status,
                                                                                    "extra" : order.extra,
                                                                                    "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                    "description" : order.description,
                                                                                    "comment" : order.comment,
                                                                                    "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                    "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                    "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                    
                                                                                }
            
                                                                            });
            
                                                                        }
                                    
                                                                        
                                    
                                                                    } else { //********************* if there isn't any record of voice *****************************/
                                    

                                                                        var coord_customer = JSON.parse(order.coordinatesInit);
                                                                        var coord_driver = JSON.parse(account.startCoordinates)
        
                                                                        const distance_meter = geolib.getDistance(
                                                                            { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                            { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                        )
        
                                                                        var distance_pp = distance_meter * 1
                                                                        var distance_price = distance_pp + " ₮"

                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinates),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : plate_info[0].plate_num,
                                                                                "car_mark" : plate_info[0].car_mark
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                            
                                                                            },
                                                                             
                                                                            "voice" : null
                                        
                                                                        });
                                    
                                                                    }

                                                                } else { //*************************** if the driver hasn't any info of the car *********************/

                                                                    if (voice) { //******************** if there's a voice for the client ***************************/
                                        

                                                                        var coord_customer = JSON.parse(order.coordinatesInit);
                                                                        var coord_driver = JSON.parse(account.startCoordinates)
        
                                                                        const distance_meter = geolib.getDistance(
                                                                            { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                            { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                        )
        
                                                                        var distance_pp = distance_meter * 1
                                                                        var distance_price = distance_pp + " ₮"

                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp,
                                                                                
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinates),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : null,
                                                                                "car_mark" : null
                                                                            }, 
                                                                            "voice" :{
                                                                                "id" : voice.id,
                                                                                "orderId" : voice.orderId,
                                                                                "customerId" : voice.customerId,
                                                                                "driverId" : voice.driverId,
                                                                                "voicePath" : voice.voicePath,
                                                                                "timestamp" : voice.timestamp
                                                                            }
                                        
                                                                        });
                                    
                                                                    } else { //********************* if there isn't any record of voice *****************************/

                                                                        var coord_customer = JSON.parse(order.coordinatesInit);
                                                                        var coord_driver = JSON.parse(account.startCoordinates)
        
                                                                        const distance_meter = geolib.getDistance(
                                                                            { latitude: coord_customer.lat, longitude: coord_customer.lng },
                                                                            { latitude: coord_driver.lat, longitude: coord_driver.lng }
                                                                        )
        
                                                                        var distance_pp = distance_meter * 1
                                                                        var distance_price = distance_pp + " ₮"

                                                                        res.json({
                                        
                                                                            "msg": `Driver with ID : ${driverId} will come to you within : ${minutes} minutes.`,
                                                                            "drive_temporary" : {
                                                                                "dt_id" : null,
                                                                                "drive_id" : null,
                                                                                "acc_id" : account.id,
                                                                                "order_id" : order.id,
                                                                                "drive_coordinates" : JSON.parse(account.startCoordinates),
                                                                                "drive_timestamp" : null,
                                                                                "distance_meter" : distance_meter,
                                                                                "distance_price" : distance_price
                                                                            },
                                                                            "order" : {
                                                                                "id" : order.id,
                                                                                "tel" : order.tel,
                                                                                "customerId" : order.customerId,
                                                                                "driverId": order.driverId,
                                                                                "coordinatesInit" : JSON.parse(order.coordinatesInit),
                                                                                "locationInit": order.locationInit,
                                                                                "coordinatesInitDriver" : JSON.parse(order.coordinatesInitDriver),
                                                                                "coordinatesDest" : JSON.parse(order.coordinatesDest),
                                                                                "locationDest" : order.locationDest,
                                                                                "timestampInit" : order.timestampInit,
                                                                                "timestampArrive" : order.timestampArrive,
                                                                                "timestampClosed" : order.timestampClosed,
                                                                                "timestampStarted" : order.timestampStarted,
                                                                                "timestampDrive" : order.timestampDrive,
                                                                                "timestampStopped" : order.timestampStopped,
                                                                                "timestampPaid" : order.timestampPaid,
                                                                                "status" : order.status,
                                                                                "extra" : order.extra,
                                                                                "lastVoiceDriver" : order.lastVoiceDriver,
                                                                                "description" : order.description,
                                                                                "comment" : order.comment,
                                                                                "lastVoiceDriverTimestamp" : order.lastVoiceDriverTimestamp,
                                                                                "lastVoiceCustomer" : order.lastVoiceCustomer,
                                                                                "lastVoiceCustomerTimestamp" : order.lastVoiceCustomerTimestamp
                                                                            },
                                                                            "driver" : {
                                                                                "id": account.id,
                                                                                "tel" : account.tel,
                                                                                "name" : account.name,
                                                                                "avatar" : account.avatar,
                                                                                "driver" : account.driver,
                                                                                "timestamp" : account.timestamp,
                                                                                "coordinates": JSON.parse(account.startCoordinates),
                                                                                "online" : account.online,
                                                                                "status": account.status,
                                                                                "plate_num" : null,
                                                                                "car_mark" : null
                                                                            }, 
                                                                            "voice" : null
                                        
                                                                        });
                                    
                                                                    }

                                                                }

                                                            }) .catch(error => next(error));
                            
                                                        }
                                
                                                    }) .catch(error => next(error));
    
                                                }) .catch(error => next(error));
                        
                                            } else { //**************** if account hasn't any record of order ************/
                        
                                                res.json({
                        
                                                    "order" : null,
                                                    "voice" : null,
                                                    "driver" : null,
                                                    "msg":"success"
                        
                                                })
                                            }
        
                                        }) .catch(error => next(error));
                        
                                    }) .catch(error => next(error));
                    
                                }) .catch(error => next(error));
        
                            }) .catch(error => next(error));
        
                        }) .catch(error => next(error))
    
                    }) .catch(error => next(error))
                
                }
                
            }) .catch(error => next(error))

        }

    }) .catch(error => res.status(401).send({error, "msg" : "Unauthorized"}));

})

router.get('/googleMap', (req, res, next) => {
    DriveTable.getAllDrive()
    .then(({ drivers }) => {

        const driver = drivers.map((taxi) => {
            return {
              id : taxi.accountId,         
              coordinates : JSON.parse(taxi.coordinates),
              online : taxi.online,
              status : taxi.status,
              driver : taxi.driver,
              
              timestamp : taxi.timestamp
            }
          })

        console.log("googleMap route drivers")

        res.json({ driver })

    }) .catch(error => next(error))
})

//******************************************************* This route has been changed completely ********************************************************************/

module.exports = router; 