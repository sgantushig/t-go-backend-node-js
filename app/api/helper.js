const AccountTable = require('../account/table');
const Login_idTable = require('../login_id/table');
const TokenTable = require('../token/table');

const { APP_SECRET } = require('../../secrets');

const jwt = require('jsonwebtoken');


const setSession = ({id, tel, pinHash, res, driver}) => {
  return new Promise((resolve, reject) => {
      
    Login_idTable.getLogin(id)
    .then(( login ) => {

      console.log("the first login :", login)

      if (login.length !== 0) {

        var increment = login[0].login_int + 1;

        console.log("login_int", login[0].login_int, increment)

        Login_idTable.updateLogin( increment ,id )
        .then(() => {

          Login_idTable.getLogin(id)
          .then(( login ) => {

            console.log("the second login :", login)

            increment = login[0].login_int

            setSessionToken({id, tel, pinHash, res, driver, increment });

          }) .catch(err => console.error(err));

        }) .catch(err => console.error(err));

      } else {

        var login_num = 1;

        Login_idTable.storeLogin(id, tel, login_num)
        .then(() => {

          Login_idTable.getLogin(id)
          .then(( login ) => {

            console.log("THE THIRD LOGIN  : ",login)

            var increment = login[0].login_int

            setSessionToken({id, tel, pinHash, res, driver, increment });

          }) .catch(err => console.error(err));

        }) .catch(err => console.error(err));

      };

    }) .catch(err => console.error(err));

  });

}

const setSessionToken = ({id, tel, pinHash, res, driver, increment}) => {

  console.log("from helper js ()()()()", id, tel, pinHash, driver, increment);

  if(driver !== undefined) { // if the user sent the driver property

    AccountTable.updateDriverStatus( driver, id)
    .then(() => {

      var token = jwt.sign({ id, tel, pinHash, driver, increment }, APP_SECRET, { expiresIn: "1h"});

      TokenTable.getToken(id)
      .then(( old_token ) => {

        console.log("old_token cheking : ", old_token)

        if (old_token.length === 0) { // if there's no old_token

          TokenTable.storeToken( id, token )
          .then(() => {
    
            res.json({
              "token": token,
              "otp": true,
              "msg" : "success" 
            });
    
          }) .catch(err => console.error(err));

        } else { // if there's an old_token

          TokenTable.updateToken( token, id )
          .then(() => {
    
            res.json({
              "token": token,
              "otp": true,
              "msg" : "success" 
            });
    
          }) .catch(err => console.error(err));

        };

      }) .catch(error => console.error(error));

    }) .catch(err => console.error(err));

  } else { // if the user didn't send the driver property

    var token = jwt.sign({id, tel, pinHash, driver, increment }, APP_SECRET, { expiresIn: "1h"});

    TokenTable.getToken(id)
    .then(( old_token ) => {

      console.log("old_token cheking 123 : ", old_token)

      if (old_token.length === 0) { // if there's no old_token

        TokenTable.storeToken( id, token )
        .then(() => {
  
          res.json({
            "token": token,
            "otp": true,
            "msg" : "success" 
          });
  
        }) .catch(err => console.error(err));

      } else { // if there's an old_token

        TokenTable.updateToken( token, id )
        .then(() => {
  
          res.json({
            "token": token,
            "otp": true,
            "msg" : "success" 
          });
  
        }) .catch(err => console.error(err));

      };

    }) .catch(error => console.error(error));

  };

};

const deleteSessionToken = ({res, token}) => {

  console.log("from the delete setsessiontoken : ", token)

  const {id, tel, pinHash, driver, increment} = jwt.decode(token.token, APP_SECRET);

  TokenTable.deleteToken(id)
    res.json({
      "token": null,
      "msg": "token deleted" });

};

const authenticatedAccount = (token) => {

  return new Promise((resolve, reject ) => {

    if (!token) {

      const error = new Error('Invalid token');
  
      error.statusCode = 400;
  
      return reject(error);

    } else {

      const {id, tel, pinHash, driver, increment} = jwt.decode(token, APP_SECRET);

      let authenticated = false;

        AccountTable.getAccount(tel)
        .then(({ account }) => {

          TokenTable.getToken(account.id)
          .then(( old_token ) => {

            console.log("old_token : ", old_token[0].token)

            if (token == old_token[0].token) {

              if( account && account.pinHash == pinHash) {
                authenticated = true;
              } 

              resolve({ account, authenticated});

            } else {

              const error = new Error('Invalid token');

              error.statusCode = 401;

              return reject(error);

            }

          }) .catch(err => console.error(err));

        }) .catch(err => console.error(err));

      }

  });

};

module.exports = { setSession, deleteSessionToken, authenticatedAccount };