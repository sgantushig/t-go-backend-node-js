const { Router } = require('express');
const geolib = require('geolib');

const { authenticatedAccount } = require('./helper');

const VoiceTable = require('../voice/table');
const DriveTable = require('../drive/table');
const OrdersTemporaryTable = require('../ordersTemporary/table');
const OrdersTable = require('../orders/table');
const AccountTable = require('../account/table');
const Drive_historyTable = require('../drive_history/table');
const Drive_temporaryTable = require('../drive_temporary/table');

const router = new Router();

router.get('/', (req, res, next) => {

    Drive_temporaryTable.getDrive()
    .then(({ drive }) => {
        drive.sort((a, b) => parseFloat(b.id) - parseFloat(a.id));
        res.json(drive);
    }) .catch(error => next(error));
});

router.post('/', (req, res, next) => {

    const { token } = req.body;

    authenticatedAccount(token)
    .then(({ account }) => {

    })



});

module.exports = router;