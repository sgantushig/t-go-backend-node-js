const pool = require('../../databasePool');

class OrdersTemporaryTable {

    static getOrderTemporaryCoordinatesClient(driver_id) {
        return new Promise((resolve, reject) => {

            pool.query(
                `SELECT "coordinatesClient" FROM OrdersTemporary WHERE "driver_id" = $1 ORDER BY orders_temp_id DESC
                LIMIT 1`,
                [driver_id],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrderTemporaryCoordinatesClient");

                console.log("OrdersTemporaryTable.getOrderTemporaryCoordinatesClient");
                resolve(response.rows[0]);
                }
            );
        });
    };

    static getOrderTemporaryClientId(driver_id) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "clientId" FROM OrdersTemporary WHERE "driver_id" = $1 ORDER BY orders_temp_id DESC
                LIMIT 1`,
                [driver_id],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrderTemporaryClientId");

                console.log( "from getOrderTemporaryClientId : ", response.rows[0])
                resolve( response.rows[0]);
                }
            );
        });
    };

    static getOrderTemporarySpecial(driver_id, clientId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "estimatedTime" FROM OrdersTemporary WHERE "driver_id" != $1 AND "clientId" = $2 ORDER BY orders_temp_id DESC
                LIMIT 1`,
                [driver_id, clientId],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrderTemporarySpecial");

                console.log("OrdersTemporaryTable.getOrderTemporarySpecial", response.rows[0]);
                resolve( response.rows[0] );
                }
            );
        });
    };

    static getOrderTemporarySpecial_2(driver_id, clientId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "estimatedTime" FROM OrdersTemporary WHERE "driver_id" = $1 AND "clientId" = $2 ORDER BY orders_temp_id DESC
                LIMIT 1`,
                [driver_id, clientId],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrderTemporarySpecial_2");

                console.log("OrdersTemporaryTable.getOrderTemporarySpecial_2", response.rows[0]);
                resolve( response.rows[0] );
                }
            );
        });
    };

    static getOrdersTemporary() {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM OrdersTemporary `,
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrdersTemporary");

                console.log("OrdersTemporaryTable.getOrdersTemporary", response.rows);
                resolve({ordersTemporary: response.rows});
                }
            );
        });
    };


//*********************************** This is were the codes I wrote start. *****************************************************************************/


    static getOrderTemporaryByDriverId(driver_id) {
        return new Promise((resolve, reject) => {
            pool.query(
                `
                SELECT DISTINCT ON ("clientId")
                    "clientId",
                    driver_id,
                    "coordinatesClient",
                    "coordinatesCurrent",
                    "estimatedTime",
                    minkm,
                    tel,
                    "locationInit",
                    "timestampInit",
                    id
                FROM
                    OrdersTemporary
                INNER JOIN Orders ON "clientId" = "customerId"

                WHERE "driver_id" = $1 
                ORDER BY
                    "clientId" DESC
                `,
                [driver_id],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrderTemporaryBydriver_id");

                console.log( "from getOrderTemporaryBydriver_id : ", response.rows)
                resolve( response.rows );
                }
            );
        });
    };

    static getOrderTemporary_check(driver_id, customerId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `
                SELECT * FROM OrdersTemporary WHERE "driver_id" = $1 and "clientId" = $2
                `,

                [driver_id, customerId],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.getOrderTemporary_check");

                console.log( "from getOrderTemporary_check : ", response.rows)
                resolve( response.rows );
                }
            );
        });
    };

    static storeOrdersTemporary( order_id, driver_id, coordinatesCurrent, minkm, clientId, coordinatesClient, estimatedTime, status ) {
        return new Promise((resolve, reject) => {
            pool.query(
                `insert into OrdersTemporary (order_id, driver_id, "coordinatesCurrent",  minkm, "clientId", "coordinatesClient", "estimatedTime", status)
                VALUES($1, $2, $3, $4, $5, $6, $7, $8)`,
                [ order_id, driver_id, coordinatesCurrent, minkm, clientId, coordinatesClient, estimatedTime, status ],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.storeOrdersTemporary");

                console.log("OrdersTemporaryTable.storeOrdersTemporary : ", driver_id, coordinatesCurrent, minkm, clientId, coordinatesClient, estimatedTime, status, order_id);
                resolve({msg: 'success!'});
                }
            );
        });
    };

    static updateOrderTemporary( estimatedTime, driver_id ) {
        return new Promise((resolve, reject) => {
            pool.query(
                `UPDATE OrdersTemporary SET "estimatedTime" = $1 WHERE "driver_id" = $2`,
                [estimatedTime, driver_id],
                (error, response) => {
                if (error) return console.log("ERROR FOUND AT : OrdersTemporaryTable.updateOrderTemporary", error);

                console.log("OrdersTemporaryTable.updateOrderTemporary");
                resolve({msg: 'success!'});
                }
            );
        });
    };
   
    static matchOrderTemporary(id) {
        
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM OrdersTemporary WHERE "clientId" = $1`,
                [id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.matchOrderTemporary");

                console.log("OrdersTemporaryTable.matchOrderTemporary");
                resolve(response.rows);
                }
            );
        });
    };
    static deleteDrive(id) {
        
        return new Promise((resolve, reject) => {
            pool.query(
                `Delete FROM OrdersTemporary WHERE "clientId" = $1`,
                [id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTemporaryTable.deleteDrive");

                console.log("OrdersTemporaryTable.deleteDrive");
                resolve({msg: 'success!'});
                }
            );
        });
    };

//*********************************** This is were the codes I wrote end. *****************************************************************************/


}

module.exports = OrdersTemporaryTable;

// `SELECT DISTINCT ON ("driver_id") id, "driver_id", coordinates, status, timestamp
// FROM Drive ORDER BY "driver_id", timestamp DESC;`