const pool = require('../../databasePool');

class DriveTable {

    static getDrive() {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT DISTINCT ON ("accountId") online, driver, id, "accountId", coordinates, status, timestamp
                FROM Drive ORDER BY "accountId", timestamp DESC`,

                (error, response) => {
                if (error) return reject(error);
                resolve({drive: response.rows});
                }
            );
        });
    };

    static getOneDrive( accountId ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT DISTINCT ON ("accountId") online, driver, id, "accountId", coordinates, status, timestamp
                FROM Drive WHERE "accountId" != $1 and driver = true ORDER BY "accountId", timestamp DESC`,
                
                [accountId],
                (error, response) => {
                if (error) return reject(error);

                const drivers = response.rows.filter(driver => driver.status == 'FREE' && driver.online == true && driver.coordinates !== null)

               // console.log("look at this dude, getOneDrive", response.rows);
                resolve({drivers});
                }
            );
        });
    };

    static getDriveByAccount(accountId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Drive WHERE "accountId" = $1 ORDER BY id DESC LIMIT 1`,

                [accountId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : DriveTable.getDriveByAccount");

                resolve({drive : response.rows});
                }
            );
        });
    };

    static storeDrive( accountId, coordinates, online, status, driver ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Drive ("accountId", "coordinates", online, status, driver )
                VALUES ($1, $2, $3, $4, $5)`,

                [accountId, coordinates, online, status, driver],
                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }
}

module.exports = DriveTable;