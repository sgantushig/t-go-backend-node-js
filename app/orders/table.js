const pool = require('../../databasePool');

class OrdersTable {
    static getOrder(driveId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Orders
                WHERE "driverId" = $1 order by id desc limit 1
                `,

                [driveId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getOrder");

                console.log("OrdersTable.getOrder");
                resolve({order : response.rows[0]});
                }
            );
        });
    }

    static getOrderCustomer(customerId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Orders 
                WHERE "customerId" = $1 order by id desc limit 1
                `,

                [customerId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getOrderCustomer");

                console.log("OrdersTable.getOrderCustomer");
                resolve({order : response.rows[0]});
                }
            );
        });
    }

    static getOrders() {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Orders `,
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getOrderCustomer");

                console.log("OrdersTable.getOrderCustomer");
                resolve( response.rows);
                }
            );
        });
    }

    
    static storeOrderStopped(coordinatesDest ,timestampStopped, locationDest, driverId) {

        console.log("Orders Table Store order stopped", coordinatesDest, timestampStopped, driverId, locationDest);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "coordinatesDest" = $1, "timestampStopped" = $2, "locationDest" = $3
                WHERE "driverId" = $4`,
                [coordinatesDest ,timestampStopped, locationDest, driverId],

                (error, response) => {
                if (error) return reject(error) , console.log("ERROR FOUND AT : OrdersTable.storeOrderStopped");


                console.log("OrdersTable.storeOrderStopped");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

//*********************************** This is were the codes I wrote start. *****************************************************************************/


    static storeOrderAction2(coordinatesDriver, timestampStarted, driverId) {

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "coordinatesDriver" = $1, "timestampStarted" = $2 
                WHERE "driverId" = $3`,
                [coordinatesDriver, timestampStarted, driverId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.storeOrderAction2");

                console.log("OrdersTable.storeOrderAction2");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderInit(customerId, coordinatesInit, locationInit, timestampInit, tel, status) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Orders("customerId", "coordinatesInit", "locationInit", "timestampInit", tel, status)
                VALUES($1, $2, $3, $4, $5, $6)`,

                [customerId, coordinatesInit, locationInit, timestampInit, tel, status],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.storeOrderInit");

                console.log("OrdersTable.storeOrderInit");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderBilling( timestampPaid, customerId ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampPaid" = $1
                WHERE "customerId" = $2`,
                [timestampPaid, customerId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.storeOrderBilling");

                console.log("OrdersTable.storeOrderInit");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static getOrderByDriverId(driverId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Orders WHERE "driverId"=$1 order by id desc limit 1
                `,
                [driverId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.storeOrderBilling");

                console.log("OrdersTable.storeOrderBilling");
                resolve({order : response.rows[0]});
                }
            );
        });
    }

    static getOrderIdByClient(customerId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT id FROM Orders WHERE "customerId"=$1 Order by id desc limit 1`,

                [customerId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getOrderIdByClient");

                console.log("getOrderIdByClient","costumerId is :",customerId, response.rows[0],"*****************************************");
                resolve( response.rows[0] );
                }
            );
        });
    }

    static getOrderIdByDriver(driverId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT id FROM Orders WHERE "driverId"=$1 Order by id desc limit 1`,

                [driverId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getOrderIdByDriver");

                console.log("getOrderIdByDriver","driverId is :",driverId, response.rows[0],"*****************************************");

                resolve( response.rows[0] );
                }
            );
        });
    }

    static getOrderByBothId(driverId, customerId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Orders WHERE "driverId"= $1 and "customerId" = $2 Order by id desc limit 1`,

                [driverId, customerId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getOrderByBothId");

                console.log("getOrderByBothId","driverId is :",driverId, response.rows[0],"*****************************************");
                resolve( response.rows[0] );
                }
            );
        });
    }

    static getClientIdByDriver(driverId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "customerId" FROM Orders WHERE "driverId" = $1 Order by id desc limit 1`,

                [driverId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getClientIdByDriver");

                console.log("-=-=-=-=-=-=-= getClientIdByDriver","costumerId is :", driverId, response.rows[0],"=-=-=-=-=-=-=-=-=-=");
                resolve(response.rows[0]);
                }
            );
        });
    }

    static getDriverIdByClient(customerId) {
        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT "driverId" FROM Orders WHERE "customerId" = $1 Order by id desc limit 1`,

                [customerId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.getClientIdByDriver");

                console.log("-=-=-=-=-=-=-= getDriverIdByClient","costumerId is :", customerId, response.rows[0],"=-=-=-=-=-=-=-=-=-=");
                resolve(response.rows[0]);
                }
            );
        });
    }

    static updateOrderStarted(driveId, coordinatesInitDriver, timestampStarted , extra, locationInitDriver, id) {
        console.log("update order started",driveId, coordinatesInitDriver, id, timestampStarted, locationInitDriver);
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "driverId" = $1, "coordinatesInitDriver" = $2, "timestampStarted" = $3, extra = $4, "locationInitDriver" = $5
                WHERE "id" = $6`, 

                [driveId, coordinatesInitDriver, timestampStarted, extra, locationInitDriver, id],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.updateOrderStarted");

                console.log("OrdersTable.updateOrderStarted");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static updateOrderStatus(status, customerId) {

        console.log("Update order : Status /-/-/-/-/-/-/-/-/-/-/-/-/-/", status, customerId);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET status = $1
                WHERE "customerId" = $2`, 

                [status, customerId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.updateOrderStatus");

                console.log("OrdersTable.updateOrderStatus");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static updateOrderStatusDriver(status, driverId) {

        console.log("Update order : Status, Driver /-/-/-/-/-/-/-/-/-/-/-/-/-/", status, driverId);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET status = $1
                WHERE "driverId" = $2`, 

                [status, driverId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.updateOrderStatusDriver");

                console.log("OrdersTable.updateOrderStatusDriver");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static updateOrderVoice(lastVoiceDriverTimestamp, lastVoiceCustomerTimestamp, lastVoiceCustomer, lastVoiceDriver, driverId) {

        console.log("Update order : Voices /-/-/-/-/-/-/-/-/-/-/-/-/-/", lastVoiceDriverTimestamp, lastVoiceCustomerTimestamp, lastVoiceCustomer, lastVoiceDriver, driverId);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "lastVoiceDriverTimestamp" = $1, "lastVoiceCustomerTimestamp" = $2, "lastVoiceCustomer" = $3, "lastVoiceDriver" = $4
                WHERE "driverId" = $5`, 

                [lastVoiceDriverTimestamp, lastVoiceCustomerTimestamp, lastVoiceCustomer, lastVoiceDriver, driverId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.updateOrderStatusDriver");

                console.log("OrdersTable.updateOrderStatusDriver");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static updateOrderDescription(description, driverId) {

        console.log("Update order : description, Driver /-/-/-/-/-/-/-/-/-/-/-/-/-/", description, driverId);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET description = $1
                WHERE "driverId" = $2`, 

                [description, driverId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.updateOrderDescription");

                console.log("OrdersTable.updateOrderDescription");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static updateOrderComment(comment, customerId) {

        console.log("Update order : comment, Customer /-/-/-/-/-/-/-/-/-/-/-/-/-/", comment, customerId);

        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET comment = $1
                WHERE "customerId" = $2`, 

                [comment, customerId],

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.updateOrderComment");

                console.log("OrdersTable.updateOrderComment");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static deleteOrderByDriverId(driveId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `DELETE FROM Orders WHERE "driverId" = $1`
                ,
                [driveId],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : OrdersTable.deleteOrderByDriverId");

                console.log("OrdersTable.deleteOrderByDriverId");
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static deleteOrderByCustomerId(customerId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `DELETE FROM Orders WHERE "customerId" = $1`
                ,
                [customerId],
                
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : OrdersTable.deleteOrderByCustomerId");

                console.log("deleteOrderByCustomerId : ", customerId)
                resolve({ msg: 'success!'});
                }
            );
        });
    }

//*********************************** This is were the codes I wrote end. *****************************************************************************/


    static storeOrderArrive(timestampArrive, driveId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampArrive" = $1
                WHERE "driverId" = $2`,
                [timestampArrive, driveId],
                
                (error, response) => {
                if (error) return reject(error);
                    console.log(timestampArrive)
                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static storeOrderDriving(timestampDrive, driveId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampDrive" = $1
                WHERE "driverId" = $2`,
                [timestampDrive, driveId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }


    static storeOrderClosed(timestampClosed, driveId) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Orders SET "timestampClosed" = $1
                WHERE "driverId" = $2`,
                [timestampClosed, driveId],

                (error, response) => {
                if (error) return reject(error);

                resolve({ msg: 'success!'});
                }
            );
        });
    }

  
}

module.exports = OrdersTable;
