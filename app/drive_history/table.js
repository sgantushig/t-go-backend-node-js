const pool = require('../../databasePool');

class Drive_historyTable {

    static getDriveHistory( accountId ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Drive_history WHERE "accountId" = $1 ORDER BY "driveId" DESC LIMIT 1`,

                [accountId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : DriveHistoryTable.getDriveHistory");
                //if (response.rows.length === 0) return resolve({ drive : "no record" });

                //console.log(response.rows,"get drive history wahahahha")

                resolve({drive_history: response.rows[0]});
                }
            );
        });
    };

    static storeDriveHistory( driveId, accountId, coordinates, lastStatus, lastDriver, lastTimestamp ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Drive_history ( "driveId", "accountId", "lastCoordinates", "lastStatus", "lastDriver" , "lastTimestamp")
                VALUES ($1, $2, $3, $4, $5, $6)`,

                [driveId, accountId, coordinates, lastStatus, lastDriver, lastTimestamp],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : DriveHistoryTable.storeDriveHistory");

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static deleteDriveHistory( driveId ) {

        return new Promise((resolve, reject) => {
            pool.query(

                `Delete from Drive_history 
                Where "accountId" = $1` ,

                [driveId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : DriveHistoryTable.deleteDriveHistory");

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static selectFromBoth( ) {

        return new Promise((resolve, reject) => {
            pool.query(

               `SELECT DISTINCT ON ("accountId")
                    drive."accountId",
                    driver,
                    online,
                    status,
                    timestamp,
                    coordinates,
                    "lastCoordinates",
                    plate_num, 
                    car_mark
                FROM
                    Drive
                INNER JOIN drive_history ON drive_history."accountId" = drive."accountId"
                INNER JOIN plate ON driver_id = drive."accountId"
                WHERE driver = true ORDER BY "accountId", timestamp DESC;
                ` ,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : DriveHistoryTable.selectFromBoth");

                const las_record = response.rows.filter(driver => driver.status == 'FREE' && driver.online == true && driver.coordinates !== null && driver.lastCoordinates !== null);

                //console.log("DriveHistoryTable.selectFromBoth", response.rows);
                resolve({ las_record });
                }
            );
        });
    }
}

module.exports = Drive_historyTable;