const pool = require('../../databasePool');


class AccountTable {
  static storeAccount({ usernameHash, passwordHash }) {
    console.log('asds');
    return new Promise((resolve, reject) => {
      pool.query(
        `INSERT INTO admin_account("usernamehash", "passwordhash")
        VALUES($1, $2)`,
        [usernameHash, passwordHash],
        (error, response) => {
          if (error) return reject(error);

          resolve();
        }
      )
    });
  }

  static getAccount({ usernameHash }) {
    return new Promise((resolve, reject) => {
      
      pool.query(
        `SELECT id, "passwordhash", "sessionid" FROM admin_account
         WHERE "usernamehash" = $1`,
        [usernameHash],
        (error, response) => {
          if (error) return reject(error);

          resolve({ account: response.rows[0] });
        }
      );
    });
  }

  static updateSessionId({ sessionid, usernamehash }) {
    return new Promise((resolve, reject) => {
      pool.query(
        'UPDATE admin_account SET "sessionid" = $1 WHERE "usernamehash" = $2',
        [sessionid, usernamehash],
        (error, response) => { 
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }

  
}

module.exports = AccountTable;
