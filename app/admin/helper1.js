const Session = require('./session');
const AccountTable = require('./table');
const { hash } = require('./helper');

const setSession = ({ username, res, sessionid }) => {
  let session, sessionString;

  return new Promise((resolve, reject) => {
    if (sessionid) {
      sessionString = Session.sessionString({ username, id: sessionid });

      setSessionCookie({ sessionString, res });

      resolve({ message: 'session restored' });
    } else {
      session = new Session({ username });
      sessionString = session.toString();

      AccountTable.updateSessionId({
        sessionid: session.id,
        usernamehash: hash(username)
      })
      .then(() => {
        setSessionCookie({ sessionString, res });

        resolve({ message: 'session created' });
      })
      .catch(error => reject(error));
    }
  });
};

const setSessionCookie = ({ sessionString, res }) => {
  res.cookie('sessionString', sessionString, {
    expire: Date.now() + 3600000,
    httpOnly: true
    // secure: process.env.NODE_ENV === 'PROD'
  });
};
const authenticatedAccount = ({ sessionString }) => {
  return new Promise((resolve, reject) => {
 
    if (!sessionString || !Session.verify(sessionString)) {
      const error = new Error('Invalid session');
      error.statusCode = 400;
      return reject(error);
    }
    const { username, id } = Session.parse(sessionString);
    AccountTable.getAccount({ usernameHash: hash(username) })
      .then(({ account }) => {
        const authenticated = account.sessionid === id;
        resolve({ authenticated, account, username });
      })
      .catch(error => reject(error));
  });
};
module.exports = { setSession, authenticatedAccount };