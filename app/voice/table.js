const pool = require('../../databasePool');

class VoiceTable {

//*********************************** This is were the codes I wrote start. *****************************************************************************/

    static storeVoice(customerId, driverId, voicePath, timestamp, orderId, voiceFile) {
        
        return new Promise((resolve, reject) => {
            pool.query(
                `INSERT INTO Voice ("customerId", "driverId", "voicePath", timestamp, "orderId", "voiceFile")
                VALUES ($1, $2, $3, $4, $5, $6) 
                `,
                [customerId, driverId, voicePath, timestamp, orderId, voiceFile],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.storeVoice");

                console.log("VoiceTable.storeVoice");

                resolve({ msg: 'success!'});
                }
            );
        });
    }

    static getVoiceByCustomerId(customerId, orderId, voiceFile) {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Voice WHERE "customerId" = $1 and "orderId" = $2 and "voiceFile" = $3 order by id desc`,

                [customerId, orderId, voiceFile],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.getVoiceByCustomerId");

                console.log("VoiceTable.getVoiceByCustomerId");

                resolve({voice : response.rows[0]});
                }
            );
        });
    }

    static getVoiceId(driverId, customerId, orderId) {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT id FROM Voice WHERE "driverId" = $1 and "customerId" = $2 and "orderId" = $3 order by id desc limit 1`,

                [driverId, customerId, orderId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.getVoiceId");

                console.log("VoiceTable.getVoiceId")

                resolve({voice : response.rows[0]});
                }
            );
        });
    }

    static getVoiceByDriverId(driverId, orderId, voiceFile) {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Voice WHERE "driverId" = $1 and "orderId" = $2 and "voiceFile" = $3 order by id desc`,

                [driverId, orderId, voiceFile],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.getVoiceByDriverId");

                console.log("VoiceTable.getVoiceByDriverId", response.rows[0])

                resolve({voice : response.rows[0]});
                }
            );
        });
    }

    static getVoiceByOrderId(orderId) {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Voice WHERE "orderId" = $1 and  order by id desc`,

                [orderId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.getVoiceByOrderId");

                console.log("VoiceTable.getVoiceByOrderId")

                resolve({voice : response.rows[0]});
                }
            );
        });
    }

    static getLastVoice(driverId,customerId) {

        return new Promise((resolve, reject) => {
            pool.query(
                `select "voicePath", "timestamp" from Voice where "driverId" = $1 and "customerId" = $2 order by id desc limit 1`,

                [driverId, customerId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.getLastVoice");

                console.log("VoiceTable.getLastVoice")

                resolve({ voice : response.rows });
                }
            );
        });
    }

//*********************************** This is were the codes I wrote end. *****************************************************************************/

    static getVoice() {

        return new Promise((resolve, reject) => {
            pool.query(
                `SELECT * FROM Voice`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.getVoice");

                console.log("VoiceTable.getVoice");
                resolve(response.rows);
                }
            );
        });
    }

    static deleteVoiceById(customerId) {

        return new Promise((resolve, reject) => {
            pool.query(
                `DELETE "voicePath"
                FROM Voice WHERE "customerId" = $1
                `,
                [customerId],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND IN : VoiceTable.deleteVoiceById");

                console.log("VoiceTable.deleteVoiceById");

                resolve(response.rows[0]);
                }
            );
        });
    }


}

module.exports = VoiceTable;