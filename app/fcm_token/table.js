const pool = require('../../databasePool');

class Fcm_tokenTable {

    static storeFcm_Token( account_id, push_token ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Fcm_token( account_id, push_token )
                VALUES( $1, $2 )`,

                [account_id, push_token],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Fcm_tokenTable.storeToken");

                console.log("Fcm_tokenTable.storeToken")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static updateFcm_Token( push_token, account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Fcm_token SET push_token = $1 WHERE account_id = $2`,

                [ push_token, account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Fcm_tokenTable.updateToken");

                console.log("Fcm_tokenTable.updateToken")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static getFcm_Token( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Fcm_token where account_id = $1 order by fcm_id Desc limit 1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Fcm_tokenTable.getToken");

                console.log("Fcm_tokenTable.getToken")

                resolve(response.rows);
                }
            );
        });
    };

    static getAllFcm_Token() {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT push_token FROM Fcm_token`,

                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Fcm_tokenTable.getAllFcm_Token");

                console.log("Fcm_tokenTable.getAllFcm_Token")

                resolve(response.rows);
                }
            );
        });
    };

    static deleteFcm_Token( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE token SET push_token = null where account_id = $1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Fcm_tokenTable.deleteToken");

                console.log("Fcm_tokenTable.deleteToken")

                resolve({ msg : "success" });
                }
            );
        });
    };
    
}

module.exports = Fcm_tokenTable;