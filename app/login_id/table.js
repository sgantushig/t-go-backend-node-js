const pool = require('../../databasePool');

class Login_idTable {

    static storeLogin( account_id, tel, login_int ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `INSERT INTO Login_id( account_id, tel, login_int )
                VALUES( $1, $2, $3 )`,

                [account_id, tel, login_int],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Login_idTable.storeLogin");

                console.log("Login_idTable.storeLogin")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static updateLogin( login_int ,account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `UPDATE Login_id SET login_int = $1 WHERE account_id = $2`,

                [ login_int, account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Login_idTable.updateLogin");

                console.log("Login_idTable.updateLogin")

                resolve({ msg : "success" });
                }
            );
        });
    };

    static getLogin( account_id ) {
        return new Promise((resolve, reject) => {
            pool.query(

                `SELECT * FROM Login_id where account_id = $1`,

                [account_id],
                (error, response) => {
                if (error) return reject(error), console.log("ERROR FOUND AT : Login_idTable.getLogin");

                console.log("Login_idTable.getLogin")

                resolve(response.rows);
                }
            );
        });
    };
    
}

module.exports = Login_idTable;